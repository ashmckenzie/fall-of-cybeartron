from flask import render_template
from sqlalchemy.sql import and_, true

from ...cache import cache
from ...models import (
    Teams, Users, db,
    TeamFieldEntries, UserFieldEntries, Fields,
)
from ...scoreboard import scoreboard
from ...utils import config
from ...utils.config.visibility import scores_visible
from ...utils.decorators.visibility import check_score_visibility
from ...utils.helpers import get_infos
from ...utils.modes import get_model
from ...utils.scores import get_standings


# We can cache this because we don't do any admin stuff and `get_standings()`
# only does if we tell it to
@cache.cached(timeout=60, key_prefix="_get_standings")
def _get_standings():
    # This uses the vanilla `get_standings()` rather that our filtering one
    return get_standings()

@cache.cached(timeout=60, key_prefix="_get_delegate_ids")
def _get_delegate_ids():
    # Get a set of IDs for teams verified as delegates
    Model = get_model()
    if Model is Teams:
        FieldEntriesModel = TeamFieldEntries
        field_account_id = TeamFieldEntries.team_id
    elif Model is Users:
        FieldEntriesModel = UserFieldEntries
        field_account_id = UserFieldEntries.user_id
    else:
        raise Exception("Unknown mode")
    delegate_ids = set(
        id_ for (id_, ) in
        db.session.query(Model.id).join(
            FieldEntriesModel, field_account_id == Model.id,
        ).join(
            Fields, and_(
                Fields.id == FieldEntriesModel.field_id,
                Fields.name == "delegate",
                Fields.field_type == "boolean",
                FieldEntriesModel.value == true(),
            )
        )
    )
    return delegate_ids

@scoreboard.route("/scoreboard")
@check_score_visibility
def listing():
    infos = get_infos()

    if config.is_scoreboard_frozen():
        infos.append("Scoreboard has been frozen")

    standings = _get_standings()
    delegate_ids = _get_delegate_ids()

    return render_template(
        "plugins/delegate_scoreboard/templates/scoreboard.html",
        standings=standings, infos=infos,
        delegate_ids=delegate_ids,
    )
