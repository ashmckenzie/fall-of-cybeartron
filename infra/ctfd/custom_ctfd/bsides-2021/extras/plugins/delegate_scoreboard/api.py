from collections import defaultdict

import flask
from flask_restx import Resource

from ... import api as ctfd_api
from ...api.v1 import scoreboard as api_scrb
from ...cache import cache
from ...models import Awards, Solves, Users, db, Fields
from ...utils import get_config
from ...utils.dates import isoformat, unix_time_to_utc
from ...utils.decorators.visibility import (
    check_account_visibility,
    check_score_visibility,
)
from .scores import plank_standings


@api_scrb.scoreboard_namespace.route("/top/<only>/<count>")
@api_scrb.scoreboard_namespace.param("count", "How many top teams to return")
class PlankScoreboardDetail(Resource):
    # Making this a memoized staticmethod fixes caching
    @staticmethod
    @check_account_visibility
    @check_score_visibility
    @cache.memoize(timeout=60)
    def get(only, count):
        response = {}

        # This is our value add:
        Fields.query.filter(Fields.name==only).first_or_404()
        standings = plank_standings(count=count, only_field=only)

        team_ids = [team.account_id for team in standings]

        solves = Solves.query.filter(Solves.account_id.in_(team_ids))
        awards = Awards.query.filter(Awards.account_id.in_(team_ids))

        freeze = get_config("freeze")

        if freeze:
            solves = solves.filter(Solves.date < unix_time_to_utc(freeze))
            awards = awards.filter(Awards.date < unix_time_to_utc(freeze))

        solves = solves.all()
        awards = awards.all()

        # Build a mapping of accounts to their solves and awards
        solves_mapper = defaultdict(list)
        for solve in solves:
            solves_mapper[solve.account_id].append(
                {
                    "challenge_id": solve.challenge_id,
                    "account_id": solve.account_id,
                    "team_id": solve.team_id,
                    "user_id": solve.user_id,
                    "value": solve.challenge.value,
                    "date": isoformat(solve.date),
                }
            )

        for award in awards:
            solves_mapper[award.account_id].append(
                {
                    "challenge_id": None,
                    "account_id": award.account_id,
                    "team_id": award.team_id,
                    "user_id": award.user_id,
                    "value": award.value,
                    "date": isoformat(award.date),
                }
            )

        # Sort all solves by date
        for team_id in solves_mapper:
            solves_mapper[team_id] = sorted(
                solves_mapper[team_id], key=lambda k: k["date"]
            )

        for i, _team in enumerate(team_ids):
            response[i + 1] = {
                "id": standings[i].account_id,
                "name": standings[i].name,
                "score": int(standings[i].score),
                "solves": solves_mapper.get(standings[i].account_id, []),
            }
        return {"success": True, "data": response}
