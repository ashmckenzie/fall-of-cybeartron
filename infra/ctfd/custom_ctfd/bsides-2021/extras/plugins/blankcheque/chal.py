import pathlib

import flask

from ... import models as ctfd_models
from ... import plugins as ctfd_plugins
from ...utils import logging as ctfd_log
from ...utils import user as ctfd_user_utils
from .. import challenges as plugins_chals
from .. import dynamic_challenges as plugins_dynchals

PLUGINS_DIR = pathlib.Path(ctfd_plugins.__file__).parent
SCRIPT_DIR = pathlib.Path(__file__).parent
ASSETS_DIR = SCRIPT_DIR / "assets"
# "plugins/" is actually magic from the template loader rather than a path
ASSETS_PREFIX = f"plugins/{ASSETS_DIR.relative_to(PLUGINS_DIR)}"

STEALTH_TYPE = "stealth"
STEALTH_DYNAMIC_TYPE = "stealth-dynamic"


class StealthChallengeMixin(object):
    @classmethod
    def is_visible(cls, challenge):
        user_obj = ctfd_user_utils.get_current_user()
        is_solved_q = (
            ctfd_models.db.session
            .query(ctfd_models.Solves.challenge_id)
            .filter(
                ctfd_models.Solves.challenge_id == challenge.id,
                ctfd_models.Solves.account_id == user_obj.account_id,
            )
        )
        return is_solved_q.first() is not None


class StealthChallenge(ctfd_models.Challenges):
    __mapper_args__ = {"polymorphic_identity": STEALTH_TYPE}


class StealthChallengeType(
    StealthChallengeMixin, plugins_chals.CTFdStandardChallenge
):
    id = STEALTH_TYPE
    name = STEALTH_TYPE
    challenge_model = StealthChallenge
    # We only change the create template
    templates = plugins_chals.CTFdStandardChallenge.templates.copy()
    templates["create"] = f"{ASSETS_PREFIX}/create_stealth.html"
    blueprint = flask.Blueprint(STEALTH_TYPE, __name__)


class StealthDynamicChallenge(plugins_dynchals.DynamicChallenge):
    __mapper_args__ = {"polymorphic_identity": STEALTH_DYNAMIC_TYPE}


class StealthDynamicChallengeType(
    StealthChallengeMixin, plugins_dynchals.DynamicValueChallenge
):
    id = STEALTH_DYNAMIC_TYPE
    name = STEALTH_DYNAMIC_TYPE
    challenge_model = StealthDynamicChallenge
    # We only change the create template
    templates = plugins_chals.CTFdStandardChallenge.templates.copy()
    templates["create"] = f"{ASSETS_PREFIX}/create_stealth_dynamic.html"
    blueprint = flask.Blueprint(STEALTH_DYNAMIC_TYPE, __name__)
