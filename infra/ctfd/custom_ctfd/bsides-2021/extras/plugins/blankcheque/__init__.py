import os
import pathlib

from .. import challenges as plugins_chals
from .. import migrations as plugins_migrations
from .. import override_template
from . import api, chal

SCRIPT_DIR = pathlib.Path(__file__).resolve().parent
TEMPLATE_OVERRIDE_DIR = SCRIPT_DIR / "templates" / "override"


def load(app):
    # Override the challenges template to add the new form
    for p, _, fs in os.walk(TEMPLATE_OVERRIDE_DIR):
        for f in fs:
            fp = pathlib.Path(p, f)
            override_template(
                str(fp.relative_to(TEMPLATE_OVERRIDE_DIR)),
                open(fp).read()
            )
    # Register the `/attempt_any` API endpoint
    api.register()
    # Register the new challenge types
    plugins_migrations.upgrade()
    plugins_chals.CHALLENGE_CLASSES.update({
        chal.STEALTH_TYPE: chal.StealthChallengeType,
        chal.STEALTH_DYNAMIC_TYPE: chal.StealthDynamicChallengeType,
    })
