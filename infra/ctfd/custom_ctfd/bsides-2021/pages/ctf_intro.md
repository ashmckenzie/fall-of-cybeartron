___
## CTF Introduction

A CTF (Capture The Flag) is a computer security competition that requires contestants to solve a variety of challenges in order to discover or retrieve flags. Flags are often required to be submitted to a scoring system to award points and ultimately determine competition winners. Whilst every CTF is different, individual challenges often belong to one of the following categories: pwnable, reversing, web, crypto, programming, forensics or misc. Most CTFs will have challenges from each of the topics allowing for participants to choose challenges that align with their skillsets or interests.

To further expand the common categories.

### Pwnable

Challenges contain a vulnerable service listening on a port on a server the competition organisers control. Contestants are usually given a copy of the server (usually an executable) in which they are expected to find a vulnerability. From there the participant will write an exploit to take advantage of the vulnerability and allow the retrieval of a flag. The flag is generally a file that resides on the vulnerable server.

### Reversing

Participants are usually given an executable application. Contained within the application will be an obfuscated flag. Participants must reverse engineer the application to determine the flag.

### Web

Participants are usually given a URL which will contain a vulnerable or misconfigured website or web application. Participants are usually required to break into the application to retrieve a flag.

### Crypto

Participants are required to break or clone cryptographic objects in order to retrieve a flag. These are usually implementation flaws or replications of well known cryptographic weaknesses.

### Programming

These challenges usually involve implementing a solution to a programming task in the most efficient way possible. Often size or time constraints are involved to enforce only the most suitable solutions.

### Forensics

Participants are given some piece of information or file often found in computer forensics (network packet capture, suspicious file etc.). Contained in this will be a flag but often under many layers of obfuscation and abstraction.

### Misc

Challenges that don't fit into any other category. These challenges might delve into the inner workings or little-used options of common applications or file formats, or explore some facet of computer science that is often overlooked.

## CTF Environment
Completing CTF challenges requires a number of specialised tools. Further they often require participants to download and run applications provided by the competition organisers.  For a number of reasons, it's not a good idea to run CTF challenges on your main Operating System; aside from the security implications of running untrusted code, CTF challenges often require certain libraries and have expectations on their operating environment. 
It is becoming more common for challenges to be delivered in the form of a docker container but even then, it is still advisable to run this within a segregated environment dedicated to CTFs.
The best way to achieve this is via a Virtual Machine. `VirtualBox` is a good free choice for Linux or mac systems, `Hyper-V` is great for Windows 10 hosts.

### Guest Operating System 
Any guest operating system could work for a CTF environment, but the latest Ubuntu LTS will have the best support for all the tools you are going to need.

### Tools
#### GDB - debugger of choice 
These extensions make GDB much more useful:
 * [pwndbg](https://github.com/pwndbg/pwndbg) or;
 * [gef](https://github.com/hugsy/gef)

#### Python3 - language of choice for CTF solutions.
 * [Pwntools](https://docs.pwntools.com/) - exploit helper libraries.

#### Code Editor / IDE
 * [VS Code](https://code.visualstudio.com/) is a great free option.

#### Disassembly / Decompilation
 * [Ghidra](https://ghidra-sre.org/) is a great free option.
 * [IDA Pro](https://www.hex-rays.com/products/ida/) is still the gold standard if you can afford it - there is a cut-down free version.
 * [BinaryNinja](https://binary.ninja/) is great for its scripting interface and UI - not free though.

#### Static & Dynamic Analysis
 * [Angr](https://angr.io/)

#### Hex editor
 * `xxd` is great for quick viewing of  files in the command line.
 * [010](https://www.sweetscape.com/010editor/) is really good - Is not free but has a free trial.
 * For a GUI hex editor that is free check out `GNOME Hex Editor`.

#### Data manipulation
 * [CyberChef (“The cyber swiss-army knife”)](https://gchq.github.io/CyberChef/)

#### Multi-architecture support (eg. run ARM binaries on a x86 machine)
 * Qemu - see http://reverseengineering.stackexchange.com/questions/8829/cross-debugging-for-mips-elf-with-qemu-toolchain

#### [Docker](https://www.docker.com/)
A open platform for developing, shipping, and running applications.

#### [Wireshark](https://www.wireshark.org/)
Packet capture and network protocol analysis.

## Getting Started

Check out our `beginner` tagged [challenges](/challenges), these are designed to be a nice introduction to CTF challenges.

### [CTF Field Guide](https://trailofbits.github.io/ctf/)
This is a great resource for getting started in CTFs.

### [LiveOverflow](https://liveoverflow.com/)

Great videos on various IT security topics and CTF challenge walkthroughs.

In particular check out the [Binary Exploitation playlist](https://www.youtube.com/playlist?list=PLhixgUqwRTjxglIswKp9mpkfPNfHkzyeN).


## Moar please
So now you are hooked on CTFs, check out these resources (after the BSides Canberra CTF has finished of course).

### [Pwnable.kr](http://pwnable.kr/)
Slightly dated but a great selection of challenges to build up experience.

### [Microcorruption](https://microcorruption.com/login)
Not a CTF as such but a really interesting set of pwnable challenges on a custom architecture.

### [CTF Time](https://ctftime.org/)
Contains past and present CTF information. Includes links to writeups for past CTFs.
