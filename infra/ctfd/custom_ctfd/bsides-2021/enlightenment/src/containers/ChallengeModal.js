import { useState, useLayoutEffect, useEffect } from 'react'

import Modal from 'react-bootstrap/Modal'
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'

import { baseURL } from '../baseUrl'
import ChallengeDetails from './ChallengeDetails'
import ChallengeSolves from './ChallengeSolves'

const ChallengeModal = props => {
    const { id, solveHandler } = props
    const [challenge, setChallenge] = useState(null)
    const [solved, setSolved] = useState(false)

    useLayoutEffect(() => {
        fetch(`${baseURL}/api/v1/challenges/${id}`)
            .then(response => response.json())
            .then(result => {
                setChallenge(result.data)
            })
            .catch(console.error)
    }, [id, solved])

    useEffect(() => {
        if (challenge) {
            setSolved(challenge.solved_by_me)
            solveHandler()
        }
    }, [challenge, solved, solveHandler])

    let output = null
    if (challenge) {
        // If the challenge is hidden, solves = null, which will break some stuff
        const isHidden = challenge.type === "hidden"
        const numOfSolves = isHidden ? "?" : challenge.solves

        output = (
            <Modal
                {...props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        /path/to/{challenge.category}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Tabs defaultActiveKey="details" id="challenge-modal-tabs">
                        <Tab eventKey="details" title="Details">
                            <ChallengeDetails challenge={challenge} onSolve={() => setSolved(true)} isHidden={isHidden} />
                        </Tab>
                        <Tab eventKey="solves" title={`Solves (${numOfSolves})`}>
                            <ChallengeSolves id={id} solved={solved} isHidden={isHidden} />
                        </Tab>
                    </Tabs>
                </Modal.Body>
            </Modal>
        )
    }

    return output
}

export default ChallengeModal
