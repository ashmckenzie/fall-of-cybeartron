import Button from 'react-bootstrap/Button'

const FlagSubmission = props => {
    const { handleSubmit, handleChange } = props
    return (
        <div className="flag-submission-form">
            <form onSubmit={handleSubmit}>
                <input type="text" placeholder="cybears{...}" onChange={handleChange} />
                <Button variant="primary" type="submit">
                    Submit
            </Button>
            </form>
        </div>
    )
}

export default FlagSubmission
