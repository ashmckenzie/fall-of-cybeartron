import Alert from 'react-bootstrap/Alert'

const SubmissionAlert = props => {
    const { attempt, show, close } = props
    const variant = attempt.data.status === "correct" ? "success" : "danger"

    return <Alert variant={variant} show={show} onClose={close} dismissible transition>{attempt.data.message}</Alert>
}

export default SubmissionAlert
