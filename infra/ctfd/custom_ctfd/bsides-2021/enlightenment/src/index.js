import React from 'react';
import ReactDOM from 'react-dom';
import ChallengesBoard from './containers/ChallengesBoard';

ReactDOM.render(
  <React.StrictMode>
    <ChallengesBoard />
  </React.StrictMode>,
  document.getElementById('challenges-board')
);
