terraform {
  required_version = ">= 0.14.9"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.34"
    }
  }
}

resource "aws_acm_certificate" "cert" {
  count = var.https_certificate_arn == "" ? 1 : 0
  domain_name       = var.ctf_domain
  validation_method = "DNS"
}


resource "aws_route53_record" "cert_validation" {
  count = var.https_certificate_arn == "" ? 1 : 0
  name    = aws_acm_certificate.cert[count.index].domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert[count.index].domain_validation_options.0.resource_record_type
  zone_id = var.ctf_domain_zone_id
  records = [aws_acm_certificate.cert[count.index].domain_validation_options.0.resource_record_value]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "cert" {
  count = var.https_certificate_arn == "" ? 1 : 0
  certificate_arn         = aws_acm_certificate.cert[count.index].arn
  validation_record_fqdns = [aws_route53_record.cert_validation[count.index].fqdn]
}

module "ctfd" {
  source                            = "1nval1dctf/ctfd/aws"
  version                           = "0.6.9"
  https_certificate_arn             = var.https_certificate_arn != "" ? var.https_certificate_arn : aws_acm_certificate.cert[0].arn
  ctfd_version                      = var.ctfd_version
  elasticache_cluster_instances     = var.elasticache_cluster_instances
  elasticache_cluster_instance_type = var.elasticache_cluster_instance_type
  db_cluster_instances              = var.db_cluster_instances
  db_serverless_min_capacity        = var.db_serverless_min_capacity
  db_cluster_instance_type          = var.db_cluster_instance_type
  db_deletion_protection            = var.db_deletion_protection
  db_engine_mode                    = var.db_engine_mode
  force_destroy_challenge_bucket    = var.force_destroy_challenge_bucket
  asg_min_size                      = var.asg_min_size
  asg_max_size                      = var.asg_max_size
  asg_instance_type                 = var.asg_instance_type
  workers                           = var.workers
  worker_connections                = var.worker_connections
  allowed_cidr_blocks               = var.allowed_cidr_blocks
  ctfd_overlay                      = var.ctfd_overlay
  create_cdn                        = true
  ctf_domain                        = var.ctf_domain
  ctf_domain_zone_id                = var.ctf_domain_zone_id
  ctfd_repo                         = var.ctfd_repo
  db_skip_final_snapshot            = var.db_skip_final_snapshot
}

terraform {
  backend "s3" {
    bucket         = "terraform-20200221041354879200000001"
    key            = "ctfd/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-locks"
    region         = "ap-southeast-2"
  }
}
