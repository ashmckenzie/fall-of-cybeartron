
# Grab the list of availability zones
data "aws_availability_zones" "available" {}

resource "random_string" "suffix" {
  length  = 8
  special = false
}

# Create a VPC to launch our eks into
module "vpc" {
  source               = "terraform-aws-modules/vpc/aws"
  version              = "2.64.0"
  name                 = "${var.challenge_eks_cluster_name}_vpc"
  cidr                 = var.vpc_cidr_block
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets       = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  enable_dns_hostnames = true
  enable_dns_support   = true
  enable_nat_gateway   = true
  single_nat_gateway   = true
  vpc_tags = {
    "kubernetes.io/cluster/${var.challenge_eks_cluster_name}" = "shared"
  }
  public_subnet_tags = {
    "kubernetes.io/cluster/${var.challenge_eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"                                  = "1"
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/${var.challenge_eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"                         = "1"
  }
}