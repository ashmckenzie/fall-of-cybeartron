terraform {
  required_version = ">= 0.14.9"
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.34"
    }
  }
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  exec {
    api_version = "client.authentication.k8s.io/v1alpha1"
    args        = ["eks", "get-token", "--cluster-name", data.aws_eks_cluster.cluster.id]
    command     = "aws"
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_iam_role" "worker_role" {
  name = module.eks.worker_iam_role_name
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "14.0.0"
  cluster_name    = var.challenge_eks_cluster_name
  cluster_version = "1.19"
  # need private for containers and public for LoadBalancers
  subnets = concat(module.vpc.private_subnets, module.vpc.public_subnets)
  vpc_id  = module.vpc.vpc_id
  # avoid the need for aws-iam-authenticator
  kubeconfig_aws_authenticator_command      = "aws"
  kubeconfig_aws_authenticator_command_args = ["eks", "get-token", "--cluster-name", var.challenge_eks_cluster_name]
  map_users = var.eks_users
  # needed for OpenID Connect Provider
  enable_irsa = true
  workers_group_defaults = {
    root_volume_type = "gp2"
  }
  worker_groups = [
    {
      name                 = "${var.challenge_eks_cluster_name}-autoscaling-group"
      instance_type        = var.eks_instance_type
      platform             = "linux"
      asg_min_size         = var.eks_autoscaling_group_min_size
      asg_max_size         = var.eks_autoscaling_group_max_size
      asg_desired_capacity = var.eks_autoscaling_group_desired_capacity

      tags = [{
        key                 = "k8s.io/cluster-autoscaler/enabled"
        propagate_at_launch = false
        value               = "true"
        }, {
        key                 = "k8s.io/cluster-autoscaler/${var.challenge_eks_cluster_name}"
        propagate_at_launch = false
        value               = "true"
      }]
    },
    {
      name          = "${var.challenge_eks_cluster_name}-autoscaling-group-windows"
      instance_type = var.eks_instance_type
      platform      = "windows"
      # we set these vars to  0 on first creation as we need to do some configuration on the cluster outside of terraform
      # then we can re-run terraform apply with these set to values other than 0 to create the windows worker nodes
      asg_min_size         = var.eks_autoscaling_group_windows_min_size
      asg_max_size         = var.eks_autoscaling_group_windows_max_size
      asg_desired_capacity = var.eks_autoscaling_group_windows_desired_capacity

      tags = [{
        key                 = "k8s.io/cluster-autoscaler/enabled"
        propagate_at_launch = false
        value               = "true"
        }, {
        key                 = "k8s.io/cluster-autoscaler/${var.challenge_eks_cluster_name}"
        propagate_at_launch = false
        value               = "true"
      }]
    }
  ]
}

module "alb_ingress_controller" {
  source  = "iplabs/alb-ingress-controller/kubernetes"
  version = "3.4.0"

  k8s_cluster_type = "eks"
  k8s_namespace    = "kube-system"

  aws_region_name  = var.aws_region
  k8s_cluster_name = module.eks.cluster_id
}

terraform {
  backend "s3" {
    bucket         = "terraform-20191116042935470700000001"
    key            = "eks/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "terraform-state-lock-dynamo"
    region         = "ap-southeast-2"
  }
}