resource "kubernetes_service_account" "external_dns" {
  provider = kubernetes
  metadata {
    name = "external-dns"
  }
  automount_service_account_token = "true"
}
resource "kubernetes_cluster_role" "external_dns" {
  metadata {
    name = "external-dns"
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["services"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["pods"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources  = ["nodes"]
    verbs      = ["list", "watch"]
  }
}

resource "kubernetes_cluster_role_binding" "external_dns" {
  metadata {
    name = "external-dns-viewer"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "external-dns"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "external-dns"
    namespace = "default"
  }
}

resource "kubernetes_deployment" "external_dns_challenge" {
  timeouts {
    create = "1m"
    update = "1m"
    delete = "1m"
  }
  metadata {
    name = "external-dns-challenge"
  }

  spec {

    selector {
      match_labels = {
        app = "external-dns-challenge"
      }
    }

    template {
      metadata {
        labels = {
          app  = "external-dns-challenge"
          name = "external-dns-challenge"
        }
      }

      spec {
        container {
          name  = "external-dns"
          image = "registry.opensource.zalan.do/teapot/external-dns:latest"
          args = concat([
            "--source=service",
            "--source=ingress",
            "--domain-filter=${var.chal_domain}",
            "--provider=aws",
            "--policy=upsert-only",
            "--aws-zone-type=public",
            "--registry=txt",
            "--txt-owner-id=${var.chal_domain_zone_id}"
          ])
        }
        security_context {
          fs_group = 65534
        }
        node_selector = var.kubernetes_deployment_node_selector

        service_account_name            = "external-dns"
        automount_service_account_token = "true"
      }
    }

    strategy {
      type = "Recreate"
    }
  }
}

resource "kubernetes_deployment" "external_dns_status" {
  timeouts {
    create = "1m"
    update = "1m"
    delete = "1m"
  }
  metadata {
    name = "external-dns-status"
  }

  spec {

    selector {
      match_labels = {
        app = "external-dns-status"
      }
    }

    template {
      metadata {
        labels = {
          app  = "external-dns-status"
          name = "external-dns-status"
        }
      }

      spec {
        container {
          name  = "external-dns"
          image = "registry.opensource.zalan.do/teapot/external-dns:latest"
          args = concat([
            "--source=service",
            "--source=ingress",
            "--domain-filter=${var.ctf_domain}",
            "--provider=aws",
            "--policy=upsert-only",
            "--aws-zone-type=public",
            "--registry=txt",
            "--txt-owner-id=${var.ctf_domain_zone_id}"
          ])
        }
        security_context {
          fs_group = 65534
        }
        node_selector = var.kubernetes_deployment_node_selector

        service_account_name            = "external-dns"
        automount_service_account_token = "true"
      }
    }

    strategy {
      type = "Recreate"
    }
  }
}
