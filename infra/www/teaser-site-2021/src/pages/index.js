import '../css/index.css'

import React from 'react'
import { Helmet } from "react-helmet"


export default function Home() {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>/path/to/enlightenment</title>
        <link rel="canonical" href="http://ctf.cybears.io" />
        <link rel="icon"
          type="image/png"
          href="https://ctf.cybears.io/favicon.ico"></link>
      </Helmet>
      <div className="sunburst">
        <img src="sunrise.svg" alt="Rotating sunburst background" />
      </div>
      <div className="overlay">
        <h1 className="title">/path/to/enlightenment</h1>
        <h2>BSides Canberra Capture the Flag</h2>
        <div className="footer">
          <div className="details">9th & 10th of April 2021</div>
          <div className="twitter">
            <a href="https://twitter.com/cybearsCTF" target="_blank" rel="noreferrer noopener">@cybearsCTF</a></div>
          <div className="sponsor">
            Sponsored by: <br />
            <a href="https://www.asd.gov.au/" target="_blank" rel="noreferrer noopener"><img src="asd-logo.png" alt="ASD Logo" /></a>
          </div>
        </div>
      </div>
    </>
  )
}