
variable "namespace" {
  type        = string
  description = "Kubernetes namespace to deploy into"
  default     = "default"
}
variable "annotations" {
  type        = string
  description = "Grafana k8s deploy annotations."
  default     = ""
}
variable "service_annotations" {
  type        = string
  description = "Grafana k8s deploy service annotations."
  default     = ""
}
variable "host" {
  type        = string
  description = "Grafana k8s deploy host."
  default     = ""
}
variable "extra_paths" {
  type        = string
  description = "Grafana k8s extra_paths."
  default     = ""
}
variable "service" {
  type        = string
  description = "Grafana k8s service."
  default     = ""
}