resource "kubernetes_config_map" "cloudwatch-exporter-config" {
  metadata {
    name = "cloudwatch-exporter-config"
    namespace = var.namespace
  }

  data = {
    "config.yml" = file("${path.module}/config.yml")
  }
}

resource "kubernetes_config_map" "cloudwatch-exporter-cloudfront-config" {
  metadata {
    name = "cloudwatch-exporter-cloudfront-config"
    namespace = var.namespace
  }

  data = {
    "config.yml" = file("${path.module}/cloudfront-config.yml")
  }
}

variable "cloudwatch_exporter_deployment_node_selector" {
  type = map(string)
  default = {
    "kubernetes.io/os" = "linux"
  }
  description = "Node selectors for cloudwatch_exporter kubernetes deployment"
}

resource "kubernetes_pod" "cloudwatch-exporter" {
  metadata {
    name = "cloudwatch-exporter"
    labels = {
      App = "cloudwatch-exporter"
    }
    namespace = var.namespace
  }

  spec {
    container {
      image = "prom/cloudwatch-exporter"
      name  = "cloudwatch-exporter"
      env {
        name  = "AWS_ACCESS_KEY_ID"
        value = var.aws_access_key_id
      }
      env {
        name  = "AWS_SECRET_ACCESS_KEY"
        value = var.aws_secret_access_key
      }

      port {
        container_port = 9106
      }
      volume_mount {
        name       = "config"
        mount_path = "/config"
      }
    }
    volume {
      name = "config"
      config_map {
        name = kubernetes_config_map.cloudwatch-exporter-config.metadata[0].name
      }
    }
    node_selector = var.cloudwatch_exporter_deployment_node_selector
  }
}

resource "kubernetes_service" "cloudwatch-exporter" {
  metadata {
    name = "cloudwatch-exporter"
    namespace = var.namespace
  }
  spec {
    selector = {
      App = kubernetes_pod.cloudwatch-exporter.metadata[0].labels.App
    }
    port {
      port        = 9106
      target_port = 9106
    }
  }
}

# output "cloudwatch_exporter_ip" {
#   value = kubernetes_service.cloudwatch-exporter.load_balancer_ingress[0].hostname
# }

resource "kubernetes_pod" "cloudwatch-exporter-cloudfront" {
  metadata {
    name = "cloudwatch-exporter-cloudfront"
    labels = {
      App = "cloudwatch-exporter-cloudfront"
    }
    namespace = var.namespace
  }

  spec {
    container {
      image = "prom/cloudwatch-exporter"
      name  = "cloudwatch-exporter-cloudfront"
      env {
        name  = "AWS_ACCESS_KEY_ID"
        value = var.aws_access_key_id
      }
      env {
        name  = "AWS_SECRET_ACCESS_KEY"
        value = var.aws_secret_access_key
      }

      port {
        container_port = 9106
      }
      volume_mount {
        name       = "config"
        mount_path = "/config"
      }
    }
    volume {
      name = "config"
      config_map {
        name = kubernetes_config_map.cloudwatch-exporter-cloudfront-config.metadata[0].name
      }
    }
    node_selector = var.cloudwatch_exporter_deployment_node_selector
  }
}

resource "kubernetes_service" "cloudwatch-exporter-cloudfront" {
  metadata {
    name = "cloudwatch-exporter-cloudfront"
    namespace = var.namespace
  }
  spec {
    selector = {
      App = kubernetes_pod.cloudwatch-exporter-cloudfront.metadata[0].labels.App
    }
    port {
      port        = 9106
      target_port = 9106
    }
  }
}
