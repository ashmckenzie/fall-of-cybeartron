import gitlab
import getpass
import requests
import io

class Gitlab:
    def __init__(self, repo_base, project, private_token=None):
        self.repo_base = repo_base
        self.project_name = project
        self.private_token = private_token
        self.gl = gitlab.Gitlab(repo_base, private_token=private_token)
        try:
            self.project = self.gl.projects.get(project)
        except gitlab.exceptions.GitlabGetError:
            if private_token is None:
                private_token = getpass.getpass(prompt='Request to authenticate with gitlab API failed, perhaps this is a private repo. Please enter an authorised access token: ', stream=None)     
                self.gl = gitlab.Gitlab(repo_base, private_token=private_token)
                self.project = self.gl.projects.get(project)

    def get_child_pipeline(self, pipeline, child_pipeline_name):
        for bridge in pipeline.bridges.list():
            if bridge.name == child_pipeline_name:
                return self.project.pipelines.get(bridge.downstream_pipeline['id'])

    def get_job(self, pipeline, job_name):
        for job in pipeline.jobs.list():
            if job.name == job_name:
                return self.project.jobs.get(job.id)

    
    def get_latest_pipeline(self, ref, only_successful=True):
        query_parameters={'ref':ref}
        if only_successful:
            query_parameters['status'] = 'success'
        pipelines =  self.project.pipelines.list(query_parameters=query_parameters)
        if len(pipelines) > 0:
            return pipelines[0]
        else:
            raise ValueError('No successful pipelines')

    def get_artifact(self, job):
        b = io.BytesIO()
        job.artifacts(streamed=True, action=b.write)
        return b
    
    def get_artifact_from_job(self, pipeline, job_name):
        job_id = 0
        try:
            job = self.get_job(pipeline, job_name)
            return self.get_artifact(job)
        except gitlab.exceptions.GitlabAuthenticationError:
            for job in pipeline.jobs.list():
                if job.name == job_name:
                    job_id = job.id 

        if job_id:
            r = requests.get(f"{self.repo_base}/api/v4/projects/{self.project_name}/jobs/{job_id}/artifacts")
            if r.status_code == 200:
                return io.BytesIO(r.content)
        raise FileNotFoundError("Failed to download artifacts")

