import getpass
import json
import logging
import os
import subprocess
import traceback
import yaml

KUBECTL_PROG=('kubectl',)
K3S_KUBECTL_PROG=('k3s', 'kubectl')
# We use these if we want to be quiet while running subprocesses (the default)
QUIET_KWARGS = {
    "stdout": subprocess.DEVNULL,
    "stderr": subprocess.DEVNULL,
}

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
REGISTRY_SECRET_BASENAME='.k8s_registry_secret.'

class Kubectl:
    def __init__(self, k3s=False):
        if k3s:
            logging.debug("Setting kubectl to k3s kubectl")
            try:
                self.kubectl_proc = K3S_KUBECTL_PROG
                logging.debug("Trying " + ' '.join(self.kubectl_proc) + ' version')
                subprocess.check_call(K3S_KUBECTL_PROG + ('version',), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            except subprocess.CalledProcessError as exc:
                logging.debug(' '.join(self.kubectl_proc) + ' version failed - will use sudo')
                self.kubectl_proc = ("sudo", ) + K3S_KUBECTL_PROG
        else:
            logging.debug("Setting kubectl to kubectl")
            self.kubectl_proc = KUBECTL_PROG

    def _do_action(self, action, fname, can_fail=False, quiet=True):
        cmdline = (
            *self.kubectl_proc, action,
            "--filename", fname, "--recursive",
        )
        logging.info("Running kubernetes %s on %r", action, fname)
        try:
            subprocess.check_call(cmdline, **QUIET_KWARGS if quiet else {})
        except subprocess.CalledProcessError as exc:
            if not can_fail:
                raise exc

    def create(self, deploy_yml_path, **kwargs):
        self._do_action("create", deploy_yml_path, **kwargs)

    def apply(self, deploy_yml_path, **kwargs):
        self._do_action("apply", deploy_yml_path, **kwargs)

    def create_or_replace(self, deploy_yml_path, can_fail=False, **kwargs):
        try:
            self._do_action("create", deploy_yml_path, **kwargs)
        except subprocess.CalledProcessError:
            self._do_action(
                "replace", deploy_yml_path, can_fail=can_fail, **kwargs
            )

    def delete(self, deploy_yml_path, can_fail=True, **kwargs):
        self._do_action("delete", deploy_yml_path, can_fail=can_fail, **kwargs)

    def delete_all_services(self, ns=None, quiet=True):
        try:
            logging.info("Runing kubernettes delete services --all")
            params = ()
            if ns:
                params += ('-n', ns) 
            params += ("delete", "services", "--all", )
            subprocess.check_call(
                self.kubectl_proc + params,
                **QUIET_KWARGS if quiet else {}
            )
        except subprocess.CalledProcessError:
            pass

    def delete_all_pods(self, ns=None, quiet=True):
        try:
            logging.info("Runing kubernettes delete pod --all")
            params = ()
            if ns:
                params += ('-n', ns) 
            params += ("delete", "pod", "--all", )
            subprocess.check_call(
                self.kubectl_proc + params,
                **QUIET_KWARGS if quiet else {}
            )
        except subprocess.CalledProcessError:
            pass

    def wait_for_deployment(self, name, ns="default", quiet=True):
        logging.info("Runing kubernettes rollout status")
        subprocess.check_call(
            self.kubectl_proc + (
                "rollout", "status", "--namespace", ns,
                "deployment.apps/{}".format(name)
            ), **QUIET_KWARGS if quiet else {}
        )

    def get_service_address(self, name, ns="default"):
        logging.info("Runing kubernettes get services")
        service_yml = subprocess.check_output(
            self.kubectl_proc + (
                "get", "services", "--output", "yaml", "--namespace", ns, name
            )
        )
        service_info = yaml.load(service_yml, Loader=yaml.SafeLoader)
        service_ingress = service_info["status"]["loadBalancer"]["ingress"][0]
        if "hostname" in service_ingress:
            addr = service_ingress["hostname"]
        else:
            addr = service_ingress["ip"]
        # We use the `nodePort` if it's present since that's what's actually
        # available on budget load balancers like klipper which is used by k3s
        try:
            port = service_info["spec"]["ports"][0]["nodePort"]
        except KeyError:
            port = service_info["spec"]["ports"][0]["port"]
        if port not in (80, 443):
            addr += ":{}".format(service_info["spec"]["ports"][0]["port"])
        return addr

    def has_secret(self, name, ns="default"):
        result = False
        try:
            logging.info("Runing kubernettes get secrets")
            logging.debug(self.kubectl_proc + ("get", "secrets", "--namespace", ns, name))
            subprocess.check_call(
                self.kubectl_proc + ("get", "secret", "--namespace", ns, name),
                **QUIET_KWARGS
            )
        except subprocess.CalledProcessError:
            pass

    def find_or_prompt_docker_secret(self, name, ns="default"):
        if not self.has_secret(name, ns):
            while True:
                try:
                    self.add_k8s_secret(name, ns)
                except subprocess.CalledProcessError:
                    traceback.print_exc()
                    print("Try again!")
                else:
                    break

    def ensure_namespace(self, namespace):
        # create namespace if it doesn't exist
        try:
            logging.debug(self.kubectl_proc + ("describe", "namespace", namespace))
            subprocess.check_call(
                self.kubectl_proc + ("describe", "namespace", namespace),
                **QUIET_KWARGS
            )
        except subprocess.CalledProcessError:
            logging.debug(self.kubectl_proc + ("create", "namespace", namespace))
            subprocess.check_call(
                self.kubectl_proc + ("create", "namespace", namespace),
                **QUIET_KWARGS
            )

    def add_k8s_secret(self, name, ns):
        registry_secret_file = os.path.join(SCRIPT_DIR, REGISTRY_SECRET_BASENAME + name + '.yml')
        if not os.path.exists(registry_secret_file):
            token_name = input("What is the deploy token username? ")
            password = getpass.getpass(prompt='What is the deploy token password? ', stream=None)
            # Make a k8s secret spec with the deploy token
            cmdline = (
                *self.kubectl_proc,
                "create", "secret", "docker-registry", name,
                "--docker-server=registry.gitlab.com",
                "--docker-email=deploy@cybears.io",
                "--docker-username=" + token_name,
                "--docker-password=" + password,
                "--dry-run=client", "--output", "yaml"
            )
            proc = subprocess.run(
                cmdline, check=True, universal_newlines=True,
                stdout=subprocess.PIPE
            )
            with open(registry_secret_file,'w+') as fout:
                fout.write(proc.stdout)
        else:
            logging.info('There is an existing secret file - reusing it!')

        # create namespace if it doesn't exist
        self.ensure_namespace(ns)

        logging.info("Runing kubernettes apply")
        logging.debug(self.kubectl_proc + ("apply", "--namespace", ns, "-f", registry_secret_file))
        subprocess.check_call(
            self.kubectl_proc + ("apply", "--namespace", ns, "-f", registry_secret_file),
            **QUIET_KWARGS
        )

    def deployment_exists(self, name, ns):
        result = False
        try:
            logging.info("Runing kubernettes get secrets")
            logging.debug(self.kubectl_proc + ("get", "deployments.v1.apps", "--namespace", ns, '--field-selector', f'metadata.name={name}', '--output', 'yaml'))

            deployment_yml = subprocess.check_output(
                self.kubectl_proc + ("get", "deployments.v1.apps", "--namespace", ns, '--field-selector', f'metadata.name={name}', '--output', 'yaml')
            )
            deployment_info = yaml.load(deployment_yml, Loader=yaml.SafeLoader)
            if deployment_info['items']:
                result = True
        except subprocess.CalledProcessError:
            pass
        return result
