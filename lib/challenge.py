import logging
import os
import os.path
import subprocess
import yaml

logger = logging.getLogger(__file__)

# We support the follow challenge types
VALID_TYPES = {
    "standard",
    "dynamic",
    "stealth",
    "stealth-dynamic",
}
DYNAMIC_TYPES = {
    "dynamic",
    "stealth-dynamic",
}
# These are the tags we normally use and some common synonyms
CORE_TAGS = {
    "badge",
    "crypto", "crypt",
    "forensics", "for",
    "misc",
    "pwn", "pwnable",
    "rev", "re",
    "sandbox", "esc",
    "trivia",
    "trophy", "\U0001f3c6",
    "web",
}

# These are the labels we apply to pods for network isolation. The real meaning
# is defined by the network policies we add to the challenge namespace in a k8s
# deployment but pretty much:
#  * `full` isolation means no outbound connections anywhere
#  * `call-out` isolation allows egress to non-private IPv4 blocks
#  * `none` applies no isolation policy
VALID_ISOLATION_TYPES = {"full", "call-out", "none"}
DEFAULT_ISOLATION = "full"
assert DEFAULT_ISOLATION in VALID_ISOLATION_TYPES

# WARNING: Don't change this line, it ensures we don't duplicate the flag 
# description (TRIVIA_FLAG_DESCRIPTION) in a deployed challenge.
# This is a "special" markdown comment so is not rendered.
TRIVIA_FLAG_BLOCK = '''\n\n[//]: # (TRIVIA BLOCK)\n\n'''
# You are free to change this copy
TRIVIA_FLAG_DESCRIPTION = '''Trivia questions do not use the `cybears{}` flag format.
If you're having trouble submitting a flag which you think is correct, speak to an admin.'''

# We define a set of score values to be used by default and for challenges in
# certain categories/with certain tags. We expect to be using dynamic scoring
# so we allow either a single integer value or a 3-tuple of (max, min, decay).
# If a value is a single integer, the default min ratio and decay will be used.
DEFAULT_VALUE = 500
DEFAULT_MIN_MULT = 0.1
DEFAULT_DECAY = 10
TAG_VALUES = {
    "beginner": 10,
    "tutorial": 10,
    "trivia": 100,
}

class NotFound(BaseException):
    pass

class Handout(object):
    def __init__(self, handout_path, root_path):
        self._path = handout_path
        self._file = open(os.path.join(root_path, handout_path), "rb")

    @property
    def path(self):
        return self._path

    @property
    def file(self):
        return self._file

class Deployment(object):
    def __init__(self, deployment_attrs, root_path):
        """
        Represents a deployment config for k8s

        Deployment attrs is a dict containing values from the deployment section
        """
        self._root_path = root_path
        attrs = deployment_attrs if deployment_attrs is not None else dict()
        self._attrs = attrs

    @property
    def deploy_name(self):
        return self._attrs.get("deploy_name")
    @deploy_name.setter
    def deploy_name(self, new_deploy_name):
        self._attrs["deploy_name"] = new_deploy_name

    @property
    def container_image(self):
        return self._attrs.get("container_image")
    @container_image.setter
    def container_image(self, new_container_image):
        self._attrs["container_image"] = new_container_image

    @property
    def container_image_os(self):
        return self._attrs.get("container_image_os") if "container_image_os" in self._attrs else 'linux'
    @container_image_os.setter
    def container_image_os(self, new_container_image_os):
        self._attrs["container_image_os"] = new_container_image_os

    @property
    def healthcheck_image(self):
        return self._attrs.get("healthcheck_image")
    @healthcheck_image.setter
    def healthcheck_image(self, new_healthcheck_image):
        self._attrs["healthcheck_image"] = new_healthcheck_image

    @property
    def solve_script(self):
        return self._attrs.get("solve_script")
    @solve_script.setter
    def solve_script(self, new_solve_script):
        self._attrs["solve_script"] = new_solve_script

    @property
    def target_port(self):
        if isinstance(self._attrs.get("target_port"), int):
            return [self._attrs.get("target_port")]
        # if its not a single int it ought to already be a list of ints.
        return self._attrs.get("target_port")
    @target_port.setter
    def target_port(self, new_target_port_list):
        self._attrs["target_port"] = new_target_port_list

    @property
    def target_dns_subdomain(self):
        return self._attrs.get("target_dns_subdomain")
    @target_dns_subdomain.setter
    def target_dns_subdomain(self, new_target_dns_subdomain):
        self._attrs["target_dns_subdomain"] = new_target_dns_subdomain

    @property
    def min_replicas(self):
        return int(self._attrs.get("min_replicas", 1))
    @min_replicas.setter
    def min_replicas(self, new_min_replicas):
        self._attrs["min_replicas"] = new_min_replicas

    @property
    def max_replicas(self):
        return int(self._attrs.get("max_replicas", 10))
    @max_replicas.setter
    def max_replicas(self, new_max_replicas):
        self._attrs["max_replicas"] = new_max_replicas

    @property
    def replica_target_cpu_percentage(self):
        return int(self._attrs.get("replica_target_cpu_percentage", 75))
    @replica_target_cpu_percentage.setter
    def replica_target_cpu_percentage(self, new_replica_target_cpu_percentage):
        self._attrs["replica_target_cpu_percentage"] = new_replica_target_cpu_percentage

    @property
    def healthcheck_interval(self):
        return int(self._attrs.get("healthcheck_interval", 300))
    @healthcheck_interval.setter
    def healthcheck_interval(self, new_healthcheck_interval):
        self._attrs["healthcheck_interval"] = new_healthcheck_interval

    @property
    def cpu_request(self):
        return self._attrs.get("cpu_request", "400m")
    @cpu_request.setter
    def cpu_request(self, new_cpu_request):
        self._attrs["cpu_request"] = new_cpu_request

    @property
    def mem_request(self):
        return self._attrs.get("mem_request", "800Mi")
    @mem_request.setter
    def mem_request(self, new_mem_request):
        self._attrs["mem_request"] = new_mem_request

    @property
    def cpu_limit(self):
        return self._attrs.get("cpu_limit", "800m")
    @cpu_limit.setter
    def cpu_limit(self, new_cpu_limit):
        self._attrs["cpu_limit"] = new_cpu_limit

    @property
    def mem_limit(self):
        return self._attrs.get("mem_limit", "1Gi")
    @mem_limit.setter
    def mem_limit(self, new_mem_limit):
        self._attrs["mem_limit"] = new_mem_limit

    @property
    def healthcheck_cpu_request(self):
        return self._attrs.get("healthcheck_cpu_request", "100m")
    @healthcheck_cpu_request.setter
    def healthcheck_cpu_request(self, new_healthcheck_cpu_request):
        self._attrs["healthcheck_cpu_request"] = new_healthcheck_cpu_request

    @property
    def healthcheck_mem_request(self):
        return self._attrs.get("healthcheck_mem_request", "400Mi")
    @healthcheck_mem_request.setter
    def healthcheck_mem_request(self, new_healthcheck_mem_request):
        self._attrs["healthcheck_mem_request"] = new_healthcheck_mem_request

    @property
    def healthcheck_cpu_limit(self):
        return self._attrs.get("healthcheck_cpu_limit", "300m")
    @healthcheck_cpu_limit.setter
    def healthcheck_cpu_limit(self, new_healthcheck_cpu_limit):
        self._attrs["healthcheck_cpu_limit"] = new_healthcheck_cpu_limit

    @property
    def healthcheck_mem_limit(self):
        return self._attrs.get("healthcheck_mem_limit", "1Gi")
    @healthcheck_mem_limit.setter
    def healthcheck_mem_limit(self, new_healthcheck_mem_limit):
        self._attrs["healthcheck_mem_limit"] = new_healthcheck_mem_limit

    @property
    def privileged(self):
        return self._attrs.get("privileged", "false")
    @privileged.setter
    def privileged(self, new_privileged):
        self._attrs["privileged"] = new_privileged

    @property
    def isolation(self):
        return self._attrs.get("isolation", DEFAULT_ISOLATION)
    @isolation.setter
    def isolation(self, new_isolation):
        if new_isolation not in self.VALID_ISOLATION_TYPES:
            raise ValueError(new_isolation)
        self._attrs["isolation"] = new_isolation

    DEFAULT_TEMPLATE = os.path.normpath(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        "..", "infra", "kubernetes-deploy", "template_deployment.yml"
    ))
    @property
    def template(self):
        template_path = self._attrs.get("template", self.DEFAULT_TEMPLATE)
        if not os.path.isabs(template_path):
            template_path = os.path.join(self._root_path, template_path)
        if not os.path.isfile(template_path):
            raise FileNotFoundError(template_path)
        return template_path
    @template.setter
    def template(self, new_template_path):
        norm_path = os.path.normpath(new_template_path)
        if not os.path.isfile(norm_path):
            raise FileNotFoundError(new_template_path)
        self._attrs["template"] = norm_path

    def dump(self):
        return self._attrs.copy()

class Challenge(object):
    """
    An object which describes a CTF challenge and its associated data.
    """
    def __init__(self, root_path, attrs=None, need_handouts=True):
        self._root_path = root_path
        attrs = attrs if attrs is not None else dict()
        if need_handouts:
            # Concretise the handouts we were initialised with
            handouts = attrs.get("handouts", list())
            try:
                attrs["handouts"] = list(
                    Handout(handout, root_path) for handout in handouts or list()
                )
            except FileNotFoundError:
                # We can try to build the challenge
                self.try_build()
                # And now retry the handout loading
                attrs["handouts"] = list(
                    Handout(handout, root_path) for handout in handouts or list()
                )
        self._attrs = attrs

    @classmethod
    def from_manifest(cls, manifest_path, need_handouts=True):
        """
        Load a challenge MANIFEST.yml and yield `Challenge` objects from it.
        """
        with open(manifest_path) as mf:
            manifest_documents = yaml.load_all(mf, Loader=yaml.SafeLoader)
            for manifest_document in manifest_documents:
                # MANIFESTs may only contain one challenge block under this key
                try:
                    challenge = manifest_document.get("challenge", None)
                except KeyError:
                    # Weird, log the malformed document and move on
                    logger.error(
                        "Malformed MANIFEST document in %r", manifest_path
                    )
                else:
                    yield cls(
                        os.path.dirname(manifest_path), challenge,
                        need_handouts=need_handouts
                    )

    def dump(self):
        return self._attrs.copy()

    ###########################################################################
    # Simple properties
    ###########################################################################
    @property
    def name(self):
        return self._attrs.get("name", "Unnamed challenge")
    @name.setter
    def name(self, new_name):
        self._attrs["name"] = new_name

    @property
    def deploy_name(self):
        return self.deployment.deploy_name if self.deployment and self.deployment.deploy_name else self.name

    @property
    def type(self):
        return self._attrs.get("type", "dynamic")
    @type.setter
    def type(self, new_type):
        if new_type not in VALID_TYPES:
            raise ValueError(new_type)
        self._attrs["type"] = new_type

    @property
    def category(self):
        return self._attrs.get("category", "No category")
    @category.setter
    def category(self, new_category):
        self._attrs["category"] = new_category

    @property
    def value(self):
        return int(self._attrs.get("value", 0))
    @value.setter
    def value(self, new_value):
        self._attrs["value"] = new_value

    @property
    def deployment(self):
        d = self._attrs.get("deployment")
        return Deployment(d, root_path=self._root_path) if d else None
    @deployment.setter
    def deployment(self, new_value):
        if not isinstance(new_value, Deployment) and not isinstance(new_value, dict):
            raise AttributeError("deployment must be a Deployment object or a dictionary")
        self._attrs["deployment"] = new_value

    @property
    def description(self):
        _description = self._attrs.get("description", "No description")
        # Add our little trivia blurb rather than adding to each challenge.
        if 'trivia' in self.tags and TRIVIA_FLAG_BLOCK not in _description:
            _description = _description + TRIVIA_FLAG_BLOCK + TRIVIA_FLAG_DESCRIPTION
        return _description
    @description.setter
    def description(self, new_description):
        self._attrs["description"] = new_description


    @property
    def use(self):
        # We use challenges by default unless they opt out with a YAML value
        # which ends up being falsey in Python. Normally `use: false` would be
        # the expected trope for opt-out challenges.
        return int(self._attrs.get("use", True))
    @use.setter
    def use(self, new_use):
        self._attrs["use"] = new_use

    @property
    def requirements(self):
        return self._attrs.get("requirements", [])
    @requirements.setter
    def requirements(self, new_requirements):
        self._attrs["requirements"] = new_requirements

    @property
    def anonymize(self):
        return self._attrs.get("anonymize", None)
    @anonymize.setter
    def anonymize(self, new_anonymize):
        self._attrs["anonymize"] = new_anonymize

    ###########################################################################
    # Workflow properties
    #
    # All workflow attributes default to False (or 0 for playtest) if not
    # specified.
    ###########################################################################
    @property
    def workflow(self):
        return self._attrs.get("workflow", dict())
    @workflow.setter
    def workflow(self, new_workflow):
        if not isinstance(new_workflow, dict):
            raise TypeError("workflow must be a dict")


    @property
    def wf_concept(self):
        return self.workflow.get("concept", False)
    @wf_concept.setter
    def wf_concept(self, new_concept):
        if not isinstance(new_concept, bool):
            raise TypeError("concept must be a bool")
        wf = self.workflow
        wf["concept"] = new_concept
        self.workflow = wf

    @property
    def wf_workup(self):
        return self.workflow.get("workup", False)
    @wf_workup.setter
    def wf_workup(self, new_workup):
        if not isinstance(new_workup, bool):
            raise TypeError("workup must be a bool")
        wf = self.workflow
        wf["workup"] = new_workup
        self.workflow = wf

    @property
    def wf_playtest(self):
        return self.workflow.get("playtest", False)
    @wf_playtest.setter
    def wf_playtest(self, new_playtest):
        if not isinstance(new_playtest, int):
            raise TypeError("playtest must be an int")
        wf = self.workflow
        wf["playtest"] = new_playtest
        self.workflow = wf

    @property
    def wf_ci(self):
        return self.workflow.get("ci", False)
    @wf_ci.setter
    def wf_ci(self, new_ci):
        if not isinstance(new_ci, bool):
            raise TypeError("ci must be a bool")
        wf = self.workflow
        wf["ci"] = new_ci
        self.workflow = wf

    @property
    def wf_deploy(self):
        return self.workflow.get("deploy", False)
    @wf_deploy.setter
    def wf_deploy(self, new_deploy):
        if not isinstance(new_deploy, bool):
            raise TypeError("deploy must be a bool")
        wf = self.workflow
        wf["deploy"] = new_deploy
        self.workflow = wf

    @property
    def wf_review(self):
        return self.workflow.get("review", False)
    @wf_review.setter
    def wf_review(self, new_review):
        if not isinstance(new_review, bool):
            raise TypeError("review must be a bool")
        wf = self.workflow
        wf["review"] = new_review
        self.workflow = wf

    @property
    def wf_theme(self):
        return self.workflow.get("theme", False)
    @wf_theme.setter
    def wf_theme(self, new_theme):
        if not isinstance(new_theme, bool):
            raise TypeError("theme must be a bool")
        wf = self.workflow
        wf["theme"] = new_theme
        self.workflow = wf

    ###########################################################################
    # Iterables
    ###########################################################################
    @property
    def tags(self):
        return self._attrs.get("tags") or list()
    @tags.setter
    def tags(self, new_tags):
        self._attrs["tags"] = list(new_tags)
    def add_tag(self, new_tag):
        self.tags.append(new_tag)

    @property
    def handouts(self):
        return self._attrs.get("handouts") or list()
    @handouts.setter
    def handouts(self, new_handouts):
        self._attrs["handouts"] = new_handouts
    def add_handout(self, new_handout):
        # Handouts need to be conretised
        self.handouts.append(Handout(new_handout, self._root_path))

    @property
    def flags(self):
        return self._attrs.get("flags") or list()
    @flags.setter
    def flags(self, new_flags):
        self._attrs["flags"] = new_flags
    def add_handout(self, new_flag):
        # Handouts need to be conretised
        self.flags.append(new_flag)

    ###########################################################################
    # Calculated properties
    ###########################################################################
    @property
    def dynamic_value(self):
        # Consider both the category and tags
        value_candidates = list()
        for tag in [self.category, *self.tags]:
            cand = TAG_VALUES.get(tag, DEFAULT_VALUE)
            if isinstance(cand, int):
                hi, lo, decay = cand, cand * DEFAULT_MIN_MULT, DEFAULT_DECAY
            # Sanity check that we don't have nonsensical values
            assert hi >= lo, (hi, lo)
            assert decay >= 1, decay
            value_candidates.append((hi, lo, decay))
        # We'll choose the value with the lowest maximum and minimum scores.
        # This is handled nicely by a simple numeric sort which does select the
        # lowest decay rate as well but that's probably not an issue
        return sorted(value_candidates)[0]

    ###########################################################################
    # General helpers
    ###########################################################################
    def find_first_file(self, filename):
        for path, __, files in os.walk(self._root_path):
            if filename in files:
                return os.path.join(self._root_path, path, filename)

    ###########################################################################
    # Build processes
    ###########################################################################
    def try_build(self):
        try:
            if self.try_build_sh():
                return
        except NotFound:
            pass
        try:
            if self.try_cmake():
                return
        except NotFound:
            pass
        try:
            if self.try_make():
                return
        except NotFound:
            logger.exception("Unable to build anything for %r", self._root_path)

    def try_cmake(self):
        top_level_cmakelist = self.find_first_file("CMakeLists.txt")
        if top_level_cmakelist is None:
            raise NotFound("No CMakeLists.txt present")
        # XXX: Maybe we should create a build directory? The MANIFEST is
        # hardcoded to know where handouts live though so for the moment we'll
        # just do everything in the challenge root and hope nobody requires a
        # separate build tree...
        cmake_srcdir = os.path.dirname(top_level_cmakelist)
        return self._try_somewhere(("cmake", "."), cmake_srcdir)

    def try_make(self):
        top_level_makefile = self.find_first_file("Makefile")
        if top_level_makefile is None:
            raise NotFound("No Makefile present")
        makefile_dir = os.path.dirname(top_level_makefile)
        return self._try_somewhere(("make", ), makefile_dir)

    def try_build_sh(self):
        build_sh = self.find_first_file("build.sh")
        if build_sh is None:
            raise NotFound("No build.sh")
        build_dir = os.path.dirname(build_sh)
        return self._try_somewhere((build_sh, ), build_dir)

    @staticmethod
    def _try_somewhere(cmd, cwd):
        logger.info(cmd)
        logger.info(cwd)
        try:
            subprocess.check_output(
                cmd, cwd=cwd, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as exc:
            logger.warn(
                "Unable to run %r at %r: %s", cmd, cwd, exc
            )
            logger.info(exc.stdout)
        else:
            return True

    def __repr__(self):
        s = '''{name}
    Category: {category} 
    Tags: {tags}
        '''.format(name = self.name,
                category=self.category,
                tags = self.tags)
        return s
