import os
import sys
import glob
import logging
import zipfile
import io
import argparse

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR, 'lib')
sys.path.append(LIB_DIR)

from challenge import Challenge

def discover_manifests(paths, challenge_obj=False, need_handouts=True):
    for path in paths:
        logging.info(f'searching for MANIFEST.yml files in {path}')
        for manifest_path in glob.iglob(f'{os.path.abspath(path)}/**/MANIFEST.yml*', recursive=True):
            logging.info(f'found {manifest_path}')
            if challenge_obj:
                yield from Challenge.from_manifest(manifest_path, need_handouts=False)
            else:
                yield os.path.realpath(manifest_path)


def unzip_unzips(byte_stream, directory):
    z = zipfile.ZipFile(byte_stream)
    for item in z.infolist():
        if item.is_dir():
            continue
        with z.open(item.filename) as b:
            z1 = zipfile.ZipFile(io.BytesIO(b.read()))
            for item1 in z1.infolist():
                with z1.open(item1.filename) as b1:
                    os.makedirs(os.path.dirname(os.path.join(directory.name, item1.filename)), exist_ok=True)
                    with open(os.path.join(directory.name, item1.filename), 'wb') as out:
                        out.write(b1.read())


class kv_dict_append_action(argparse.Action):
    """
    argparse action to split an argument from KEY=VALUE and append to a dictionary.
    """
    def __call__(self, parser, args, values, option_string=None):
        try:
            (k, v) = values.split("=", 2)
        except ValueError as ex:
            raise argparse.ArgumentError(self, f"could not parse argument \"{values[0]}\" as k=v format")
        d = getattr(args, self.dest) or {}
        d[k] = v
        setattr(args, self.dest, d)