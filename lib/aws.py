import os.path
import time
import logging
import terraform
import time
import socket
import select
import sys
import subprocess
import boto3
import requests
import json
import copy
from pathlib import Path
import _kubectl
import ctfd_admin_pass

CTFD_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','infra', 'ctfd')
TERRAFORM_CTFD_DIR=os.path.join(CTFD_DIR, 'terraform')

TERRAFORM_CHALLENGES_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','infra', 'challenges')
MON_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','infra', 'mon', 'aws')

TERRAFORM_EKS_DIR=os.path.join(TERRAFORM_CHALLENGES_DIR, 'eks')
TERRAFORM_EKS_WINDOWS_DIR=os.path.join(TERRAFORM_EKS_DIR, 'windows')

CONFIG_MAP_FILENAME=os.path.join(TERRAFORM_EKS_DIR, 'configmap.yml')

BIN_DIR=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..','bin')
HOME = str(Path.home())

KUBECONFIG_DIR = os.path.join(HOME, '.kube')
KUBECONFIG = os.path.join(KUBECONFIG_DIR, 'config')

AWS_PROG=['aws']

# used by spin up script for challenge
# class Kubectl(_kubectl.Kubectl):
#     def __init__(self):
#         super(_kubectl.Kubectl, self).__init__()

kubectl = _kubectl.Kubectl(k3s=False)

# aws can reuse the same port numbers for challenges since they get their own dns/ip
reuse_ports = True

def get_users():
    args = ['iam', 'list-users', '--out', 'json']
    result = subprocess.check_output(AWS_PROG + args)
    return json.loads(result)

def _get_admin_pass():    
    ctfd_password = terraform.output(TERRAFORM_CTFD_DIR, 'ctfd_password', True)

    ctfd_admin_pass.set_pass(ctfd_password['result'])

def _download_file(url, dir):
    filename = url.rsplit('/', 1)[1]
    r = requests.get(url, allow_redirects=True)
    open(os.path.join(dir, filename), 'wb').write(r.content)


def _populate_kubectl(deployment_dir):
    logging.warn(f"This script will overwrite {KUBECONFIG}. Press ENTER or Ctrl-C in the next 10 seconds to stop the script so you can back up your current {KUBECONFIG}")
    i, o, e = select.select( [sys.stdin], [], [], 10 )
    if (i):
        logging.warn("exiting")
        exit(0)    
    logging.warn("No input received. Continuing...")

    try:
        kubeconfig_output = terraform.output(deployment_dir, 'kubeconfig', False)
        os.makedirs(KUBECONFIG_DIR, exist_ok=True)
        with os.fdopen(os.open(KUBECONFIG, os.O_WRONLY | os.O_CREAT | os.O_TRUNC, 0o600), 'wb') as kubeconfig:
            kubeconfig.write(kubeconfig_output)
    except subprocess.CalledProcessError:
        pass
    try:
        config_map_aws_auth_output = terraform.output(deployment_dir, 'config_map_aws_auth', False)
        with open(CONFIG_MAP_FILENAME, 'wb') as configmap:
            configmap.write(config_map_aws_auth_output)
    except subprocess.CalledProcessError:
        pass


def start_ctfd(deployment_dir, config, verbose=False):
    logging.info("Issuing terraform apply command")
    terraform.apply(TERRAFORM_CTFD_DIR, config['filename'], vars=config['vars'], verbose=verbose)
    _get_admin_pass()

    # wait for dns to propagate
    logging.info("Waiting for DNS to propagate")
    for i in range(1, 181, 3):
        try:
            dns = socket.gethostbyname(config['ctf_domain'])
            logging.info("DNS is ready!")
            break
        except socket.gaierror as exc:
            logging.warn(exc)
        time.sleep(i)

    return "https://{}/".format(config['ctf_domain'])

def state_ctfd(deployment_dir, config, verbose=False):
    logging.info("Issuing terraform state command to get remote state")
    terraform.state_pull(TERRAFORM_CTFD_DIR)
    logging.info("Issuing terraform output command to get remote state")
    _get_admin_pass()
    logging.info(terraform.output(TERRAFORM_CTFD_DIR))

def stop_ctfd(deployment_dir, config, verbose=False):
    purge_ctfd(deployment_dir, config)

def purge_ctfd(deployment_dir, config, verbose=False):
    logging.info("Issuing terraform destroy command")
    terraform.destroy(TERRAFORM_CTFD_DIR, config['filename'], vars=config['vars'])

def start_challenges(config, verbose=False):
    eks_terraform_config_vars = copy.deepcopy(config['vars'])
    # Give all account users system:masters perms on the cluster
    # yep :(
    users = []
    for user in get_users()['Users']:
        users.append(f'''{{
            userarn  = "{user['Arn']}"
            username = "{user['UserName']}"
            groups   = ["system:masters"]
            }}'''
        )
    eks_terraform_config_vars['eks_users'] = '[' + ',\n'.join(users) + ']'

    # spin up eks without windows nodes
    extra_vars = {
        'eks_autoscaling_group_windows_min_size': '0',
        'eks_autoscaling_group_windows_max_size': '0',
        'eks_autoscaling_group_windows_desired_capacity': '0'
    }
    extra_vars.update(eks_terraform_config_vars)

    logging.info("Going to spin up EKS.")
    terraform.apply(TERRAFORM_EKS_DIR, config['filename'], vars=extra_vars, verbose=verbose)

    # SETUP KUBECTL CONFIG
    _populate_kubectl(TERRAFORM_EKS_DIR)

    # do windows EKS setup if required
    if config['eks_autoscaling_group_windows_max_size'] > 0:
        logging.info("Adding windows support to EKS.")
        logging.info("Following https://docs.aws.amazon.com/eks/latest/userguide/windows-support.html#enable-windows-support")

        logging.info("Deploying the VPC resource controller to the cluster.")
        vpc_resource_controller = 'https://amazon-eks.s3-us-west-2.amazonaws.com/manifests/{}/vpc-resource-controller/latest/vpc-resource-controller.yaml'.format(config['aws_region'])
        kubectl.apply(vpc_resource_controller)

        files = [
            'https://amazon-eks.s3-us-west-2.amazonaws.com/manifests/{}/vpc-admission-webhook/latest/webhook-create-signed-cert.sh'.format(config['aws_region']),
            'https://amazon-eks.s3-us-west-2.amazonaws.com/manifests/{}/vpc-admission-webhook/latest/webhook-patch-ca-bundle.sh'.format(config['aws_region']),
            'https://amazon-eks.s3-us-west-2.amazonaws.com/manifests/{}/vpc-admission-webhook/latest/vpc-admission-webhook-deployment.yaml'.format(config['aws_region'])
        ]
        for f in files:
            _download_file(f, TERRAFORM_EKS_WINDOWS_DIR)
        os.chmod(os.path.join(TERRAFORM_EKS_WINDOWS_DIR, 'webhook-create-signed-cert.sh'), 0o744)
        os.chmod(os.path.join(TERRAFORM_EKS_WINDOWS_DIR, 'webhook-patch-ca-bundle.sh'), 0o744)

        logging.info("Generating a secret for secure communication.")
        result = subprocess.check_output('./webhook-create-signed-cert.sh', cwd=TERRAFORM_EKS_WINDOWS_DIR)

        logging.info("Configuring VPC admission webhook and creating a deployment file for it.")
        cmd = "cat ./vpc-admission-webhook-deployment.yaml | ./webhook-patch-ca-bundle.sh"
        ps = subprocess.Popen(cmd, shell=True, cwd=TERRAFORM_EKS_WINDOWS_DIR, stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        output = ps.communicate()[0]
        with open(os.path.join(TERRAFORM_EKS_WINDOWS_DIR, 'vpc-admission-webhook.yaml'), 'wb') as out_file:
            out_file.write(output)

        logging.info("Deploying the VPC admission webhook.")
        kubectl.apply(os.path.join(TERRAFORM_EKS_WINDOWS_DIR, 'vpc-admission-webhook.yaml'))

        logging.info("Going to spin up EKS windows nodes.")
        terraform.apply(TERRAFORM_EKS_DIR, config['filename'], vars=eks_terraform_config_vars, verbose=verbose)

    # spin up kubernetes provider and extras
    logging.info("Going to spin up kubernetes services. external-dns and cluster-autoscaler.")
    terraform.apply(TERRAFORM_CHALLENGES_DIR, config['filename'], vars=config['vars'], verbose=verbose)  
    kubectl.find_or_prompt_docker_secret("regcred", ns="challenges")

    terraform.apply(MON_DIR, config['filename'], vars=config['vars'], verbose=verbose)

def stop_challenges(config):
    terraform.destroy(MON_DIR, config['filename'], vars=config['vars'])
    kubectl.delete_all_services(ns='challenges')
    kubectl.delete_all_pods(ns='challenges')
    kubectl.delete_all_services(ns='monitoring')
    kubectl.delete_all_pods(ns='monitoring')
    kubectl.delete_all_services(ns='kube-system')
    kubectl.delete_all_pods(ns='kube-system')
    logging.info("Waiting for Load Balancers to be deleted.")
    for i in range(1, 31, 3):
        client = boto3.client('elbv2')
        lbs = client.describe_load_balancers()
        # no load balancers
        if not lbs['LoadBalancers']:
            break
        # only a CTFd load balancer
        if len(lbs['LoadBalancers']) == 1 and 'ctfd' in lbs['LoadBalancers'][0]['LoadBalancerName']:
            break
        time.sleep(i)
    else:
        print("Failed to delete Load Balancers. You will need manual intervention as the terraforms will not be able to clean up properly")
        input('Press enter when you are ready to continue.')

    terraform.destroy(TERRAFORM_CHALLENGES_DIR, config['filename'], vars=config['vars'])
    # refresh needed to solve: https://github.com/terraform-aws-modules/terraform-aws-eks/issues/1162
    terraform.refresh(TERRAFORM_EKS_DIR, config['filename'], vars=config['vars'])
    terraform.destroy(TERRAFORM_EKS_DIR, config['filename'], vars=config['vars'])

def state_challenges(config):
    logging.info("Issuing terraform output command to get remote state for eks")
    logging.info(terraform.output(TERRAFORM_EKS_DIR))
    # SETUP KUBECTL CONFIG
    _populate_kubectl(TERRAFORM_EKS_DIR)

    logging.info("Issuing terraform output command to get remote state for kubernetes")
    logging.info(terraform.output(TERRAFORM_CHALLENGES_DIR))

    logging.info("Issuing terraform output command to get remote state for monitoring")
    logging.info(terraform.output(MON_DIR))
