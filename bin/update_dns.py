#!/usr/bin/env python

import argparse
import json
import socket

import boto3
import botocore.exceptions

CYBEARS_ZONE = None

def load_spec_ips(specs):
    """ Load a kubernetes spec and yield IP/Domains """
    for item in specs["items"]:
        metadata = item["metadata"]
        spec = item["spec"]
        assert metadata
        assert spec
        if spec["type"] == "LoadBalancer":
            try:
                ip = item["status"]["loadBalancer"]["ingress"][0]["ip"]
            except KeyError:
                try:
                    ip = socket.gethostbyname_ex(item["status"]["loadBalancer"]["ingress"][0]["hostname"])[2]
                except KeyError:
                    continue
            domain = metadata["annotations"]['cybears_dns']
            
            yield ip, domain

def load_spec_cnames(specs):
    """ Load a kubernetes spec and yield CNAME/Domains """
    for item in specs["items"]:
        metadata = item["metadata"]
        spec = item["spec"]
        assert metadata
        assert spec
        if spec["type"] == "LoadBalancer":
            try:
                cname = item["status"]["loadBalancer"]["ingress"][0]["hostname"]
            except KeyError:
                continue
            domain = metadata["annotations"]['cybears_dns']
            
            yield cname, domain

def update_domain_ip(ip, domain, zone, delete=False):
    """ Use route53 to update IP -> domain mappings """
    client = boto3.client('route53')
    action = 'DELETE' if delete else 'UPSERT'
    change = {
        'Comment': 'Update CTF challenges',
        'Changes': [
            {
                'Action': action,
                'ResourceRecordSet': {
                    'Name': domain,
                    'Type': 'A',
                    'TTL': 60,
                    'ResourceRecords': [ {'Value': i} for i in ip],
                }
            }
        ]
    }
    client.change_resource_record_sets(HostedZoneId=zone, ChangeBatch=change)

def update_domain_cname(cnames, domain, zone, delete=False):
    """ Use route53 to update CNAME -> domain mappings """
    client = boto3.client('route53')
    action = 'DELETE' if delete else 'UPSERT'
    change = {
        'Comment': 'Update CTF challenges',
        'Changes': [
            {
                'Action': action,
                'ResourceRecordSet': {
                    'Name': domain,
                    'Type': 'CNAME',
                    'TTL': 60,
                    'ResourceRecords': [ {'Value': i} for i in cnames],
                }
            }
        ]
    }
    try:
        client.change_resource_record_sets(HostedZoneId=zone, ChangeBatch=change)
    except botocore.exceptions.ClientError as exc:
        print("[!] {}".format(exc))

def create_records_from_spec(services_path, zone, delete=False):
    """ Given a path to a JSON kubernetes services spec, update the domain names associated with the load balancers """
    with open(services_path) as f:
        services = json.loads(f.read())
    #for ip, domain in load_spec_ips(services):
    #    if not delete:
    #        print("[+] {} -> {}".format(domain, ip))
    #    else:
    #        print("[-] {} -> {}".format(domain, ip))
    #    update_domain_cname([ip], domain, zone, delete)
    for cname, domain in load_spec_cnames(services):
        if not delete:
            print("[+] {} -> {}".format(domain, cname))
        else:
            print("[-] {} -> {}".format(domain, cname))
        update_domain_cname([cname], domain, zone, delete)
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-z", "--zone-id", required=True, help="The Cybears CTF zone ID from route 53")
    parser.add_argument('-j', '--spec-json', required=True, help="Path to the services spec JSON. Output from `kubectl get services -o json`")
    parser.add_argument('-d', '--delete', action='store_true', help='Deletes records instead of creating them')
    args = parser.parse_args()
    create_records_from_spec(args.spec_json, args.zone_id, args.delete)
