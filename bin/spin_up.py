#!/usr/bin/env python3
import argparse
import importlib
import os
import os.path
import pathlib
import logging
import sys
import spin_up_challenges
import spin_up_ctfd

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BASE_DIR=os.path.normpath(os.path.join(SCRIPT_DIR, '..'))
LIB_DIR=os.path.join(BASE_DIR,'lib')
sys.path.append(LIB_DIR)
import utils


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.set_defaults(action=None)
    parser.set_defaults(platform=None)
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose logging')
    parser.add_argument('--hostname', nargs=1, default=None, help='Hostname to use in challenge descriptions. Useful for local deployments')
    parser.add_argument(
        "--config", action="store",
        help="JSON configuration file for the ctf",
        type=argparse.FileType('r'),
        required=True
    )
    parser.add_argument(
        "--var", action=utils.kv_dict_append_action,
        metavar="'VAR=VALUE'",
        help='set or override individual config variables'
    )
    # We add top level subparsers to define which platform we're using
    sps = parser.add_subparsers()
    k3s_sp = sps.add_parser("k3s", help="Use k3s kubectl")
    k3s_sp.set_defaults(platform="k3s")
    aws_sp = sps.add_parser("aws", help="Use aws via terraform")
    aws_sp.set_defaults(platform="aws")
    
    # And then add action subparsers to each of those so that we have
    # reasonable natural feeling `./script <platform> <action>` command lines
    for platform in (k3s_sp, aws_sp):
        # Subparsers for the actual actions we support
        sps = platform.add_subparsers()
        start_sp = sps.add_parser("start", help="Start")
        stop_sp = sps.add_parser("stop", help="Stop")
        purge_sp = sps.add_parser("purge", help="Stop and purge state")
        state_sp = sps.add_parser("state", help="Get the config form the remote state. Only useful for aws(terraform)")

    # parse args (and throw away) for those that we support both challenge and ctfd spinup.
    parser.parse_args()
    challenge_args = spin_up_challenges.parse_args()
    ctfd_args = spin_up_ctfd.parse_args()
    try:
        challenge_args.action(challenge_args.platform, challenge_args.config, challenge_args.verbose)
        if ctfd_args.hostname:
            ctfd_args.config['hostname'] = ctfd_args.hostname[0]
        ctfd_args.action(ctfd_args.platform,  os.path.join(BASE_DIR, ctfd_args.config['ctfd_dir']), ctfd_args.config)
    except ModuleNotFoundError as exc:
        raise ModuleNotFoundError(
            "{} platform is not supported".format(challenge_args.platform)
        )
