#!/usr/bin/env python

import argparse
from pwn import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote', help='The host:port of the remote to connect to')
    args = parser.parse_args()

    r = remote(*args.remote.split(':'))
    flag = r.recvall()
    log.success("Flag: {}".format(flag))
    assert b'cybears{TESTFLAGTESTFLAG}' in flag
