#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import sys

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
parser.add_argument('--verbose', '-v', action='store_true', default=default_verbose, help='Enable more verbose logging')
args = parser.parse_args()

if args.verbose:
    context.log_level = 'debug'

# Change this to point to your binary if you have one
target = './chal.py'

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!
if args.remote:
    log.info("Solving against server")
    p = remote(*args.remote.split(':'))
else:
    log.info("Solving against local binary")
    if not os.path.isfile(target):
        log.error("Binary %s does not exist! Have you built the challenge?", target)
        sys.exit(1)
    p = process(target)

# Now we solve the challenge using
# pwntools. In this case the flag is just printed
# to stdout so we read until the challenge exits.
output = p.recvall()

# and the we iterate through the output to find the
# flag
flag = None
for line in output.splitlines():
    # We're outputting text, so decode from bytes to strings
    line = line.decode('utf-8')
    if 'cybears{' in line:
        flag = line.strip()

# Now we assert our flag was found.
# The script should exit with 0 if it solved the challenge
# and got the flag and it should return an error code
# if it could not. This is an easy way to fail.
assert '<flag goes here>' in flag

# If we're all good we print it and exit cleanly with a 0
log.success("The flag is: %s", flag)
