DOCKER := "$(shell command -v docker)"
# Work out if we should can have sudo available
ifeq (, $(shell which sudo))
  $(warn "No sudo in $(PATH), assuming we can just run docker without it")
  SUDO := ""
else
  ifeq (,$(shell "$(DOCKER)" info))
    SUDO := "sudo"
  else
    SUDO :=
  endif
endif

all: outside_docker test package

# We define two high level targets for inside and outside docker
outside_docker: run_builder build_runner
inside_docker: release debug

# We need a phony target so we can refer to the challenge binary in rules - the
# binary is an output of `run_builder` which occurs if we're `outside_docker`
# or if we run `inside_docker` so we don't define a hard and fast dependency
.PHONY: call_to_arms

# Here is the process for building the docker builder and then using it to
# build the actual challenge binary
build_builder:
	$(SUDO) "$(DOCKER)" build -t call-to-arms-builder -f ./docker/build/Dockerfile .
run_builder: build_builder
	$(SUDO) "$(DOCKER)" run --rm -v $(CURDIR):/tmp/build call-to-arms-builder    \
		make -C /tmp/build inside_docker
# We also need to eventually build a container which will hold the challenge
build_runner: package
	$(SUDO) "$(DOCKER)" build -t call-to-arms-runner -f ./docker/run/Dockerfile .

# Here is the process for compiling the challenge - you can run this
# `inside_docker` or if you have an ARM GCC
release:
	arm-linux-gnueabi-g++ -o ./call-to-arms call-to-arms.cpp -O2
	arm-linux-gnueabi-strip -s ./call-to-arms
debug:
	arm-linux-gnueabi-g++ -o ./call-to-arms call-to-arms.cpp -O2 -DDEBUG

# We package the challenge into the handouts directory
package: call_to_arms
	mkdir -p ./handout
	cp ./assets/Dockerfile ./call-to-arms ./handout/

# We can run the solver script against the built runner container
test:
	$(SUDO) "$(DOCKER)" run -d --rm --name call-to-arms-test -p 31337:2323 call-to-arms-runner >/dev/null
	./solve.py -r localhost:31337; $(SUDO) "$(DOCKER)" kill call-to-arms-test >/dev/null

# We clean up the handout directory and the compiled challenge binary
clean:
	rm -rf ./handout/ ./call_to_arms
