/**
 * @brief Wheel of Fish
 **/
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <stdbool.h>

// Cheat mode
// #define CTF_DEBUG


typedef struct {
    char * name;
} Fish;

#define NUMBER_OF_FISH      12
const Fish g_Fish[NUMBER_OF_FISH] = {
    { "Barramundi" },
    { "Red Snapper" },
    { "Carp" },
    { "Trout" },
    { "Bluefish"},
    { "Eel" },
    { "Perch" },
    { "Tuna" },
    { "Herring" },
    { "Flounder" },
    { "Cod" },
    { "Halibut" },
};

// Structure to force stack layout
// (otherwise the stack cookie protection may reorder the stack variables)
typedef struct {
    char name[8];
    uint32_t seed;
    uint32_t spins;
} StackLayout_t;


bool PromptYesNo(const char * prompt)
{
    char c = 0;
    printf("%s [y/n]: ", prompt);
    while (1)
    {
        c = getchar();
        if (tolower(c) == 'y')
        {
            return true;
        }
        else if (tolower(c) == 'n')
        {
            return false;
        }
    }
}

void PrintFlag()
{
    char buffer[128];
    FILE * flag_file = fopen("/noob/flag.txt", "r");
    if (flag_file)
    {
        fread(buffer, 1, sizeof(buffer), flag_file);
        fclose(flag_file);
        printf("A flag! %s\n", buffer);
    }
    else
    {
        printf("There was a problem retrieving the flag. Please ask your local cybear for assistance\n");
    }
}


int main(int argc, char* argv[])
{
    StackLayout_t s = { 0 };

    s.seed = (uint32_t)time(0);
    s.spins = 3;

#ifdef CTF_DEBUG
    printf("DEBUG: Enter random seed:");
    scanf("%d", &s.seed);
#endif // CTF_DEBUG

    printf("WHEEL! OF! FISH!\n");
    printf("Enter your name: ");

    // BUG: use this overflow to set the random number seed
    scanf("%16s", &s.name);

#ifdef CTF_DEBUG
    printf("DEBUG: Seed is %d\n", s.seed);
#endif // CTF_DEBUG

    printf("Welcome %s! It's time to play Wheel Of Fish!\n", s.name);

    srand(s.seed);

    while (s.spins > 0)
    {
        // spin the wheel
        uint32_t spin = rand() % NUMBER_OF_FISH;

        printf("Your spin: %s\n", g_Fish[spin].name);

        if (strcmp(g_Fish[spin].name, "Red Snapper") == 0)
        {
            printf("Ooh! A red snapper! Mmm, very tasty!\n");
            printf("Okay, %s. Listen carefully. You can hold onto your red snapper, or you can trade it all in for what's in the box.\n", s.name);
            bool result = PromptYesNo("Would you like to take the box?");
            if (result)
            {
                printf("Let's look under the box!\n");
                uint32_t whats_in_the_box = rand() % 50;
                if (whats_in_the_box == 3)
                {
                    // a flag
                    PrintFlag();
                }
                else
                {
                    printf(".... NOTHING! ABSOLUTELY NOTHING!\n");
                }
                return 0;
            }
        }
        else
        {
            bool spin_again = PromptYesNo("Would you like to spin again?");
            if (!spin_again)
            {
                printf("Thanks for playing WHEEL OF FISH!\n");
                return 1;
            }
        }
        s.spins--;
    }

    if (s.spins == 0)
    {
        printf("You did not win your weight in fish. Thanks for playing WHEEL OF FISH!\n");
    }
    return 0;

}