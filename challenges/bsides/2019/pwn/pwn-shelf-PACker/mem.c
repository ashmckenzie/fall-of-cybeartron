#include "mem.h"
#include "pac.h"

void* pac_alloc(size_t size) {
    PACIASP();
    void *ret = NULL;
    AUTIAC_R(&_malloc, ret, size);
    PACIA(&ret);
    AUTIASP(ret);
}
void pac_free(void* ptr) {
    PACIASP();
    AUTIA(&ptr)
    AUTIAC(&_free, ptr);
    AUTIASP();
}
