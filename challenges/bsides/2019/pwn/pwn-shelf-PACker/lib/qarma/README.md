== Prerequisites
   1. cmake
   1. ninja


== Configure
    cmake -G Ninja .

== Build
    ninja -C .
