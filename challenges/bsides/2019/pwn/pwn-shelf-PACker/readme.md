=== Shelf PACker

x is a maturing autobot and has taken up a part time job at the local supermarket. (s)he works the night fill but has a hard time remembering to what aisle certain products belong, so (s)he's developed this app to keep track of the tricky ones.

Hes/Shes seen his hipster cousins have some cool hardware in their pockets, x could never afford that on his salary but thinks he can replicate some of the sweet new protections on his ancient architecture.


== Build
requires:
   1. cmake
   1. libbsd-dev
   1. ninja-build (optional)

Be sure to do a Release build if you want the solver to work! It uses a couple
of fixed offsets to work out initial information and will probably blow up
claiming there was a NULL byte in the key if you do it wrong.

`cmake -DCMAKE_BUILD_TYPE="Release" .` (add `-G Ninja` if you have it)


== Hints

   1. Contains 3 bugs
      1. signed comparison on aisle index for get and set. Allows for negative index to pass size limit check.
      1. add item dont enforce null allowing for leaking of pointer back to aisle array

   1. https://github.com/dkales/qarma64-python/blob/master/qarma.py
      1. Made to work with this library. Not sure its completely valid - certainly decryption looks broken in this lib.

   1. Use index get bug to leak qarma key and tweak.
   1. Use index set bug to overwrite supermarket_name pointer using key and tweak from above with qarma64 to forge valid pointers.
      1. Use printName to read arbitrary memory.
   1. ...
   1. profit
