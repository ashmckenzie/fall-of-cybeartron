#ifndef PAC_H
#define PAC_H
#include <stdint.h>
#include <stdio.h>
#include <dlfcn.h>
#include <stdbool.h>
#include "lib/qarma/qarma.h"

typedef int (*PRINTF)(const char *format, ...);
typedef int (*FPUTS)(const char *str, FILE *stream);
typedef int (*SCANF)(const char *format, ...);
typedef void* (*MALLOC)(size_t size);
typedef void (*FREE)(void *ptr);
typedef int (*PUTS)(const char *);
typedef char* (*FGETS)(char *str, int n, FILE *stream);
typedef void (*SETBUF)(FILE *stream, char *buffer);
typedef void (*EXIT)(int status);
typedef char* (*WRITE)(void *str, int n, FILE *stream);
typedef char* (*READ)(void *str, int n, FILE *stream);
FPUTS _fputs;
SETBUF xsetbuf;
PRINTF _printf;
READ _read;
SCANF _scanf;
FGETS _fgets;
FREE _free;
PUTS _puts;
EXIT __exit;
MALLOC _malloc;
WRITE _write;



// global key, This will be important for an attacker to get
typedef struct {
    __uint128_t qarma_key;
    uint64_t qarma_tweak;
} KEY_DATA;
extern KEY_DATA key_data;

uint64_t temp;
uint64_t temp2;
uint64_t cipher_pointer;
uint64_t cipher_pointer_fail;
uint64_t pac_pointer_fail;


// Setup PAC on a pointer and return
#define PACIA(pointer) \
    *(uint64_t*)pointer |= (qarma64((uint8_t *)pointer, (uint8_t *)&key_data.qarma_tweak, (uint8_t *)&key_data.qarma_key, true, 5) & 0xffff000000000000);

// Setup PAC for a function pointer
#define PACIAC(pointer, function) \
    pointer = function; \
    PACIA(&pointer);


// Setup PAC for a return address (saved stack pointer)
#define PACIASP() \
 uint64_t saved_sp; \
\
 __asm__ __volatile__( \
   "movq 8(%%rbp), %0\n\t" \
   : "=r"(saved_sp) \
 ); \
 PACIA(&saved_sp)

// Validate PAC on a pointer
#define AUTIA(pointer) \
    temp = *(uint64_t*)pointer & 0xffffffffffff; \
    cipher_pointer = qarma64((uint8_t *)&temp, (uint8_t *)&key_data.qarma_tweak, (uint8_t *)&key_data.qarma_key, true, 5); \
    /* Crazy use of temps to avoid these being optimised out in relaese builds*/ \
    temp = *(uint64_t*)pointer & 0xffff000000000000; \
    temp2 = cipher_pointer & 0xffff000000000000; \
    if((temp2) == temp) { \
        temp2 =  *(uint64_t*)pointer & 0xffffffffffff; \
        /* blank asm stops the following assignment being optimised out in relaese builds*/ \
        __asm__ __volatile__( " " ); \
        *(uint64_t*)pointer = temp2; \
    } else { \
        pac_pointer_fail = *(uint64_t*)pointer; \
        cipher_pointer_fail = cipher_pointer; \
        *(uint64_t*)pointer = 0x4141414141414141; \
    } \
    /* blank asm stops the previous assignment being optimised out in relaese builds*/ \
    __asm__ __volatile__( " " );

// Validate PAC for a return address (saved stack pointer)
#define AUTIASP(x) \
 AUTIA(&saved_sp) \
 __asm__ __volatile__( \
   "movq %0, 8(%%rbp)\n\t" \
   : "=r"(saved_sp) \
 ); \
 return x;

// Validate PAC for a non value eturning function pointer and call
#define AUTIAC(call_target, ... ) \
    AUTIA(call_target) \
    (*call_target)(__VA_ARGS__); \
    PACIA(call_target)

// Validate PAC for a value-returning function pointer and call
#define AUTIAC_R(call_target, return_var, ... ) \
    AUTIA(call_target) \
    return_var = (*call_target)(__VA_ARGS__); \
    PACIA(call_target)
#endif //PAC_H

#define DOPACTHING(pointer, x) \
    AUTIA(&pointer) \
    do { \
      x \
    } while(0); \
    PACIA(&pointer);


void init(void);
