#include <stdio.h>
#include <stdlib.h>

#define RNG_MAX ((1U << 31) - 1)
int g_Seed = 0;

int nextRand()
{
    g_Seed = (g_Seed * 1103515245 + 12345) & RNG_MAX;
    return g_Seed;
}

int main(int argc, char* argv[])
{
    if (argc < 4)
    {
        printf("usage: %s output_file size seed\n", argv[0]);
        return 1;
    }
    
    const char * output_file_path = argv[1];
    int size = atoi(argv[2]);
    int seed = atoi(argv[3]);
    
    g_Seed = seed;
    
    char * random_data = malloc(size);
    if (random_data)
    {
        for(int i = 0; i < size; i++)
        {
            random_data[i] = nextRand() & 0xFF;
        }
        
        FILE * output_file = fopen(output_file_path, "wb");
        if (output_file)
        {
            fwrite(random_data, size, 1, output_file);
            
            fclose(output_file);
        }
        else
        {
            printf("Could not open %s\n", output_file_path);
            return 2;
        }
        
        free(random_data);
    }
    else
    {
        printf("Out of memory");
        return 2;
    }
    
    return 0;
}