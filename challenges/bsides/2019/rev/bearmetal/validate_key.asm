    ; Validate key: "ZvbXrp1-"

    ; x[6]+x[4]+x[3] = 251
    xor bx, bx
    movzx dx, byte [si+6]
    mov bl, byte[si+4]
    add dx, bx
    mov bl, byte[si+3]
    add dx, bx
    cmp dx, 251
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[4]+x[1] = 350
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+4]
    add dx, bx
    mov bl, byte[si+1]
    add dx, bx
    cmp dx, 350
    jne validate_key_fail
    
    call look_busy

    ; x[7]+x[2]+x[1] = 261
    xor bx, bx
    movzx dx, byte [si+7]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+1]
    add dx, bx
    cmp dx, 261
    jne validate_key_fail
    
    call look_busy

    ; x[6]+x[6]+x[1]+x[4] = 330
    xor bx, bx
    movzx dx, byte [si+6]
    mov bl, byte[si+6]
    add dx, bx
    mov bl, byte[si+1]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    cmp dx, 330
    jne validate_key_fail
    
    call look_busy

    ; x[4]+x[2]+x[6]+x[5] = 373
    xor bx, bx
    movzx dx, byte [si+4]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+6]
    add dx, bx
    mov bl, byte[si+5]
    add dx, bx
    cmp dx, 373
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[2]+x[0] = 306
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+0]
    add dx, bx
    cmp dx, 306
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[3]+x[2]+x[7] = 349
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+3]
    add dx, bx
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+7]
    add dx, bx
    cmp dx, 349
    jne validate_key_fail
    
    call look_busy

    ; x[3]+x[6]+x[4]+x[4] = 365
    xor bx, bx
    movzx dx, byte [si+3]
    mov bl, byte[si+6]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    cmp dx, 365
    jne validate_key_fail
    
    call look_busy

    ; x[7]+x[6]+x[3]+x[7] = 227
    xor bx, bx
    movzx dx, byte [si+7]
    mov bl, byte[si+6]
    add dx, bx
    mov bl, byte[si+3]
    add dx, bx
    mov bl, byte[si+7]
    add dx, bx
    cmp dx, 227
    jne validate_key_fail
    
    call look_busy

    ; x[4]+x[3]+x[0]+x[2] = 390
    xor bx, bx
    movzx dx, byte [si+4]
    mov bl, byte[si+3]
    add dx, bx
    mov bl, byte[si+0]
    add dx, bx
    mov bl, byte[si+2]
    add dx, bx
    cmp dx, 390
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[2]+x[4] = 330
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    cmp dx, 330
    jne validate_key_fail
    
    call look_busy

    ; x[7]+x[2]+x[6]+x[7] = 237
    xor bx, bx
    movzx dx, byte [si+7]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+6]
    add dx, bx
    mov bl, byte[si+7]
    add dx, bx
    cmp dx, 237
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[2]+x[1]+x[0] = 424
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+2]
    add dx, bx
    mov bl, byte[si+1]
    add dx, bx
    mov bl, byte[si+0]
    add dx, bx
    cmp dx, 424
    jne validate_key_fail
    
    call look_busy

    ; x[2]+x[7]+x[1] = 261
    xor bx, bx
    movzx dx, byte [si+2]
    mov bl, byte[si+7]
    add dx, bx
    mov bl, byte[si+1]
    add dx, bx
    cmp dx, 261
    jne validate_key_fail
    
    call look_busy

    ; x[0]+x[4]+x[0]+x[6] = 343
    xor bx, bx
    movzx dx, byte [si+0]
    mov bl, byte[si+4]
    add dx, bx
    mov bl, byte[si+0]
    add dx, bx
    mov bl, byte[si+6]
    add dx, bx
    cmp dx, 343
    jne validate_key_fail
    
    call look_busy

    ; x[1]+x[3]+x[4]+x[4] = 434
    xor bx, bx
    movzx dx, byte [si+1]
    mov bl, byte[si+3]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    mov bl, byte[si+4]
    add dx, bx
    cmp dx, 434
    jne validate_key_fail

