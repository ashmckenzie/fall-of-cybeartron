import struct
import sys
import os.path
import shutil


# PE file constants
E_LFANEW = 0x3C
DOS_HEADER_MAGIC = 0x5a4d
PE_HEADER_MAGIC = 0x4550

PE_HEADER_NUMBER_OF_SECTIONS_OFFSET = 0x6
PE_HEADER_SIZEOFOPTIONALHEADER_OFFSET = 0x14
FILE_HEADER_SIZE = 0x18

SECTION_HEADER_SIZE = 0x28
SECTION_HEADER_SIZEOFRAWDATA_OFFSET = 0x10
SECTION_HEADER_POINTERTORAWDATA_OFFSET = 0x14

ORIGINAL_SECTION_NAME = ".ctforig"
NEW_SECTION_NAME = ".ctf\x00\x00\x00\x00"
XOR_KEY = b'GenuineTMx86'


def read_dword(data, offset):
    return struct.unpack("<l", data[offset:offset+4])[0]


def read_word(data, offset):
    return struct.unpack("<h", data[offset:offset+2])[0]


def read_fixed_str(data, offset, buffer_size):
    str_value = struct.unpack("%ds" % (buffer_size), data[offset:offset+buffer_size])[0]
    return str_value.decode("utf-8").split('\0', 1)[0]


def find_pe_section(pe_data, section_name):
    physical_address = 0
    size = 0

    if read_word(pe_data, 0) != DOS_HEADER_MAGIC:
        raise Exception("invalid DOS header")

    # Get offset to the PE header
    pe_header_offset = read_word(pe_data, E_LFANEW)

    # Validate PE header
    if read_word(pe_data, pe_header_offset) != PE_HEADER_MAGIC:
        raise Exception("invalid PE header")

    # Get number of sections
    number_of_sections = read_word(pe_data, pe_header_offset + PE_HEADER_NUMBER_OF_SECTIONS_OFFSET)
    size_of_optional_header = read_word(pe_data, pe_header_offset + PE_HEADER_SIZEOFOPTIONALHEADER_OFFSET)

    # Get first section header
    section_header = pe_header_offset + size_of_optional_header + FILE_HEADER_SIZE

    for section_id in range(0, number_of_sections):
        this_section_name = read_fixed_str(pe_data, section_header, 8)

        if section_name == this_section_name:
            size = read_dword(pe_data, section_header + SECTION_HEADER_SIZEOFRAWDATA_OFFSET)
            physical_address = read_dword(pe_data, section_header + SECTION_HEADER_POINTERTORAWDATA_OFFSET)
            break

        section_header += SECTION_HEADER_SIZE

    if physical_address != 0:
        return (section_header, physical_address, size)
    else:
        raise Exception("section not found")


def patch_binary(pe_path):

    # Load original executable
    pe_data = None
    print("Original executable: %s" % (pe_path))
    with open(pe_path, "rb") as pe_file:
        pe_data = bytearray(pe_file.read())

    # Find the section to patch
    (section_header, section_address, section_size) = find_pe_section(pe_data, ORIGINAL_SECTION_NAME)

    # XOR the section with the key
    print("Obfuscating %s section" % ORIGINAL_SECTION_NAME)
    for offset in range(section_address, section_address + section_size):
        if pe_data[offset] != 0:
            pe_data[offset] = pe_data[offset] ^ ord(XOR_KEY[offset % len(XOR_KEY)])

    # Rename the section
    # the name is the first entry in IMAGE_SECTION_HEADER
    section_name_offset = section_header
    print("Patching section name at 0x%x" % (section_name_offset))
    pe_data[section_name_offset:section_name_offset+8] = b'.ctf\x00\x00\x00\x00'

    # Backup the original binary
    backup_file = pe_path + ".bak"
    shutil.copy(pe_path, backup_file)
    print("Backup written to: %s" % (backup_file))
    
    with open(pe_path, "wb") as pe_file:
        pe_file.write(pe_data)

    print("SUCCESS!")


def show_usage():
    print("usage: %s <path-to-executable>" % (sys.argv[0]))


def main():
    if len(sys.argv) != 2:
        show_usage()
        return

    pe_path = sys.argv[1]
    if not os.path.isfile(pe_path):
        print("File %s does not exist" % (pe_path))
        return

    patch_binary(pe_path)


if __name__ == "__main__":
    main()