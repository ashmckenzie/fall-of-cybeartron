#!/bin/bash
set -o errexit
SCRIPT_DIR=${0%/*}

# Check if we need to sudo to do docker stuff
if ! docker info >/dev/null; then
    MAYBE_SUDO=sudo
fi

${MAYBE_SUDO} docker build -t womm-builder "${SCRIPT_DIR}"
${MAYBE_SUDO} docker rm womm-build ||:
${MAYBE_SUDO} docker run --name womm-build womm-builder
${MAYBE_SUDO} docker cp "womm-build:/tmp/src/export/" "${SCRIPT_DIR}"
${MAYBE_SUDO} docker rm womm-build
${MAYBE_SUDO} chown -R $(id -u):$(id -g) "${SCRIPT_DIR}/export/"
