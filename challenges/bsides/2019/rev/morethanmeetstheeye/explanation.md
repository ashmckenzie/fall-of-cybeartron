# More Than Meets The Eye

## The Idea
This challenge was created when I asked the question: "Can you still make Linux syscalls from WINE?".

The binary is pretty simple. It is a Windows PE that makes a Linux syscall (oldolduname) and then hashes the kernel version it returns and uses that as a key to decrypt the flag using the Windows BCrypt library.

The binary itself doesn't quite run. On Windows it will crash on the syscall interrupt instruction. On Linux it must be loaded using WINE. The syscall will succeed but at the time of writing this the encryption algorithm that was used, ARC4, hasn't been implemented in WINE and it will exit with an error. Even if it were the Linux kernel version is unlikely to match the version that was used to encrypt the flag and the output would be incorrect.

## The Solution

The uname syscalls populate a struct with information about the system. The binary uses the kernel version to generate the key. The oldolduname syscall was chosen because it uses a struct with space for only 8 characters for the version. Knowing that it is a Linux kernel version that is only 8 characters long severely limits the possible key space making it possible to brute force.

Other than the introduction of the Linux syscall the binary has no other tricks or obfuscation and should be fairly straightforward to reverse. The BCrypt library even uses plaintext strings to select the hashing and decryption algorithms. Finding the cryptography calls should also lead the player to the ciphertext.

Once everything is found it is a matter of trying all of the different kernel versions until one decrypts to a plaintext string that starts with the flag prefix `cybears{`.
