// rev_ctf.cpp

#include "pch.h"
#include <iostream>
#include <windows.h>
#include <stdio.h>
#include <bcrypt.h>
#include "secret.h"

#define NT_SUCCESS(Status)          (((NTSTATUS)(Status)) >= 0)
#define _UTSNAME_LENGTH 9

struct utsname {
	BYTE sysname[_UTSNAME_LENGTH];
	BYTE nodename[_UTSNAME_LENGTH];
	BYTE release[_UTSNAME_LENGTH];
	BYTE version[_UTSNAME_LENGTH];
	BYTE machine[_UTSNAME_LENGTH];
	//BYTE domain[_UTSNAME_LENGTH];
};

// Generate and return a hash from an input string
PBYTE hash(CONST PBYTE  input_string, UINT input_len, PUINT hash_size)
{
	BCRYPT_ALG_HANDLE       hAlg = NULL;
	BCRYPT_HASH_HANDLE      hHash = NULL;
	NTSTATUS                status = 0;
	DWORD                   cbData = 0,
		cbHash = 0,
		cbHashObject = 0;
	PBYTE                   pbHashObject = NULL;
	PBYTE                   pbHash = NULL;

	//open an algorithm handle
	status = BCryptOpenAlgorithmProvider(&hAlg, BCRYPT_SHA256_ALGORITHM, NULL, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	//calculate the size of the buffer to hold the hash object
	status = BCryptGetProperty(hAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbHashObject, sizeof(DWORD), &cbData, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	//allocate the hash object
	pbHashObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHashObject);
	if (NULL == pbHashObject) { goto Cleanup; }

	//calculate the length of the hash
	status = BCryptGetProperty(hAlg, BCRYPT_HASH_LENGTH, (PBYTE)&cbHash, sizeof(DWORD), &cbData, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	//allocate the hash buffer on the heap
	pbHash = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbHash);
	if (NULL == pbHash) { goto Cleanup; }

	//create a hash
	status = BCryptCreateHash(hAlg, &hHash, pbHashObject, cbHashObject, NULL, 0, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }


	//hash some data
	status = BCryptHashData(hHash, input_string, input_len, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	//close the hash
	status = BCryptFinishHash(hHash, pbHash, cbHash, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

Cleanup:

	if (hAlg) { BCryptCloseAlgorithmProvider(hAlg, 0); }

	if (hHash) { BCryptDestroyHash(hHash); }

	if (pbHashObject) { HeapFree(GetProcessHeap(), 0, pbHashObject); }

	// Everything went correctly then return the hash
	if (pbHash && NT_SUCCESS(status)) {
		if (hash_size != NULL) { *hash_size = cbHash; }
		return pbHash;
	};

	if (pbHash) { HeapFree(GetProcessHeap(), 0, pbHash); }

	return NULL;
}

// Decrypt the ciphertext using a key
PBYTE decrypt(CONST PBYTE ciphertext, UINT cipher_len, PBYTE key, UINT key_len)
{
	BCRYPT_ALG_HANDLE hAesAlg = NULL;
	BCRYPT_KEY_HANDLE hKey = NULL;
	NTSTATUS status = 0;
	DWORD cbPlainText = 0;
	DWORD  cbData = 0;
	DWORD cbKeyObject = 0;
	PBYTE pbPlainText = NULL;
	PBYTE pbKeyObject = NULL;

	// Open an algorithm handle.
	status = BCryptOpenAlgorithmProvider(&hAesAlg, BCRYPT_RC4_ALGORITHM, NULL, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	// Calculate the size of the buffer to hold the KeyObject.
	status = BCryptGetProperty(hAesAlg, BCRYPT_OBJECT_LENGTH, (PBYTE)&cbKeyObject, sizeof(DWORD), &cbData, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject) { goto Cleanup; }

	// Generate the key from supplied input key bytes.
	status = BCryptGenerateSymmetricKey(hAesAlg, &hKey, pbKeyObject, cbKeyObject, (PBYTE)key, key_len, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	// Get the expected length of the plaintext
	status = BCryptDecrypt(hKey, (PBYTE)ciphertext, cipher_len, NULL, NULL, 0, NULL, 0, &cbPlainText, 0);
	if (!NT_SUCCESS(status)) { goto Cleanup; }

	pbPlainText = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbPlainText);
	if (NULL == pbPlainText) { goto Cleanup; }

	// Use the key to decrypt the ciphertext buffer.
	status = BCryptDecrypt(hKey, (PBYTE)ciphertext, cipher_len, NULL, NULL, 0, pbPlainText, cbPlainText, &cbData, 0);


Cleanup:

	if (hAesAlg)
	{
		BCryptCloseAlgorithmProvider(hAesAlg, 0);
	}

	if (hKey)
	{
		BCryptDestroyKey(hKey);
	}

	if (pbKeyObject)
	{
		HeapFree(GetProcessHeap(), 0, pbKeyObject);
	}

	if (pbPlainText)
	{
		if (NT_SUCCESS(status))
		{
			return pbPlainText;
		}
		HeapFree(GetProcessHeap(), 0, pbPlainText);
	}

	return NULL;

}


int main(int argc, char** argv)
{
	utsname this_utsname;
	utsname* p_utsname = &this_utsname;

	// Get the utsname data
	__asm {
		mov ebx, p_utsname;	// ebx = &this_utsname
		mov eax, 0x3B;		// 0x3B - old uname
		int 0x80;
	}

	// Hash the kernel release
	UINT hash_size;
	PBYTE this_hash = hash(this_utsname.release, strlen((char *)this_utsname.release), &hash_size);

	if (this_hash != NULL)
	{
		// Decrypt the secret using the hash
		PBYTE plaintext = decrypt((PBYTE)SECRET, sizeof(SECRET), this_hash, hash_size);

		if (plaintext != NULL)
		{
			// Print the plaintext
			printf("%s", plaintext);
			HeapFree(GetProcessHeap(), 0, plaintext);
		}

		HeapFree(GetProcessHeap(), 0, this_hash);
	}

	return 0;
}

