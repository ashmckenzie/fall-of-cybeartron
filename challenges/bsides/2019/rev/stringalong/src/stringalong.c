#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>

void print_flag() {
    char flag[] = { 'c', 'y', 'b', 'e', 'a', 'r', 's', '{', 's', 't', '4', 'c', 'k', '_', 'B', 'u', '1', 'l', 't', '}' };
    syscall(SYS_write, 0, flag, sizeof(flag));
}

int main() {
    printf("Good luck finding my strings!");
}
