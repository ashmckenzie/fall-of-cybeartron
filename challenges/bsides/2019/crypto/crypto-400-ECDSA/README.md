crypto-400-ECDSA
=================

* _title_: Signed, Sealed and Delivered
* _points_: 400
* _tags_: crypto

## Flags
* `cybears{T4k3_th4t_pl4yst4t10n}`

## Challenge Text
```markdown
We've managed to find one of the DecepticomTSS signature servers. It seems to be sending out authenticated messages, if we could somehow sign our own messages, we would be able to spoof messages and disrupt their orders to their troops. See what you can do.

Connect to the signature server here: `nc crypto.cybears.io 3141`
```

## Attachments
* `DecepticomTSS_SignatureServer.py` : server source-code

## Hints
* Repeated nonces are a catastropic failure, is this one fine?
* Do the maths

## References
* https://crypto.stackexchange.com/questions/7904/attack-on-dsa-with-signatures-made-with-k-k1-k2
