import Crypto.Cipher.PKCS1_v1_5 as rsa_pkcs1_v1_5
import Crypto.PublicKey.RSA as RSA
import Crypto.Cipher.AES as AES
import os
from binascii import *
import json
import struct

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i', '--inputfile', type=str,  default="", help='File to Encrypt/Decrypt', required=True)
parser.add_argument('-p', '--public', type=str,  default="", help='Public Key file')
parser.add_argument('-v', '--private', type=str,  default="", help='Private Key file (to decrypt only)')
parser.add_argument('-m', '--mode', type=str,  default="encrypt", help='encrypt|decrypt')
args = parser.parse_args()

input_file = args.inputfile
public_key_file = args.public
private_key_file = args.private
mode = args.mode

## Import Public Key or use default
if public_key_file == "":
    public_key_str = '''-----BEGIN PUBLIC KEY-----
MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgHayomeY+3lTj9wxUzwK046uwx/2
0/G0BRnEhnXGjN8XILnmox/Hupo4Rkj6HlbDFVELy3glz/ISlIkhWKPAdDpLYHLE
KrRruyc6HbyrgVIuJB2cKPh5vLq9D/wt0rx26NsXyATsS3X+LlViSmV9hnsTv6Y0
6fbIBBbG8klDsZxrAgMBAAE=
-----END PUBLIC KEY-----'''
else:
    try:
        f = open(public_key_file, 'r')
        public_key_str = f.read()
        f.close()
    except:
        print("ERROR: Couldn't open public key file")
        exit(-1)

try:
    public_key = RSA.importKey(public_key_str)
except:
    print("ERROR: Couldn't open public key")
    exit(-1)

## If exists, import private key
private_key = None
if private_key_file != "":
    try:
        f = open(private_key_file, 'r')
        private_key_str = f.read()
        f.close()
        private_key = RSA.importKey(private_key_str)
    except:
        print("ERROR: Couldn't open private key")
        exit(-1)

##check modes
valid_modes = ["encrypt", "decrypt", "ENCRYPT", "DECRYPT"]
if mode not in valid_modes:
    print("ERROR: invalid mode. Select either 'encrypt' or 'decrypt'")
    exit(-1)

if mode == "decrypt" and type(private_key) == type(None):
    print("ERROR: Decrypt chosen, but no valid private key selected")
    exit(-1)

#Open input file
if input_file == "":
    print("ERROR: input_file blank")
    exit(-1)

try:
    f = open(input_file, 'rb')
    input_data = f.read()
    f.close()
except:
    print("ERROR: Could not open input_file")
    exit(-1)


def pad(data, blocksize):
    data_len_mod = len(data) % blocksize
    pad_len = blocksize - data_len_mod
    return data + pad_len.to_bytes(1, byteorder="big")*pad_len

def encrypt_data(plain, user_key, user_IV):
    a = AES.new(key = user_key, IV = user_IV, mode = AES.MODE_CBC)
    return a.encrypt(pad(plain, a.block_size))

#this decrypts the extra block appended and doesnt de-pad
def decrypt_data(cipher, user_key, user_IV):
    a = AES.new(key = user_key, IV = user_IV, mode = AES.MODE_CBC)
    return a.decrypt(cipher)

#Generate Random AES Key
#pad to AES.blocksize
#Encrypt file with AES key + IV
## format [magic = b'M4YH3M'][type = 0xAA, len = 4 bytes, data1 (len bytes)][ type = 0xEE = enc data, len = 4 bytes, data2 (len bytes) ]
## data1 = RSA_PKCS1(public_key, b'{k=b'HEXKEY', iv=b'HEXIV'})
## data2 = AES(plain, kev, iv)
#Encrypt AES Key with RSA
#Embed all in file .enc

#have decrypt function

debug = False
magic = b'M4YH3M'

if __name__ == "__main__":

    if mode == "encrypt":
        if(debug):
            print("DEBUG: encrypting")

        #generate key + iv randomly
        k = os.urandom(16)
        iv = os.urandom(16)

        #AES encrypt input file
        cipher = encrypt_data(input_data, k, iv)
        if(debug):
            print("DEBUG: cipher_aes = {}".format(cipher.hex()))

        #prepare key & iv for public key encryption
        data1_plain_dict = { "k":k.hex(), "iv" : iv.hex()}
        data1_plain_str = json.dumps(data1_plain_dict)

        #public key encrypt AES + IV
        r = rsa_pkcs1_v1_5.new(public_key)
        data1_cipher = r.encrypt(bytes(data1_plain_str, 'utf-8'))

        if(debug):
            print("DEBUG: cipher_rsa = {}".format(data1_cipher.hex()))

        #write outputfile
        data_out = b''
        data_out += magic
        data_out += b'\xAA' + struct.pack("<I", len(data1_cipher)) + data1_cipher
        data_out += b'\xEE' + struct.pack("<I", len(cipher)) + cipher

        output_filename = input_file + ".enc"

        try:
            g = open(output_filename, "wb")
            g.write(data_out)
            g.close()
        except:
            print("ERROR: Failed to write output file")
            exit(-1)

        if(debug):
            print("DEBUG: Output file successfully written - should delete original file")

        exit(0)


    if mode == "decrypt":
        if(debug):
            print("DEBUG: decrypting")

        #validate input
        ##check header
        if (input_data[0:len(magic)] != magic):
            print("ERROR: magic value not found")
            exit(-1)

        ##read in public key encrypted aes key:
        tag_pubenc = input_data[len(magic): len(magic) + 1]
        if tag_pubenc != b'\xAA':
            print("ERROR: Couldn't find public key encrypted blob - {}".format(hexlify(tag_pubenc)))
            exit(-1)
        len_pubenc = struct.unpack("<I", input_data[len(magic)+1:len(magic)+1+4])[0]

        if(len_pubenc > len(input_data)):
            print("ERROR: Problem with input length")
            exit(-1)

        data_pubenc = input_data[len(magic)+1+4: len(magic)+1+4 +len_pubenc]

        if(debug):
            print("tag: {}".format(tag_pubenc))
            print("len pubenc: {}".format(len_pubenc))
            print("data pubenc: {}".format(data_pubenc))


        #read in aes encrypted data
        tag_aesenc = input_data[len(magic)+1+4 +len_pubenc : len(magic)+1+4 +len_pubenc + 1]
        if tag_aesenc != b'\xEE':
            print("ERROR: Couldn't find aes key encrypted blob")
            exit(-1)

        len_aesenc = struct.unpack("<I", input_data[len(magic)+1+4 +len_pubenc + 1:len(magic)+1+4 +len_pubenc + 1+4])[0]

        if(len_aesenc > (len(input_data) - len_pubenc)):
            print("ERROR: Problem with input length")
            exit(-1)

        data_aesenc = input_data[len(magic)+1+4 +len_pubenc + 1+4:]

        if(len(data_aesenc) != len_aesenc):
            print("ERROR: Data length doesn't match expected value")
            exit(-1)

        #decrypt aes key
        r = rsa_pkcs1_v1_5.new(private_key)
        data1_plain = r.decrypt(data_pubenc, "DECRYPTION ERROR")

        try:
            data1_plain_dict = json.loads(data1_plain)
        except:
            print("ERROR: Json parse error - decryption failed")
            exit(-1)

        if(list(data1_plain_dict.keys()) != ['k', 'iv']):
            print("ERROR: Json parse error")
            exit(-1)

        k = unhexlify(data1_plain_dict['k'])
        iv = unhexlify(data1_plain_dict['iv'])

        #decrypt data
        data2_plain = decrypt_data(data_aesenc, k, iv)

        #output data
        output_filename = input_file + ".dec"

        try:
            g = open(output_filename, "wb")
            g.write(data2_plain)
            g.close()
        except:
            print("ERROR: Failed to write output file")
            exit(-1)

        if(debug):
            print("DEBUG: Successfully wrote decrypted output file")

        exit(0)
