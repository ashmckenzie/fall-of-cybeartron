#!/usr/bin/python3

#crypto-100-VanityRSA solution

import unittest.mock
unittest.mock.patch("time.clock", create=True).start()

from pwn import *
from Crypto.Util.number import isPrime, getPrime, GCD, inverse, getRandomNBitInteger
import Crypto.PublicKey.RSA as RSA
import os
import sys

def generatePrivateKey(bitlength=1024, e=65537, target=b'cybearsctf', debug=False):

        result = False

        putative_N = b'\xff' + target + os.urandom(bitlength//8 - len(target) - 1 )
        if(debug == True):
                print( "putative N: " + str(putative_N))
        pN = int.from_bytes(putative_N, byteorder="big")

        p = e
        while GCD(p,e) > 1:
                p = getPrime(bitlength//2)

        pQ = pN//p
        q = pQ

        while (not isPrime(q) or GCD(q,e)>1):
                q = q + 1 #technically we could check if q is odd, and then add two here... 

        N = p*q
        d = inverse(e, (p-1)*(q-1))
        R = RSA.construct((N,e,d))

        return R.exportKey()

import argparse 

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=2718, type=int, help='Remote port')
args = parser.parse_args()

hostname = args.hostname
port = args.port

if __name__ == "__main__":

	conn = remote(hostname, port)
	prompt = conn.recvuntil("finish): ")

	#print("**" + str(prompt))
	log.info("Sending RSA key...")
	rsa_key = generatePrivateKey()
	#print("**" +  rsa_key.decode("utf-8") + "\n")

	#todo: why does this need two carriage returns? 
	conn.send(rsa_key.decode("utf-8") + "\n\n")

	#todo: why does this need two conn.recv()??
	resp2 = conn.recv()
	#print("**" + resp2.decode("utf-8"))
	#log.success("Flag is " + str(resp2))
	resp2 = conn.recv()
	#print("**" + resp2.decode("utf-8"))
	log.success("Flag is " + str(resp2))
	conn.close()
	assert 'cybears{1m_t00_s3xy_f0r_th15_k3y}' in str(resp2)


