crypto-100-VanityRSA
=================

* _title_: Vanity Keys
* _points_: 100
* _tags_: crypto

## Flags
* `cybears{1m_t00_s3xy_f0r_th15_k3y}`

## Challenge Text
```markdown
Cybears comms officer cipher has identified a problem with our old communications protocol. You 
will need to regenerate all of our private keys

To ensure that these are cybertronian keys, you will need to generate a new 1024-bit RSA public/private key pair, ensuring that the string "cybearsctf" is embedded in the upper half of the RSA public modulus.  

You will need to submit your answer in X.509 format.  

As an example the integer 1205757975063083732915455 contains the byte string "TESTTEST"

Submit your 1024-bit RSA private key in X.509 format here: `nc crypto.cybears.io 2718`
```

## Attachments
*

## Hints
* X.509 format looks like
```
-----BEGIN RSA PRIVATE KEY-----
MIICXAIBAAKBgQD/VEVTVFRFU1Q2ovEr/Iaz006A38wDF1dvWScrRTWG0hdnKjjJ
efTEHFFib2gRjzRAtveEPZXPmCeQb+RQYo8clcJUBJFaii2UZXzyR6tnqAt7pdYJ
oNfs99BJvvyQhaTOqkw4rmlr8SBtTkzPXX6MI+ZpvpnCsksC8iL5KdzHMQIDAQAB
AoGBAIhOyc/Jpk94Q+4DIPNVlefsd+1vDG/eByyHkNSV1xSJljraHITa2aPUZlJB
nXI6XC/sPclYQ2EXHpAKW/3aEAGX1RtoSN8aBDXNWeXqlz4/CXbOMcNF2fbGVX6I
0CVyKBWHr4VX/ruyyUaNqJan3IdsdKjsNkFkanr9gDlz2xABAkEAuCDy+kvLN781
crcq8tQJRy2xkKms4aVsoukFySAEjOIXTGWhVvnuPMqkgxTX9RhvqoWds+XPL2eS
puiVlbqhAQJBAWL+BnPnvufBF0gPz2Qj1SGc7OCnrf2JUf2rCGmRT7qGjuQM4X7f
pBfLmapdPoh5quC1Tj+y3ScmFhUpQp1t9jECQCsVkqVcrN2LgU8pawRM9yrPl1f5
S/m0wpnQGsl4E3h/wuHeegUnEEbrR9lgPDQelqp4/3DD2loGSzuA+teBRwECQDTK
dgcyhW9NhbrPrxXDRmSzQ369MOCtVSYWEzAvzd19OS6sw7PsaiinvHhbWXtOLJ0y
GSrb3It/3HTVJ/Tlb2ECQFjeIrpXrLkvxncoMBF6gj172RuagsKfogfhQpPz70mx
f0kQf8kgqebINMlnEEQkKDxLGcn5vh1ZncWrp3SfKDA=
-----END RSA PRIVATE KEY-----
```
The above key has "TESTTEST" embedded in its modulus

## References
* https://news.ycombinator.com/item?id=9516824
