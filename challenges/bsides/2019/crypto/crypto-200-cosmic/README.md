crypto-200-Cosmic
=================

* _title_: Cosmic
* _points_: 200
* _tags_:  crypto

## Flags
* `cybears{D@mn_th0s3_c0sm1c_r@yz}`

## Challenge Text
```markdown
We've intercepted a DecepticomTSS message stream [transmission interrupted] coordinates of some so[transmission interrupted] due to the ionic storm-front, communications a[transmission interrupted]e of vital importance.
```

## Attachments
* `messages.json`: json string with messages and signatures
* `flag.txt.enc`: encrypted flag

## Hints
* 

## References
* PKCSv1.5 signatures with RSA https://www.ietf.org/rfc/rfc3447.txt
* https://www.cryptologie.net/article/371/fault-attacks-on-rsas-signatures/ 

## Notes - Build
* `make` : to build handouts 
* `make test` : to run solver and validate
* `make clean` : to remove artifacts

<details>
<summary>SPOILERS</summary>
<p>

## Notes - Walkthrough
* This challenge is based on the fact that if there is a single bit error during decryption/signing, and this is released, the RSA modulus can be factored
* Provided with the challenge is a json file which includes the public modulus and exponent, and metadata relating to the padding and hashing algorithm needed to validate sigs. Then there are pairs of (message, signatures). 
* The messages are random (but someone can have fun trying to figure them out if they want)
* All of the signatures are valid except for one
* Players should parse the json, identify the incorrect signature, then use that to factor the modulus
* They can then create an RSA private key to decrypt the flag. 


</p>
</details>

