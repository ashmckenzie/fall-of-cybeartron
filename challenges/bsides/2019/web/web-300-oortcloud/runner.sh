#!/bin/sh

# runner.sh
# to run in teletraan-1 docker image

cd /home/oort

#start nginx with custom config
/usr/sbin/nginx -c /home/oort/teletraan-1.conf 

#start tcpdump (background)
tcpdump -G 300 -w '/usr/share/nginx/html/pcap/teletraan-1-%M.pcap' -s0 'port 443' &

#start flask+gunicorn
source venv/bin/activate
export FLASK_APP=flaskr
flask init-db
exec gunicorn -b 127.0.0.1:5000 --access-logfile=/home/oort/logs/cb_access.log --error-logfile=/home/oort/logs/cb_error.log cyberbook:app
