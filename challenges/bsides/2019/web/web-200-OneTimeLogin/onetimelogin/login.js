function doLogin() {
	document.forms["login_form"].submit();
}

function generateQRCode() {

	var key = $('#password').val();
	//console.log('Key: ' + key);
	key = base32.encode(key);
	key = key.replace(/=/g, ""); //Strip out the = padding if required
	//console.log('Key: ' + key);
	var keyHex = base32tohex(key);
	keyHex = keyHex.replace(/-/g, ""); //bug in base32tohex which sometimes returns -ve values
	//console.log('Key: ' + keyHex);

	//var epoch = Math.round(new Date().getTime() / 1000.0);
	//var time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, '0');
	//otp = genOTP(keyHex, epoch);

	$('#qrImg').attr('src', 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=200x200&chld=M|0&cht=qr&chl=otpauth://totp/user@host.com%3Fsecret%3D' + key);

}

function genOTP(keyHex, epoch) {
	var time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, '0');

	var shaObj = new jsSHA("SHA-1", "HEX");
	shaObj.setHMACKey(keyHex, "HEX");
	shaObj.update(time);
	var hmac = shaObj.getHMAC("HEX");

	if (hmac == 'KEY MUST BE IN BYTE INCREMENTS') {
                console.log(hmac);
        } else {
            var offset = hex2dec(hmac.substring(hmac.length - 1));
            var part1 = hmac.substr(0, offset * 2);
            var part2 = hmac.substr(offset * 2, 8);
            var part3 = hmac.substr(offset * 2 + 8, hmac.length - offset);
        }

	var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec('7fffffff')) + '';
	otp = (otp).substr(otp.length - 6, 6);
	//console.log("OTP: " + otp);

	return otp;
}

function dec2hex(s) { return (s < 15.5 ? '0' : '') + Math.round(s).toString(16); }
function hex2dec(s) { return parseInt(s, 16); }

function base32tohex(base32) {
	var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
	var bits = "";
	var hex = "";

	for (var i = 0; i < base32.length; i++) {
	    var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
	    bits += leftpad(val.toString(2), 5, '0');
	}

	for (var i = 0; i+4 <= bits.length; i+=4) {
	    var chunk = bits.substr(i, 4);
	    hex = hex + parseInt(chunk, 2).toString(16) ;
	}
	return hex;

}

function leftpad(str, len, pad) {
	if (len + 1 >= str.length) {
	    str = Array(len + 1 - str.length).join(pad) + str;
	}
	return str;
}

//RFC3548 base32
var base32 = (function(){
    this.encode = encode;
    this.decode = decode;
    var charJson = {
        '0' : 'A',
        '1' : 'B',
        '2' : 'C',
        '3' : 'D',
        '4' : 'E',
        '5' : 'F',
        '6' : 'G',
        '7' : 'H',
        '8' : 'I',
        '9' : 'J',
        '10' : 'K',
        '11' : 'L',
        '12' : 'M',
        '13' : 'N',
        '14' : 'O',
        '15' : 'P',
        '16' : 'Q',
        '17' : 'R',
        '18' : 'S',
        '19' : 'T',
        '20' : 'U',
        '21' : 'V',
        '22' : 'W',
        '23' : 'X',
        '24' : 'Y',
        '25' : 'Z',
        '26' : '2',
        '27' : '3',
        '28' : '4',
        '29' : '5',
        '30' : '6',
        '31' : '7'
    }
    var keyJson = reverseJson(charJson);
    function reverseJson(jsonData){
        var newJson = {};
        for(var key in jsonData){
            newJson[jsonData[key]] = key;
        }
        return newJson;
    }

    function encode(input){
        if(typeof input == 'undefined' || input == null){
            return null;
        }
        var arr = input.split('');
        var aAscii2 = [];
        for(var key in arr){
            var c = arr[key];
            var ascii = c.charCodeAt();
            var ascii2 = ascii.toString(2);
            var gap = 8 - ascii2.length;
            var zeros = '';
            for(var i = 0; i < gap; i++){
                zeros = '0' + zeros;
            }
            ascii2 = zeros + ascii2;
            aAscii2.push(ascii2);
        }
        var source = aAscii2.join('');
        var eArr = [];
        for(var i = 0; i < source.length; i += 5){
            var s5 = source.substring(i, i + 5);
            if(s5.length < 5){
                var gap = 5 - s5.length;
                var zeros = '';
                for(var gi = 0; gi < gap; gi++){
                    zeros += '0';
                }
                s5 += zeros;
            }
            var eInt = parseInt(s5, 2)
            eArr.push(charJson[eInt]);
        }
        if(eArr.length % 8 != 0){
            var gap = 8 - (eArr.length % 8);
            for(var i = 0; i < gap; i++){
                eArr.push('=');
            }
        }
        var eStr = eArr.join('');
        return eStr;
    }

    function decode(input){
        if(typeof input == 'undefined' || input == null){
            return null;
        }
        var trimStr = input.replace(/=/,'');
        var arr = trimStr.split('');
        var noArr = [];
        for(var i = 0; i < arr.length; i++){
            var key = keyJson[arr[i]];
            var key2 = parseInt(key).toString(2);
            if(key2.length < 5){
                var gap = 5 - key2.length;
                var zeros = '';
                for(var gi = 0; gi < gap; gi++){
                    zeros += '0';
                }
                key2 = zeros + key2;
            }
            noArr.push(key2);
        }
        var source = noArr.join('');
        if(source.length % 8 != 0){
            var gap = 8 - (source.length % 8);
            var zeros = '';
            for(var i = 0; i < gap; i++){
                zeros += '0';
            }
            source += zeros;
        }
        var dArr = [];
        for(var i = 0; i < source.length; i += 8){
            var temp = source.substring(i, i+8);
            var dStr =String.fromCharCode(parseInt(temp, 2));
            dArr.push(dStr);
        }
        var result = dArr.join('');
        return result;
    }
  return this;
})();


