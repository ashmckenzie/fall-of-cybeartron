import json
import os
import os.path
import socket
import subprocess
import tempfile
import uuid

import asyncio
import aiohttp_jinja2
import aiohttp_session
import aiohttp_session.redis_storage
import aioredis
import jinja2

from aiohttp import web

###############################################################################
# Flag!
###############################################################################
# This arbitrates access to the flag by setting `wootwootgotroot` if they auth
# with this as the secret - there's no insight into this, it's not the vector
SECRET = "mah9akahyi8aeg9Us3aecuVaiNoh2jie2noo8eey7ooxuo7shohmoh9shothu9ew"
GOT_ROOT_SESSION_KEY = "wootwootgotroot"

async def flag(request):
    session = await aiohttp_session.get_session(request)
    if session.get(GOT_ROOT_SESSION_KEY, False) is True:
        return web.Response(
            status=200, text="cybears{mylongestyeahboyever10hours}\n"
        )
    else:
        return web.Response(status=401, text="Seems unlikely, buddy\n")

###############################################################################
# Fixation vector
###############################################################################
async def check_login(request):
    session = await aiohttp_session.get_session(request)
    # We provide a JSON response with a bit of manufactured insight into the
    # session so the player can notice that it's possible to get a entirely
    # empty session state mapping
    if request.headers.get("Accept") == "application/json":
        if session.new:
            # Return `[null]` when we have a new session because we won't
            # set anything in it and therefore it won't be sent to the client
            return web.json_response([None])
        else:
            # Otherwise map the entire session dictionary into a JSON array
            # which acts as a discovery for the fixation bug and also a way to
            # verify if you've been escalated yet
            return web.json_response([dict(session.items())])
    # If they didn't request JSON then spit out some human music *beep boop*
    if session.get("logged_in", False) is True:
        text = "You are logged in, {}.\n".format(
            session.get("username", "unknown")
        )
        if session.get(GOT_ROOT_SESSION_KEY, False) is True:
            text += (
                "Congratulations, you are an admin and are therefore better "
                "than everyone else.\n"
            )
    else:
        text = "You aren't logged in ):\n"
    return web.Response(status=200, text=text)

async def login(request):
    # Don't touch a session for a bad POST
    if request.content_length and request.content_type != "application/json":
        return web.Response(status=415, text="JSON only please\n")
    # We use `get_session()` rather than `new_session()`, ignoring the advice
    # in the 'aiohttp_session' docs because we want a session fixation bug!
    session = await aiohttp_session.get_session(request)
    # This is how we allow a practical and interesting fixation attack vector.
    # The injection into the victim's client causes them to have a session
    # which is already logged in and we refuse to escalate it. However if the
    # attacker invalidates their session, the `aiohttp_session` bug in its
    # `redis_storage` backend results in an empty session which can then be
    # escalated.
    if session.get("logged_in", False) is True:
        username = session["username"]
        return web.Response(
            status=200, text=f"You're already logged in, {username}.\n"
        )
    data = await request.json() if request.content_length else dict()
    username = session["username"] = data.get("username", "anonymous")
    if data.get("secret") == SECRET:
        session[GOT_ROOT_SESSION_KEY] = True
    session["logged_in"] = True
    return web.Response(status=200, text=f"Logged in as {username}\n")

async def logout(request):
    session = await aiohttp_session.get_session(request)
    session.invalidate()
    return web.Response(status=204, text="Thanks for visiting my website!\n")

###############################################################################
# Response splitting vector
###############################################################################
async def home(request):
    # Get the banner
    key = "banner:{}".format(request["isolation"])
    banner = await request.app.redis_pool.get(key)
    banner = banner.decode() if banner is not None else "There's no news yet!"
    # Now render the homepage
    resp = aiohttp_jinja2.render_template(
        'home.jinja2', request, {"banner": banner}
    )
    # Set the "robot banner" which is used to allow response splitting
    resp.headers["X-Banner-For-Robots"] = banner
    return resp

# Banners will live for an hour to avoid keeping heaps of junk in redis
BANNER_TTL = 60 * 60

async def new_banner(request):
    session = await aiohttp_session.get_session(request)
    if session.get("logged_in", False) is not True:
        return web.Response(status=401, text="User must be logged in\n")
    if request.content_type != "application/json":
        return web.Response(status=415, text="JSON only please")
    json_data = await request.json()
    if "banner" not in json_data:
        return web.Response(status=422, text="No banner provided in request\n")
    key = "banner:{}".format(request["isolation"])
    last_banner = await request.app.redis_pool.getset(
        key, json_data["banner"].encode()
    )
    await request.app.redis_pool.expire(key, BANNER_TTL)
    return web.Response(
        status=200,
        text=(
            f"Your banner will be used for the next hour! "
            f"Thanks for contributing to my website!\n"
            f"The last banner was {last_banner!r}\n"
        )
    )

###############################################################################
# Player isolation middleware
###############################################################################
ISOLATION_COOKIE_NAME = (
    "CTF_ISOLATION_YOU_SHOULD_ONLY_SHARE_THIS_WITH_YOUR_TEAM"
)
# Allow isolation tokens to live for 2 hours - assume that players will nuke
# their cookies or something more frequently than this and will chalk it up to
# weirdness or ask us what's up if it affects them
ISOLATION_COOKIE_TTL = 60 * 60 * 2

@web.middleware
async def isolate_request(request, handler):
    # Check if the request came with an isolation token or if we need a new one
    req_isolation_cookie = request.cookies.get(ISOLATION_COOKIE_NAME)
    if req_isolation_cookie is not None:
        key = "isolation:{}".format(req_isolation_cookie)
        if await request.app.redis_pool.get(key):
            request["isolation"] = req_isolation_cookie
        else:
            # Kill the isolation cookie in the request and send a new one back
            req_isolation_cookie = None
    # If we ended up with no valid isolation cookie then we need a new one
    if req_isolation_cookie is None:
        # Generate a new isolation token
        request["isolation"] = uuid.uuid4().hex
        # Stick a truthy key in redis and make it expire at some point
        key = "isolation:{}".format(request["isolation"])
        await request.app.redis_pool.set(key, "1")
        await request.app.redis_pool.expire(key, ISOLATION_COOKIE_TTL)
    resp = await handler(request)
    # Only pass back the isolation cookie if we generated a new one
    if req_isolation_cookie is None:
        resp.cookies[ISOLATION_COOKIE_NAME] = request["isolation"]
    return resp

###############################################################################
# Admin polling background task
###############################################################################
FIXIE_COOKIE_NAME = "ILOVEMYFIXIEBIKE"

async def admin_request(app, isolation_token):
    # This is kind of shitty because we have to use something which is
    # vulnerable to cookie injection via response splitting. Neither `aiohttp`
    # or `requests` handle the injected headers in a way that lets us inject
    # the FIXIE cookie but we know that `curl` does, hence this weirdo
    # subprocess garbage because I can't be bothered finding something else...
    # Rather than actually being async with the processes here, we just assume
    # they'll be pretty quick and go to sleep for a second between them.
    #
    # We need to make a cookie jar for curl to use first
    with tempfile.NamedTemporaryFile(mode="w+", delete=False) as cookie_jar:
        cookie_jar.write(
            "\t".join((
                "localhost", "FALSE", "/", "FALSE", "0",
                "CTF_ISOLATION_YOU_SHOULD_ONLY_SHARE_THIS_WITH_YOUR_TEAM",
                isolation_token
            ))
        )
        cookie_jar.write("\n")
    # We have a base command we define for the two URI requests we need to make
    curl_cmd = (
        "curl", "-o", "/dev/null",
        "-b", cookie_jar.name, "-c", cookie_jar.name,
    )
    # This request to `/` injects the FIXIE cookie
    injector_proc = subprocess.Popen(
        curl_cmd + ("http://localhost:31337/",), stderr=open(os.devnull)
    )
    while injector_proc.poll() is None:
        await asyncio.sleep(0.1)
    # We should have the fixie session now so we can check if we need to
    # escalate it by breaking into the redis session storage
    with open(cookie_jar.name) as cookie_jar:
        try:
            (fixie_session_token, ) = (
                line.strip().split("\t")[-1]
                for line in cookie_jar.readlines()
                if FIXIE_COOKIE_NAME in line
            )
        except ValueError:
            print(
                "ADMIN: Isolated session {!r} didn't inject us"
                .format(isolation_token)
            )
            return
    # We magically know this from looking at the redis storage code
    session_key = "_".join((FIXIE_COOKIE_NAME, fixie_session_token))
    session = await app.redis_pool.get(session_key)
    if session is None:
        print(
            "ADMIN: Isolated session {!r} injected a bad fixie session {!r}"
            .format(isolation_token, fixie_session_token)
        )
        return
    session = json.loads(session)
    # If there is no session or if we don't have the "got root" key or that key
    # is not True, we need to escalate the fixie session
    if session.get("session", {}).get(GOT_ROOT_SESSION_KEY) is not True:
        # This request to auth escalates the session with our secret
        escalator_proc = subprocess.Popen(
            curl_cmd + (
                "-X", "POST", "-H", "Content-Type: application/json",
                "-d", '{{"secret": "{}"}}'.format(SECRET),
                "http://localhost:31337/auth",
            ), stderr=open(os.devnull)
        )
        while escalator_proc.poll() is None:
            await asyncio.sleep(0.1)
        # Sanity check that the session was escalated - we expect the `session`
        # key to be present now
        session = await app.redis_pool.get(session_key)
        session = json.loads(session)
        if session["session"].get(GOT_ROOT_SESSION_KEY) is True:
            print(
                "ADMIN: Escalated fixie {!r} in isolated session {!r}"
                .format(fixie_session_token, isolation_token)
            )
        else:
            print(
                "ADMIN: Failed to escalate fixie {!r} in isolated session {!r}"
                .format(fixie_session_token, isolation_token)
            )
    else:
        print("ADMIN: No need to escalate fixie {!r} in isolated session {!r}"
            .format(fixie_session_token, isolation_token)
        )
    # Clean up the cookie jar
    os.unlink(cookie_jar.name)

async def admin_poller(app):
    # This coroutine dies silently which is kind of annoying
    while True:
        print("ADMIN: Polling for isolated sessions I can escalate")
        cur = b"0"
        while cur:
            cur, keys = await app.redis_pool.scan(cur, "isolation:*")
            for key in keys:
                isolation_token = key.rpartition(b":")[-1].decode()
                banner_key = "banner:{}".format(isolation_token)
                if await app.redis_pool.get(banner_key):
                    print(
                        "ADMIN: Trying to escalate isolated session {!r}"
                        .format(isolation_token)
                    )
                    app.loop.create_task(admin_request(app, isolation_token))
        print(
            "ADMIN: Going to sleep - any scheduled escalations will continue"
        )
        await asyncio.sleep(5)

async def start_admin_poller(app):
    app.admin_poller = app.loop.create_task(admin_poller(app))

async def cancel_admin_poller(app):
    app.admin_poller.cancel()

###############################################################################
# App setup
###############################################################################
async def make_redis_pool():
    redis_address = (os.environ.get("BACKEND_HOSTNAME", "redis"), '6379')
    for i in range(5):
        try:
            pool = await aioredis.create_redis_pool(redis_address, timeout=1)
        except socket.gaierror as exc:
            print("Attempt {} to create a redis pool failed".format(i))
            await asyncio.sleep(1 + 2**i)
        else:
            break
    else:
        raise ConnectionError("Unable to connect to redis backend")
    return pool

async def make_app():
    redis_pool = await make_redis_pool()
    storage = aiohttp_session.redis_storage.RedisStorage(
        redis_pool, cookie_name=FIXIE_COOKIE_NAME
    )

    async def dispose_redis_pool(app):
        redis_pool.close()
        await redis_pool.wait_closed()

    app = web.Application(middlewares=(isolate_request, ))
    app.redis_pool = redis_pool
    app.session_storage = storage
    aiohttp_session.setup(app, storage)
    app.on_cleanup.append(dispose_redis_pool)
    # Set up the admin escalation background task
    app.on_startup.append(start_admin_poller)
    app.on_cleanup.append(cancel_admin_poller)
    # Templating and statics
    asset_dir = os.path.realpath(os.path.join(
        os.path.dirname(__file__), 'assets'
    ))
    j2_loader = jinja2.FileSystemLoader(os.path.join(asset_dir, 'tmpl'))
    aiohttp_jinja2.setup(app, loader=j2_loader)
    app.router.add_static('/static', os.path.join(asset_dir, 'static'))
    # Routes
    app.router.add_get('/', home)
    app.router.add_get('/flag', flag)
    app.router.add_get('/auth', check_login)
    app.router.add_post('/auth', login)
    app.router.add_delete('/auth', logout)
    app.router.add_post('/banner', new_banner)
    return app

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    web.run_app(loop.run_until_complete(make_app()), port=31337)
