# Stevedore Level0

* title: Stevedore Level 0
* difficulty: easy
* tags: misc docker forensics

## WARNING

Because the image is going to be publically available from docker hub, the deploy script
can't run until the day of the competition.

## Infrastructure

Use the Dockerfile to build a container called cybears/level0
Login to docker.io (as cybears) and push the container there (on CTF start day)
Users pull the container image and have to find the flag

## Skills required

* docker installation
* pulling an image from docker hub
* discovering what a dockerfile contained when you don't have it

## How to test drive

1. `cd level0`
2. `docker build -t cybears/level0 .` (Close your eyes during this step)
3. Pretend you just pulled your local copy of cybears/level0 from docker hub
4. Run the container: `docker run cybears/level0`
5. Or jump into a shell: `docker run cybears/level0 /bin/sh`
6. ???

## Solution

In this first docker challenge, there is a docker images created with some old Zork
games. The flag is hidden inside the Dockerfile (on separate lines) with a fake flag.txt
embedded inside the container.

All the user needs to do is discover that the `docker history` command shows them the 
commands used in the Dockerfile to build the container. The flag isn't visible unless they
pass `--no-trunc`, then they should see it running down the side. 

## Flag

cybears{you_are_able_to_see_how_each_world_was_created}
