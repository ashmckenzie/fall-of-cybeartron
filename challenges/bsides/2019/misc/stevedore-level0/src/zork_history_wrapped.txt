                                     
HISTORY OF ZORK                      
===============                      
                                     
Origin                               
------                               
                                     
Zork is one of the earliest          
interactive fiction computer         
games, with roots drawn from         
the original genre game              
Colossal Cave Adventure. The         
first version of Zork was            
written between 1977 and 1979        
using the MDL programming            
language on a DEC PDP-10             
computer.[1][2] The authors—Tim      
Anderson, Marc Blank, Bruce          
Daniels, and Dave Lebling—were       
members of the MIT Dynamic           
Modelling Group.[3]                  
                                     
                                     
Games                                
-----                                
                                     
When Zork was published              
commercially, it was split up        
into three games: Zork: The          
Great Underground Empire – Part      
I (later known as Zork I), Zork      
II: The Wizard of Frobozz, and       
Zork III: The Dungeon Master.[4]     
                                     
                                     
Quality                              
-------                              
                                     
Zork distinguished itself in         
its genre as an especially rich      
game, in terms of both the           
quality of the storytelling and      
the sophistication of its text       
parser, which was not limited        
to simple verb-noun commands         
('hit troll'), but recognized        
some prepositions and                
conjunctions ('hit the troll         
with the Elvish sword').[5]          
                                     
                                     
Reference                            
---------                            
https://en.wikipedia.org/wiki/Zork   
                                     