#!/usr/bin/python3

with open('flag.txt', 'r') as f:
    flag = f.read().strip()

with open('zork_history_wrapped.txt', 'r') as f:
    zork = f.readlines()

x = 0
for i in range(len(zork)):
    zork[i] = 'RUN echo "' + zork[i].replace('\n', '') + (x*' ') + flag[i] + '"'
    if (i % 10 > 5):
        x = x - 1
    else:
        x = x + 1

zork.reverse()
for line in zork:
    print(line.strip())
