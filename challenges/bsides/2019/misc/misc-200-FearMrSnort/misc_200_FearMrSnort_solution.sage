# misc-200-anagram solution

# run as
# /Applications/SageMath-8.2.app/Contents/Resources/sage/sage misc_200_FearMrSnort_solution.sage

# sage available on Docker registry as
# https://hub.docker.com/r/sagemath/sagemath/
#  docker run -it sagemath/sagemath:latest

# How to Solve
# Googling "subset anagram" -> second page links to this academic paper:
# http://www.alexhealy.net/papers/anagram.pdf
# describes Noam Elkies lattice basis reduction to find subset anagrams
# Can either implement algorithm in paper, or go to the website listed in paper:
# http://www.alexhealy.net/anagram.html
# Note that if you enter the 50 names into the webiste directly, it will only give you an anagram subset of size 11 - not 12
# If you enter random subsets of size about 40, sometimes there is an anagram of size >= 12 that you can use

import string

names_normalised = ['OPTIMUSPRIME', 'BUMBLEBEE', 'MEGATRON', 'STARSCREAM', 'GRIMLOCK', 'SIDESWIPE', 'IRONHIDE', 'RATCHET', 'SOUNDWAVE', 'JAZZ', 'PROWL', 'BARRICADE', 'DEVASTATOR', 'ULTRAMAGNUS', 'BRAWL', 'DRIFT', 'THUNDERCRACKER', 'CLIFFJUMPER', 'ARCEE', 'MIRAGE', 'GALVATRON', 'HOUND', 'JETFIRE', 'SHOCKWAVE', 'SKIDS', 'LOCKDOWN', 'STRAFE', 'BLACKOUT', 'SKYWARP', 'SMOKESCREEN', 'STRONGARM', 'CHEETOR', 'RAVAGE', 'BONECRUSHER', 'DEADEND', 'MUDFLAP', 'SNARL', 'WHEELJACK', 'BLADES', 'BULKHEAD', 'HEATWAVE', 'INFERNO', 'OPTIMUSPRIMAL', 'SCOURGE', 'THRUST', 'RAMJET', 'SILVERBOLT', 'BREAKDOWN', 'CYCLONUS', 'JOLT']

def StringToVector(s):
        #remove spaces
        #make all upper case
        #freq count to vector

        FreqCount = [0]*26
        s = s.upper()
        for c in s:
                if c in string.ascii_uppercase:
                        index = ord(c) - ord('A')
                        FreqCount[index] += 1

        return FreqCount

def VectorToPerm(v, names, show_lengths = False, debug=False):
        if(len(v) != 50):
                return ""

        first = []
        second = []

        for i in range(len(v)):
                if v[i] == 1:
                        first.append(names[i].strip())
                if v[i] == -1:
                        second.append(names[i].strip())

        if debug == True:
                print first, second
        if show_lengths == True:
                print len(first), len(second)

        return "+".join(first) + " = " + "+".join(second)

#Check if vector only has {-1,0,1}
def isOnlyOneZeroMinusOne(v):
        result = True
        for e in v:
                if e not in [-1,0,1]:
                        result = False
        return result

#Return "weight" of -1 and +1's separately
def weightZeroOne(v):
        minusone = 0
        one = 0

        for e in v:
                if e == -1:
                        minusone += 1
                if e == 1:
                        one += 1
        return (minusone, one)

def generateAnagrams(names, BKZ_blocksize = 4):

    anagrams = []

    V=[]
    for t in names:
        V.append(StringToVector(t.strip()))

    M = matrix(V)
    K = M.kernel().basis_matrix()

    for k in K.BKZ(block_size=BKZ_blocksize):
        if isOnlyOneZeroMinusOne(k):
                (z,o) = weightZeroOne(k)
                if z >=12 or o >= 12:
                        anagrams.append((VectorToPerm(k, names)))

    return anagrams

if __name__ == "__main__":
    print(generateAnagrams(names_normalised))
