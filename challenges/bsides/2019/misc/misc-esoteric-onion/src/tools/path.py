import sys
from pathlang import path


def path_main():
    """
    So I told my boss that this should run a PATH file. He doesn't know what
    the hell I'm talking about. But whatever, the guys an idiot and wouldnt
    know a good language when he sees one.

    I grabbed some interpreter I found on the net. Seems to work pretty good.
    :return:
    """
    if len(sys.argv) <= 1:
        path_usage()
        exit()

    file = sys.argv[1]

    try:
        prog = path()
        prog.load_prog_file(file)
        prog.verbose = False
        prog.run()
    except KeyboardInterrupt:
        sys.exit()
    except Exception as err:
        print(err)
        exit(1)


def path_usage():
    """
    Its real easy, just plug in the file and away you go.
    :return:
    """
    print("Usage: " + sys.argv[0] + " <.path file>")
    sys.exit()


if __name__ == "__main__":
    path_main()
