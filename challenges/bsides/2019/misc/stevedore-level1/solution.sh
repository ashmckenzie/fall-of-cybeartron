#!/bin/sh

# install python3 - we'll use this for xor later
if which apk 2>/dev/null; then apk add python3; fi
if ! which python3 2>/dev/null; then echo "No python3 found. Install before using this script."; exit 1; fi

# Build level1a 
docker build -t level1a-tmp -f level1a/Dockerfile level1a/

# Find the level1a flag then copy it out
find /var/lib/docker/ -type f -iname "flag.txt" -exec /bin/sh -c 'if ! grep empty {} > /dev/null ; then echo -e "\n===\nfound level1a flag - copying to root\n===\n"; cp "{}" /level1a-flag; fi' \;

# Build level1b
docker build -t level1b-tmp -f level1b/Dockerfile level1b/

# Find the flag
find /var/lib/docker/ -type f -iname ". " -exec /bin/sh -c 'echo -e "\n===\nFound level1b flag - copying to root\n===\n"; cp "{}" /level1b-flag' \;

# Run the solve script against the two files
/usr/bin/python3 /flagxor.py

# Clean up
docker rmi level1a-tmp
docker rmi level1b-tmp
