import os
import argparse

def next_name(cur_name):
    # easy case
    if cur_name == 'Z' * len(cur_name):
        cur_name = 'A' * (len(cur_name) + 1)
    
    # now we know we dont need to increase string size
    for index in range(len(cur_name) - 1, -1, -1):
        if cur_name[index] != 'Z':
            return cur_name[0:index] + chr(ord(cur_name[index]) + 1) + cur_name[index:len(cur_name)-1]

cur_name = 'A'
lines_per_file = 10


parser = argparse.ArgumentParser(description='Setup the virla marketing challenge')

parser.add_argument('--target_dir', help='Directory to drop the files')
# parser.add_argument('--flag_file', help='Real flag file', type=argparse.FileType('r'))
# parser.add_argument('--redherring_flag_file', help='Redherring flag file', type=argparse.FileType('r'))

args = parser.parse_args()

with open('history-of-advertising.txt', 'r') as ad_text:
    more_text = True
    
    while more_text:
        with open(os.path.join(args.target_dir, cur_name + '.txt'), 'w') as out_file:
            for i in range(lines_per_file):
                line = ad_text.readline()
                if line:
                    out_file.write(line)
                else:
                    more_text = False
        cur_name = next_name(cur_name)
