// ViralMarketing.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <windows.h>
#include "fileapi.h"
#include <iostream>
#include "bcrypt.h"

#define NT_SUCCESS(Status)          (((NTSTATUS)(Status)) >= 0)
#define STATUS_UNSUCCESSFUL         ((NTSTATUS)0xC0000001L)

static std::wstring getexepath()
{
	WCHAR *result = _wgetcwd(NULL, 0);
	return std::wstring(result);
}

static HANDLE openFirstStream(std::wstring file) {
	HANDLE fileHandle = INVALID_HANDLE_VALUE;
	HANDLE hStreamFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_STREAM_DATA FindStreamData;
	WCHAR filename[MAX_PATH];

	hStreamFind = FindFirstStreamW(file.c_str(), FindStreamInfoStandard, &FindStreamData, 0);
	if (hStreamFind != INVALID_HANDLE_VALUE) {
		wcsncpy_s(filename, MAX_PATH, file.c_str(), MAX_PATH);
		wcsncat_s(filename, MAX_PATH, FindStreamData.cStreamName, MAX_PATH - wcslen(filename));
		fileHandle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
		FindClose(hStreamFind);
		if(INVALID_HANDLE_VALUE == fileHandle) {
			std::cout << "Last error: 0x" << std::hex << GetLastError() << std::endl;
		}
	}
	return fileHandle;
}

int main()
{
	DWORD					serial = 0;
	DWORD					maximumComponentLength = 0;
	DWORD					fileSystemFlags = 0;
	HANDLE					hFile = INVALID_HANDLE_VALUE;
	std::wstring			flag_dir = L"*";
	std::wstring			file_flag = L"flag.txt",
							cur_dir;
	char					password[] = { 'X', 'r', 'F', 'j', 'H', 's', 'u', 'f', 'K', '3', 's', 'f', 'd', 's' };
	DWORD					numBytesRead = 0;
	std::string				decryptedtext;
	UCHAR					buffer[0x200];
	byte					key[16],
							iv[16];
	BCRYPT_ALG_HANDLE       hAesAlg = NULL;
	BCRYPT_KEY_HANDLE       hKey = NULL;
	NTSTATUS                status = STATUS_UNSUCCESSFUL;   
	DWORD                   cbPlainText = 0,
							cbData = 0,
							cbKeyObject = 0,
							cbBlockLen = 0,
							cbBlob = 0;
	PBYTE                   pbPlainText = NULL,
							pbKeyObject = NULL,
							pbBlob = NULL;

	GetVolumeInformationA(nullptr, nullptr, 0, &serial, &maximumComponentLength, &fileSystemFlags, nullptr, 0);

	for (int i = 0; i < (sizeof(key) / sizeof(DWORD)); i++) {
		memcpy(key + (sizeof(DWORD) * i), (byte *)&serial, sizeof(DWORD));
		memcpy(iv + (sizeof(DWORD) * i), (byte *)&fileSystemFlags, sizeof(DWORD));
	}

	// Open an algorithm handle.
	if (!NT_SUCCESS(status = BCryptOpenAlgorithmProvider(
		&hAesAlg,
		BCRYPT_AES_ALGORITHM,
		NULL,
		0)))
	{
		printf("**** Error 0x%x returned by BCryptOpenAlgorithmProvider\n", status);
		goto Cleanup;
	}

	// Calculate the size of the buffer to hold the KeyObject.
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAesAlg,
		BCRYPT_OBJECT_LENGTH,
		(PBYTE)&cbKeyObject,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		printf("**** Error 0x%x returned by BCryptGetProperty\n", status);
		goto Cleanup;
	}

	// Allocate the key object on the heap.
	pbKeyObject = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbKeyObject);
	if (NULL == pbKeyObject)
	{
		printf("**** memory allocation failed\n");
		goto Cleanup;
	}

	// Calculate the block length for the IV.
	if (!NT_SUCCESS(status = BCryptGetProperty(
		hAesAlg,
		BCRYPT_BLOCK_LENGTH,
		(PBYTE)&cbBlockLen,
		sizeof(DWORD),
		&cbData,
		0)))
	{
		printf("**** Error 0x%x returned by BCryptGetProperty\n", status);
		goto Cleanup;
	}

	if (!NT_SUCCESS(status = BCryptSetProperty(
		hAesAlg,
		BCRYPT_CHAINING_MODE,
		(PBYTE)BCRYPT_CHAIN_MODE_CBC,
		sizeof(BCRYPT_CHAIN_MODE_CBC),
		0)))
	{
		printf("**** Error 0x%x returned by BCryptSetProperty\n", status);
		goto Cleanup;
	}




	// Generate the key from supplied input key bytes.
	if (!NT_SUCCESS(status = BCryptGenerateSymmetricKey(
		hAesAlg,
		&hKey,
		pbKeyObject,
		cbKeyObject,
		(PBYTE)key,
		sizeof(key),
		0)))
	{
		wprintf(L"**** Error 0x%x returned by BCryptGenerateSymmetricKey\n", status);
		goto Cleanup;
	}

	cur_dir = getexepath();
	hFile = openFirstStream(cur_dir);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		if (ReadFile(hFile, buffer, sizeof(password), &numBytesRead, NULL)) {
			if (numBytesRead != sizeof(password)) {
				std::cout << "Incorrect password";
				exit(-1);
			}
			//check the password
			for (int i = 0; i < sizeof(password); i++) {
				if (password[i] != buffer[i]) {
					std::cout << "Incorrect password";
					exit(-1);
				}
			}

			CloseHandle(hFile);

			hFile = CreateFile(file_flag.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
			if (hFile != INVALID_HANDLE_VALUE) {
				if (ReadFile(hFile, buffer, 0x200, &numBytesRead, NULL)) {
					if (NT_SUCCESS(status = BCryptDecrypt(
														hKey,
														buffer,
														numBytesRead,
														NULL,
														iv,
														16,
														NULL,
														0,
														&cbPlainText,
														BCRYPT_BLOCK_PADDING))) {
						pbPlainText = (PBYTE)HeapAlloc(GetProcessHeap(), 0, cbPlainText);
						if (NULL == pbPlainText)
						{
							printf("**** memory allocation failed\n");
							goto Cleanup;
						}
						if (NT_SUCCESS(status = BCryptDecrypt(
															hKey,
															buffer,
															numBytesRead,
															NULL,
															iv,
															16,
															pbPlainText,
															cbPlainText,
															&cbPlainText,
															BCRYPT_BLOCK_PADDING))) {
							std::cout << "Congratulations, Flag is: " << pbPlainText << std::endl;
						}
					}
					else {
						printf("**** memory allocation failed: 0x%x\n", status);
					}

				}
				CloseHandle(hFile);
			}
		}
	}


Cleanup:

	if (hAesAlg)
	{
		BCryptCloseAlgorithmProvider(hAesAlg, 0);
	}

	if (hKey)
	{
		BCryptDestroyKey(hKey);
	}

	if (pbPlainText)
	{
		HeapFree(GetProcessHeap(), 0, pbPlainText);
	}

	if (pbKeyObject)
	{
		HeapFree(GetProcessHeap(), 0, pbKeyObject);
	}
	return 0;
}