#!/usr/bin/env bash
set -o errexit

SCRIPT_DIR=$(realpath "${0%/*}")
CAPTURE_FILE="$SCRIPT_DIR/handout/capturedflags.pcap"
CAPTURE_PID=""
SERVER_PID=""

pip3 install --user -r "$SCRIPT_DIR/requirements.txt"

function cleanup {
    sudo ip link del butts0
    # Killing tcpdump seems to misbehave a lot
    echo "Killing ${CAPTURE_PID}"
    while sudo kill $CAPTURE_PID &>/dev/null; do :; done
    echo "Killing ${SERVER_PID}"
    while sudo kill ${SERVER_PID} &>/dev/null; do :; done
    sudo chown "$(id -u):$(id -g)" "$CAPTURE_FILE"
}
trap cleanup EXIT

echo "Creating links"
sudo ip link add butts0 type veth peer name butts1
sudo ip addr add 172.18.0.1/16 dev butts0
sudo ip addr add 172.23.1.136/16 dev butts1

echo "Running packet capture"
mkdir -p "${CAPTURE_FILE%/*}"
sudo tcpdump -U -i lo -w "$CAPTURE_FILE" net 172.0.0.0/8 &
CAPTURE_PID="$!"
echo "Packet capture pid: ${CAPTURE_PID}"

python3 "./capturedflags.py" &
SERVER_PID="$!"

echo "Waiting for server to come online..."
while ! netstat -nlt | grep 9000 ; do sleep 0.5; done
sleep 2
echo "Making request"
curl --silent --output /dev/null                         \
    "http://172.18.0.1:9000/stream"
echo "Done!"
