#!/usr/local/bin/python3
import os
import hashlib, binascii

correct_flag = b'6f2a61992152002d76a0cc078f7e095c2925f74222f1521dff92ded4c7ef8e5a'

print("Checking... ")
x = ''.join(os.path.realpath(__file__).split(os.sep)[-11:-1])
k = hashlib.pbkdf2_hmac('sha256', x.encode(), b'cybears', 10000000)

if binascii.hexlify(k) == correct_flag:
    print("Verified. You have the flag: cybears{{{}}}".format(x))
else:
    print("Not verified.")
