"""
This module is the main implementation of the maximised file extract problem.
It contains a class that extends the base problem class and implements the
challenge.
"""
import random
import json
import time
import math
import logging
import os
from collections import Counter
from maximum_extract_challenge import ascii_art
from base_problem.problem_framework import BaseProblem

LOGGER = logging.getLogger()

DOWNLOAD_LIMIT = 1440000
FILE_NAMES = [
    '/weeabo/waifu_pillow_talk.txt',
    '/etc/passwd',
    '/etc/shadow',
    'data/system/password.key',
    '~/.ssh',
    '/donald/covfefe.msg',
    '/Documents/swap.avi',
    '/WhatsApp/messages.db',
    '/Signal/user_data.db',
    '/data/fbMessenger/msg.db',
    '/usr/share/contacts',
    '/instagram/msgs',
    '/data/viber/data.db',
    '/data/WeChat/private.msg',
    '/telegram/msg_store.db',
    '/blackberry/inbox.db',
    '/hillary/benghazi.msg',
    '/Kakaotalk/out',
    '/skype/pm',
    '/data/kik/pm',
    '/data/com.android.chrome/app_chrome/Default',
    '/data/cybears/ctf_chat.db',
]

MAX_SIZE = int((DOWNLOAD_LIMIT * 5) / len(FILE_NAMES))
MIN_SIZE = int(MAX_SIZE / 3)
MIN_VAL = 50
MAX_VAL = 100

INVALID_INPUT_ERROR = 'INVALID_INPUT_ERROR\n' \
                      '(comma delimited file_name club members only)\n' \
                      'AGENT DISCONNECTED!'

GREEDY_SOLUTION_DETECTED_ERROR = 'GREEDY_SOLUTION_DETECTED_ERROR\n' \
                                 '(RAND Corporation P550)\n' \
                                 'AGENT DISCONNECTED!'

FILE_VALUE_NOT_MAXIMISED_ERROR = 'TOTAL_FILE_VALUE_NOT_MAXIMISED_ERROR\n' \
                                 '(csc282//C16)\n' \
                                 'AGENT DISCONNECTED!'

DUPLICATE_SELECTION_ERROR = 'DUPLICATE_SELECTION_ERROR\n' \
                            '(No duplicates allowed)\n' \
                            'AGENT DISCONNECTED!'


# It's a big problem ;)
# pylint: disable=too-many-instance-attributes
class MaximumExtractProblem(BaseProblem):
    """
    HERE IN LIES THE CHALLENGE
    """
    DIFFICULTY_SCALE = 1.2  # Bigger == Longer to answer the puzzle
    EXPECTED_LATENCY = 0.05  # Seconds of lag

    def __init__(self, flag, client_address):
        self.logger = logging.getLogger(f'{os.getpid()}:'
                                        f'SERVER_{client_address[0]}'
                                        f':{client_address[1]}')
        super().__init__()
        self.max_message_length = self.get_max_char_length()
        self.values = self.get_values()
        self.complete_set = set(FILE_NAMES)
        self.result_set = None
        self.result_set_greedy = None
        self.loading_message = "Agent is discovering files."
        self.connection_timeout_message = '\nLOST CONNECTION TO AGENT!'
        self.problem_setup_exception_message = '\nThe Agent accessed a ' \
                                               'restricted file and got ' \
                                               'terminated by AV! (try ' \
                                               'reconnecting...)'
        self.problem_statement_wait = 2
        self.flag = flag
        self.client_address = client_address

    def get_splash_screen_text(self):
        """
        Return the splash screen logo
        :return:
        """
        output = {
            'message': ascii_art.SPLASH_SCREEN,
            'split_message': False,
            'delimiter': '\n',
            'row_delay': 0.01,
        }

        return output

    def get_problem_statement_text(self):
        """
        State the problem
        :return:
        """
        output = {
            'message': ascii_art.PROBLEM_STATEMENT,
            'split_message': True,
            'delimiter': '\n',
            'row_delay': 0.08,
        }

        return output

    def setup_problem(self, attempts=0):
        """
        We've already randomised the file values and sizes.
        For this step; we'll actually do the solution, and time the algo
        solution in order to limit the time the connection has to make
        a selection.
        :return:
        """
        # Solve the problem in both a greedy way and the dynamic programming
        # way and benchmark the time. That'll be the time to beat.
        self.logger.info(f'Server is solving...')
        start_time = time.monotonic()
        self.result_set_greedy = self.get_results(self.values, algo='greedy')
        self.result_set = self.get_results(self.values)
        end_time = time.monotonic()
        self.logger.info(f'Solved!')

        # Set the response timeout.
        elapsed_time = (end_time - start_time) * \
                       MaximumExtractProblem.DIFFICULTY_SCALE + MaximumExtractProblem.EXPECTED_LATENCY
        self.logger.info(f'setting timeout to {elapsed_time} seconds.')
        self._set_timeout(elapsed_time)

        if self.result_set_greedy == self.result_set:
            if attempts > 4:
                raise ValueError
            else:
                self.logger.info(f'Greedy == DP Solution, retrying...')
                self.values = self.get_values()
                self.setup_problem(attempts + 1)

        message_string = f'\n\nSTART_FILE_LIST\n' \
                         f'{self.values_to_json(self.values)}' \
                         f'\nEND_FILE_LIST\n' \
                         f'\nAgent will timeout after ' \
                         f'{math.floor(self.problem_timeout * 1000)} ' \
                         f'ms!\n>>'
        output = {
            'message': message_string,
            'split_message': True,
            'delimiter': '\n',
            'row_delay': 0.05,
        }

        return output

    def check_client_solution(self, client_solution):
        """
        Check that the provided solution is what we are after.

        If it's correct, return the flag.
        :param client_solution:
        :return:
        """
        output = {
            'message': INVALID_INPUT_ERROR,
            'split_message': False,
            'delimiter': '\n',
            'row_delay': 0.05,
        }

        if len(client_solution) > self.max_message_length:
            self.logger.info(f'FAIL: Client solution was too long.')
            return output

        solution_string = client_solution.decode('utf-8')
        solution_string = ''.join(solution_string.split())
        item_count = Counter(solution_string.split(','))

        self.logger.debug(f'Greedy : {self.result_set_greedy}')
        self.logger.debug(f'Dynamic: {self.result_set}')
        self.logger.debug(f'Client : {set(item_count)}')

        # Test if they've nailed it.
        if set(item_count) == self.result_set:
            self.logger.info(f'SUCCESS: Solution found!')
            output['message'] = self.flag
            return output

        # Test if they've tried the greedy approach.
        if set(item_count) == self.result_set_greedy:
            self.logger.info(f'FAIL: greedy attempt tried.')
            output['message'] = GREEDY_SOLUTION_DETECTED_ERROR
            return output

        # Test if their answer is at least in the set of files
        if set(item_count).issubset(self.complete_set):

            # Tell them they've got duplicates
            if item_count.get(max(item_count)) > 1:
                self.logger.info(
                    f'FAIL: Client solution contained duplicates.')
                output['message'] = DUPLICATE_SELECTION_ERROR
                return output

            # Otherwise its a suboptimal choice
            self.logger.info(f'FAIL: sub-optimal solution tried.')
            output['message'] = FILE_VALUE_NOT_MAXIMISED_ERROR
            return output

        self.logger.info(f'FAIL: Solution was incorrect.')
        return output

    def _set_timeout(self, seconds):
        """
        Set the timeout to solve the problem
        :param seconds:
        :return:
        """
        self.problem_timeout = seconds

    @classmethod
    def get_max_char_length(cls):
        """
        Get the length of all the files as a comma separated byte string.
        Use this to dump any messages to us that are longer.
        :return:
        """
        all_joined = ', '.join(FILE_NAMES)
        all_joined = all_joined.encode('utf-8')

        # Return the length and a little leeway
        return len(all_joined) + 5

    def get_values(self):
        """
        Wrapper for generating a file size / values list
        :return:
        """
        return self.generate_values(FILE_NAMES,
                                    MIN_SIZE, MAX_SIZE,
                                    MIN_VAL, MAX_VAL)

    def get_results(self, file_list, algo=None):
        """
        Wrapper for solving the problem given.
        :param file_list:
        :return:
        """
        if algo == 'greedy':
            results = self.greedy_solver(file_list,
                                         DOWNLOAD_LIMIT)
        else:
            results = self.dynamic_programming_solver(file_list,
                                                      DOWNLOAD_LIMIT)

        # Make the results a set
        out_set = {file[0] for file in results}

        return out_set

    @classmethod
    def values_to_json(cls, values):
        """
        Take a values list generated by get_values and return a JSON doc string
        :param values:
        :return:
        """
        values_dict = {}

        for value in values:
            values_dict[value[0]] = {
                "file_size": value[1],
                "file_value": value[2],
            }

        return json.dumps(values_dict, indent=4, sort_keys=True)

    # pylint: disable=too-many-arguments
    @classmethod
    def generate_values(cls, labels, min_size, max_size, min_val, max_val):
        """
        Randomly generate some file sizes and values for some file names
        :param labels:
        :param min_size:
        :param max_size:
        :param min_val:
        :param max_val:
        :return:
        """
        # Generate a new seed
        random.seed()

        # No two files can have the same value; so produce a sampled list
        # without replacement
        values = random.sample(range(min_val, max_val), len(labels))
        file_details = []

        # Randomise the file list too
        random.shuffle(labels)
        for label in labels:
            file_size = random.randint(min_size, max_size)
            file_value = values.pop()  # Get a value off the list

            file_details.append(
                (label, file_size, file_value)
            )

        return file_details

    def dynamic_programming_solver(self, file_list, download_limit):
        """
        Wrap the recursive solver as we don't care about the total value
        returned; just the items returned.
        :param file_list:
        :param download_limit:
        :return:
        """
        _, selection = self.recursive_solver(
            download_limit,
            list(),
            file_list,
            len(file_list)
        )

        return selection

    def recursive_solver(self, download_limit, selected, files, i):
        """
        This is recursive solution to the 0-1 Knapsack problem

        To read about the problem:
        http://www.es.ele.tue.nl/education/5MC10/Solutions/knapsack.pdf
        :param download_limit:
        :param selected:
        :param files:
        :param i:
        :return:
        """
        # Get the nth file
        file = files[i - 1]
        file_size = file[1]
        file_value = file[-1]

        # If we have no more items or hit the download limit; return
        if i == 0 or download_limit == 0:
            return 0, selected

        # If that item will make it over capacity, move onto the next item
        if file_size > download_limit:
            return self.recursive_solver(download_limit,
                                         selected,
                                         files,
                                         (i - 1))

        # Otherwise do the following:
        #  1. get the value and selection including the nth item
        #  2. get the value and selection excluding the nth item
        # Return the situation of maximum value.
        new_download_limit = download_limit - file_size
        value_a, selected_a = self.recursive_solver(new_download_limit,
                                                    list(),
                                                    files,
                                                    (i - 1))
        value_a += file_value  # The new value including the file

        # Option 2: exclude the nth item.
        value_b, selected_b = self.recursive_solver(download_limit,
                                                    list(),
                                                    files,
                                                    (i - 1))

        # Return the added item if the value is more
        if value_a > value_b:
            selected_a.append(file)
            return value_a, selected_a

        return value_b, selected_b

    @classmethod
    def greedy_solver(cls, file_list, download_limit):
        """
        Simple check to see if the greedy solution is the best.

        It's a bit of a dick move, but I'll validate the DP solution above
        against this and disconnect if the solution can be done via a greedy
        method (it'd because I'm a prick).

        It shouldn't happen often if ever, but on the off chance it does it'll
        make life a little shittier for the person trying to solve this.

        :param file_list:
        :param download_limit:
        :return:
        """
        # Sort the list from most valuable to least valuable
        file_list = sorted(file_list, key=lambda x: x[-1], reverse=True)
        file_size = 0
        selection = []

        # Get the files from most valuable to least valuable
        for file in file_list:

            if (file_size + file[1]) > download_limit:
                pass
            else:
                selection.append(file)
                file_size += file[1]

        return selection
