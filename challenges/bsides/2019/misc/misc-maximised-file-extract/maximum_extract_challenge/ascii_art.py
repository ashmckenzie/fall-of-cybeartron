"""
This module contains ASCII art for the game
"""

SPLASH_SCREEN = r"""
                 _______________________________________
                |;;;;;|                           |;;;;;||
                |;;;[]|---------------------------|[];;;||
                |;;;;;|                           |;;;;;||
                |;;;;;|   MAXIMISED               |;;;;;||
                |;;;;;|                           |;;;;;||
                |;;;;;|   FILE                    |;;;;;||
                |;;;;;|                           |;;;;;||
                |;;;;;|   EXTRACT          1.44mb |;;;;;||
                |;;;;;|___________________________|;;;;;||
                |;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;||
                |;;;;;;;; _____________________ ;;;;;;;;||
                |;;;;;;;;|  ____               |;;;;;;;;||
                |;;;;;;;;| |;;;;|              |;;;;;;;;||
                |;;;;;;;;| |;;;;|              |;;;;;;;;||
                |;;;;;;;;| |;;;;|              |;;;;;;;;||
                |;;;;;;;;| |;;;;|              |;;;;;;;;||
                |;;0-1;;;| |____|              |;;;SAC;;||
                \________|_____________________|________||
                 ~~~~~~~~~^^^^^^^^^^^^^^^^^^^^^~~~~~~~~~~
"""

PROBLEM_STATEMENT = r"""
We need to extract a bunch of files but we have a limit on the total data we
can transfer. Once we've selected the files, the payload will be lost and we
won't be able to get anything else.

You need to get as much value as you can given the file sizes and file value
within the download limit we've detected!

Just send back a comma separated list of files we should get and the agent
will (hopefully) return something good. 

e.g.

>> /etc/shadow, ~/.ssh, /instagram/msgs, /etc/passwd

Don't go over 1440000 bytes in your selection as it won't fit down the pipe.

Oh yeah; be quick! 

"""
