/**
 * Carton: Game main loop
 **/
#include "game.h"
#include <SDL.h>
#include <cstdio>

 // debug
// #define SHOW_FPS
#define LIMIT_FPS


// Maximum FPS
const int kFPS = 60;

// Minimum number of milliseconds per frame
const int kMsPerFrame = 1000 / kFPS;


CGame::~CGame()
{
    if (this->DefaultFont)
    {
        delete this->DefaultFont;
    }
}

bool CGame::Initialize(SDL_Renderer * renderer, CGameState * initial_state)
{
    this->Renderer = renderer;
    this->State = initial_state;

    // Load font
    this->DefaultFont = LoadFont(this->Renderer, "D:\\barcade.bmp", 24, 24);
    this->DefaultFont->SetBorder(64, 48);

    return (this->DefaultFont != nullptr);
}

void CGame::MainLoop()
{
    SDL_GameController * controller = nullptr;

    uint32_t frames = 0;
    uint32_t game_start = SDL_GetTicks();
    char fps_str[32] = { 0 };

    SDL_Event event;

    // Start main loop
    while (!ExitRequested)
    {
        uint32_t frame_start = SDL_GetTicks();

        // Handle input
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                ExitRequested = true;
                break;
#ifndef XBOX
                // Support keyboard input on Windows
            case SDL_KEYUP:
                this->State->HandleInput(event.key.keysym.sym);
                break;
#endif // XBOX
            case SDL_CONTROLLERDEVICEADDED:
                SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "Controller device added");

                // We only support one controller at a time
                // Close existing controller
                if (controller)
                {
                    SDL_GameControllerClose(controller);
                }

                // Open new controller
                controller = SDL_GameControllerOpen(event.cdevice.which);
                if (controller == NULL)
                {
                    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not open game controller: %s", SDL_GetError());
                }
                else
                {
                    SDL_LogDebug(SDL_LOG_CATEGORY_APPLICATION, "New controller opened: %s", SDL_GameControllerName(controller));
                }


                break;
            case SDL_CONTROLLERBUTTONUP:
            {
                // Translate controller button to keypress
                SDL_Keycode keycode = SDLK_UNKNOWN;
                switch (event.cbutton.button)
                {

                case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
                    keycode = SDLK_LEFT;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
                    keycode = SDLK_RIGHT;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_UP:
                    keycode = SDLK_UP;
                    break;
                case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
                    keycode = SDLK_DOWN;
                    break;
                case SDL_CONTROLLER_BUTTON_A:
                    keycode = SDLK_RETURN;
                    break;
                case SDL_CONTROLLER_BUTTON_BACK:
                    keycode = SDLK_ESCAPE;
                    break;
                default:
                    break;
                }

                this->State->HandleInput(keycode);
            }
            break;
            default:
                break;
            }
        }

        // Update state. If a new state is returned, make it the current state.
        CGameState * new_state = this->State->Update();
        if (new_state != nullptr)
        {
            delete this->State;
            this->State = new_state;
            this->IsDirty = true;
        }

        // Check if we need to render
#ifdef SHOW_FPS
        bool update_fps = (frames % kFPS) == 1;
#else
        bool update_fps = false;
#endif

        // Track number of frames rendered so far
        frames++;

        if (this->IsDirty || update_fps)
        {
            SDL_SetRenderDrawColor(this->Renderer, 0, 0, 0, 255);
            SDL_RenderClear(this->Renderer);

            this->State->Render();

#ifdef SHOW_FPS
            // Calculate FPS
            uint32_t game_time_elapsed = SDL_GetTicks() - game_start;
            float fps = frames / (game_time_elapsed / 1000.0F);

            // Unfortunately the CRT in NXDK doesn't support floats :(
            sprintf(fps_str, "FPS: %d", (int)fps);
            // Show FPS
            this->GetDefaultFont().Render(fps_str, 32, 420);

#endif // SHOW_FPS

            // Flip
            SDL_RenderPresent(this->Renderer);

            this->IsDirty = false;
        }

        // Limit the number of frames per second
#ifdef LIMIT_FPS
        uint32_t total_frame_time = SDL_GetTicks() - frame_start;
        if (total_frame_time < kMsPerFrame)
        {
            // Wait until the next frame
            SDL_Delay(kMsPerFrame - total_frame_time);
        }
#endif // LIMIT_FPS
    }
}