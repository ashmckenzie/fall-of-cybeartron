/**
 * Carton: Shellcode egg hunter
 **/
#include "egghunter.h"
#include <string.h>
#include <stdlib.h>

#ifdef XBOX
 // building for Xbox
#include <windows.h>
#else
 // building for Windows
#include <windows.h>
#endif

// Remove logs from release builds
#ifdef CTF_DEBUG
#include <SDL.h>
#define EGGHUNTER_LOG(fmt, ...)     SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, fmt, __VA_ARGS__)
#else
#define EGGHUNTER_LOG(fmt, ...)
#endif

// Shellcode eggs have "EGGx" at the start, where X is which chunk this is
#define EGG_TAG 0x474745  // "EGG\x00"

// Base of the char that identifies which chunk this is, eg. 'a' = 0, 'b' = 1, etc.
#define EGG_COUNT_BASE 'a'

// Size of each shellcode egg
#define EGG_SIZE 23

// Number of shellcode eggs
#define NUMBER_OF_EGGS 2


// Prototype for shellcode. Returns non-zero if password is correct.
typedef int(__cdecl *ValidatePasswordFn)(const char * password);

// Find all of the shellcode eggs, assemble them, and call the shellcode to validate the password
int ValidatePassword(const char * password)
{
    uint8_t * executable_buffer = NULL;
    const size_t total_eggs_size = EGG_SIZE * NUMBER_OF_EGGS;

    // Allocate buffer to gather the eggs
    executable_buffer = (uint8_t*)VirtualAlloc(0, total_eggs_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE);

    if (nullptr == executable_buffer)
    {
        EGGHUNTER_LOG("Failed to allocate executable buffer!");
        return 0;
    }

    EGGHUNTER_LOG("Executable buffer: 0x%x", executable_buffer);

    MEMORY_BASIC_INFORMATION mbi = { 0 };
    uint8_t * current_address = 0;
    int eggs_found = 0;

    const uint8_t * end_address = (const uint8_t*)0x7ffe0000;

    while (current_address < end_address && eggs_found < NUMBER_OF_EGGS)
    {
        EGGHUNTER_LOG("Calling VirtualQuery with address: %p", current_address);
        memset(&mbi, 0, sizeof(mbi));

        if (!VirtualQuery(current_address, &mbi, sizeof(mbi)))
        {
            EGGHUNTER_LOG("VirtualQuery failed. Last error: 0x%x", GetLastError());
            break;
        }
        else
        {
            // Success!
            EGGHUNTER_LOG("%p: size=0x%lx state=0x%lx protect=0x%lx type=0x%lx\n", mbi.BaseAddress, mbi.RegionSize, mbi.State, mbi.Protect, mbi.Type);

            // Skip regions that we can't access
            if (mbi.Protect == 0 || mbi.Protect == PAGE_NOACCESS || (mbi.Protect & PAGE_GUARD) == PAGE_GUARD)
            {
                // skip
            }
            else if (mbi.RegionSize > EGG_SIZE)
            {
                uint8_t * this_region = (uint8_t*)mbi.BaseAddress;

                // Search for the eggs in this memory region
                for (size_t i = 0; i < (mbi.RegionSize - EGG_SIZE) && eggs_found < NUMBER_OF_EGGS; i++)
                {
                    uint8_t * pos = (this_region + i);

                    uint32_t header = *(uint32_t*)(this_region + i);

                    // Check if the tag starts with 'EGGx'
                    if ((header & 0x00FFFFFF) == EGG_TAG)
                    {
                        // Get the egg number
                        uint8_t egg_number = this_region[i + 3] - EGG_COUNT_BASE;

                        if (egg_number >= 0 && egg_number < NUMBER_OF_EGGS)
                        {
                            EGGHUNTER_LOG("Found egg %d at %p\r\n", egg_number, this_region + i);
                            memcpy(executable_buffer + egg_number * EGG_SIZE, this_region + i + 4, EGG_SIZE);
                            eggs_found++;
                        }
                    }
                }
            }
        }
        current_address += mbi.RegionSize;
    }
    EGGHUNTER_LOG("exited the loop");

    if (eggs_found < NUMBER_OF_EGGS)
    {
        EGGHUNTER_LOG("could not find all the eggs!\n");
        return 0;
    }
    else
    {
        ValidatePasswordFn validate_password = (ValidatePasswordFn)(executable_buffer);
        int egg_result = 0;

        egg_result = validate_password(password);

        EGGHUNTER_LOG("\nDONE! Shellcode says: %d\n", egg_result);

        return egg_result;
    }
}