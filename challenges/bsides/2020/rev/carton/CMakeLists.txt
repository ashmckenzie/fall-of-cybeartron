cmake_minimum_required(VERSION 3.9)

# Add Hunter for third-party dependencies
include("cmake/HunterGate.cmake")
HunterGate(
    URL "https://github.com/cpp-pm/hunter/archive/v0.23.250.tar.gz"
    SHA1 "0e6ce3072a280110f33162e0265fb3796652673f"
)

project(CartonPrototype)

# Use Hunter to download and build SDL2
hunter_add_package(SDL2)
find_package(SDL2 CONFIG REQUIRED)

# Build the challenge
add_executable(
    CartonPrototype
    ext/WjCryptLib/lib/WjCryptLib_Aes.c
    ext/WjCryptLib/lib/WjCryptLib_AesCbc.c
    ext/WjCryptLib/lib/WjCryptLib_Md5.c
    platform.h
    windowsplatform.h
    egghunter.cpp

    game.h
    game.cpp
    common.cpp
    enterpassword.cpp
    checkingpassword.cpp
    badpassword.cpp
    goodpassword.cpp
    anticheat.cpp
    main.cpp
)

target_include_directories(
    CartonPrototype
    PRIVATE
    $(CMAKE_CURRENT_SOURCE_DIR)
    ext/WjCryptLib/lib
)

target_link_libraries(
    CartonPrototype
    SDL2::SDL2main
    SDL2::SDL2
)

target_compile_definitions(
    CartonPrototype
    PRIVATE
    _CRT_SECURE_NO_WARNINGS
    CTF_DEBUG
)
