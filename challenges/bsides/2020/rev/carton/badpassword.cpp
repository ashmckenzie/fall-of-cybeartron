/**
 * Carton: Bad password state
 **/
#include "game.h"
#include <SDL.h>

void CBadPasswordState::HandleInput(SDL_Keycode keycode)
{
    // any button will return the player to the enter password state
    retry_requested = true;
}

CGameState * CBadPasswordState::Update()
{
    if (retry_requested)
    {
        return new CEnterPasswordState(this->game);
    }
    else
    {
        return nullptr;
    }
}

void CBadPasswordState::Render()
{
    this->game.GetDefaultFont().Render("BAD PASSWORD!\nPress any button to try again.", 32, 32);
}