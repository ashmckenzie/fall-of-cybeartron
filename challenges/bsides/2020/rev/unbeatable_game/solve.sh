#!/bin/sh

if [ $# -ne 2 ]; then
    echo "$0: need dns and port, eg $0 unbeatable_game.local 3142"
    exit 1
fi
target_dns=$1
target_port=$2

python3 -c "print('hi\n' + 's\n' * 1024 + 'p\n' * 1024 + 'r\n' * 1024 + '?' + 's\np\nr\n' * 12)" | nc $target_dns $target_port > output
grep "You get a flag" output || exit 1
