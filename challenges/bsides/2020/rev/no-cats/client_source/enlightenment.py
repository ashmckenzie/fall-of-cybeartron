#!/usr/bin/python3

import pygame
import sys
from clienthandler import ClientController
from camera import *
from player import Player
from npc import NPC
from other_player import OtherPlayer
import tcp_networking
import threading
import random
import argparse


################################# ARGUMENTS #################################

parser = argparse.ArgumentParser()
parser.add_argument("--target_dns", type=str, help='The server address.', default='no-cats.ctf.cybears.io')
parser.add_argument("--target_port", type=int, help='The server port.', default=9001)
args = parser.parse_args()


################################# LOAD UP A BASIC WINDOW AND CLOCK #################################
ClientController()
pygame.init()
DISPLAY_W, DISPLAY_H = 480, 270
canvas = pygame.Surface((DISPLAY_W,DISPLAY_H))
window = pygame.display.set_mode(((DISPLAY_W,DISPLAY_H)))
pygame.display.set_caption('There are no cats allowed in the shrine.')
icon = pygame.image.load('icon.png')
pygame.display.set_icon(icon)
running = True
clock = pygame.time.Clock()
house = pygame.image.load('house_full.png').convert()
shrine_pole = pygame.image.load('shrine_pole_front.png').convert_alpha()

################################# LOAD PLAYER AND CAMERA###################################
hero = Player()
warden = NPC('warden.png', 7)
shrinemaster = NPC('bear.png', 2)
shrinemaster.rect.midbottom = (1500, 200)
camera = Camera(hero)
follow = Follow(camera, hero)
border = Border(camera, hero)
auto = Auto(camera, hero)
camera.setmethod(follow)

################################# BEGIN NETWORKING ###################################

tcp_networking.reactor.connectTCP(args.target_dns, args.target_port, tcp_networking.EnlightenFactory())
reactor_thread = threading.Thread(target=tcp_networking.reactor.run)
reactor_thread.start()

################################# GAME LOOP ##########################
while running:
    clock.tick(60)
    ################################# CHECK PLAYER INPUT #################################
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            sys.exit(0)
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                hero.LEFT_KEY, hero.FACING_LEFT = True, True
            elif event.key == pygame.K_RIGHT:
                hero.RIGHT_KEY, hero.FACING_LEFT = True, False
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                hero.LEFT_KEY = False
            elif event.key == pygame.K_RIGHT:
                hero.RIGHT_KEY = False

    ################################# UPDATE/ Animate SPRITE #################################
    if hero.rect.x > warden.rect.x:
        warden.near_player = True
        should_blit_warning = True
    else:
        warden.near_player = False
        should_blit_warning = False

    hero.update()
    cc = ClientController.getInstance()
    cc.player_x = hero.rect.x
    with cc.players_lock:
        for player in ClientController.getInstance().players:
            player.update()
    warden.update()
    shrinemaster.update()
    shrinemaster.current_image.set_alpha(random.randint(0,255))
    camera.scroll()

    ################################# UPDATE WINDOW AND DISPLAY #################################
    canvas.blit(house, (0 - camera.offset.x, 0 - camera.offset.y))
    canvas.blit(warden.current_image, (warden.rect.x - int(camera.offset.x), warden.rect.y - int(camera.offset.y)))
    canvas.blit(shrinemaster.current_image, (shrinemaster.rect.x - int(camera.offset.x), shrinemaster.rect.y - int(camera.offset.y)))
    with cc.players_lock:
        for player in ClientController.getInstance().players:
            if player.shadow_image is not None:
                canvas.blit(player.shadow_image, ((player.rect.x - camera.offset.x) - 2, (player.rect.y - camera.offset.y) - 2))
            canvas.blit(player.current_image, (player.rect.x - camera.offset.x, player.rect.y - camera.offset.y))
        if cc.flag is not None:
            pygame.display.set_caption(cc.flag)
    canvas.blit(hero.current_image, (hero.rect.x - camera.offset.x, hero.rect.y - camera.offset.y))
    if should_blit_warning:
        canvas.blit(pygame.image.load('warning.png'), (warden.rect.x - int(camera.offset.x) + 50, warden.rect.y - int(camera.offset.y) - 50))
    canvas.blit(shrine_pole, (0 - camera.offset.x, 0 - camera.offset.y))
    window.blit(canvas, (0, 0))
    pygame.display.update()

