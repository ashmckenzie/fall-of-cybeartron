# Blinkenlights walkthrough

## Hints

1. First hint
<details>
Use CyberChef to automate slow, manual reverse engineering tasks!
</details>


## Steps

<details>
<summary>Spoiler alert!</summary>

- The program creates a tray icon
- The context menu on the tray icon includes an option to "connect" to the VPN.
- The password is "12345".
- Once connected, the tray icon will start to blink in morse code.
- Decoding the morse code gives the flag.

Decoding the morse by hand is pretty annoying, so here is an example solution using CyberChef with the data extracted from the binary:

https://gchq.github.io/CyberChef/#recipe=From_Base64('A-Za-z0-9%2B/%3D',true)XOR(%7B'option':'Hex','string':'33'%7D,'Standard',false)To_Binary('Space')Remove_whitespace(true,true,true,true,true,false)Find_/_Replace(%7B'option':'Regex','string':'111000'%7D,'-',true,false,true,false)Find_/_Replace(%7B'option':'Regex','string':'1000'%7D,'.',true,false,true,false)Find_/_Replace(%7B'option':'Regex','string':'00000000'%7D,'%20',true,false,true,false)From_Morse_Code('Space','Line%20feed')&input=MHpFUkV6RXpFUXZSQ3pPNzBUTVF1N003MHpDOUV6RVF2UkN6TzdNN3V6TzcwTHZUTUx2Uk13dTlDelBSRVRNVE1Rc3p2Uk14RVRNUkM5RUxNOUVMc3p1OU14QzdNNzBSTXd1OUN6UFJFVE1RdXpPOU13dTlFekV6RUxNNzBSTXhDN003c3owTHN6dTdzejB6RVF2UkN6TzlFVE1UTUxNN3V6TzcwTHZUTUwwVE1UTUxNN3ZRdTlNd3N6dTdzenV6TzdzenU5QzcwekM5Q3pPNzB6Q3pPNzBMdlRNTDBMTTlFekN6UFF2VE1SQzlFTE03MHpFUXZSQ3pPN3N6MFF2VE1SRXpDek96UFFzenU5QzcwekN6TzlFekVMTTlFTDB6RUx2UkN6TzlFTHZUTVF1OUVMTTlFekVUTUx2Uk14TXd1OUVMMHpDNzBUTVJDek85RXpFTHV6UFJDOU13dTdzenZSTXhDelBSQzdNNw
</details>

## Flag

<details>
<summary>Click to reveal the flag</summary>
```
cybears{alright_lets_get_this_out_onto_a_system_tray...nice!}
```
</details>
