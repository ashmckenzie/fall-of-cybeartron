import XCTest
import class Foundation.Bundle
@testable import libr2c2

final class r2c2Tests: XCTestCase {
    
    func testDownload() throws {
        let downloadOptions = DownloadOptions.init(path: productsDirectory.appendingPathComponent("r2c2").path)
        let moduleData = try PropertyListEncoder.init().encode(downloadOptions)
        let moduleOptions = ModuleOptions.init(type: .Download, data: moduleData)
        
        let c = Comms.init()
        let ret = c.dispatch(moduleOptions)
        
        XCTAssertEqual(ModuleType.Download, ret.type)
        XCTAssertNotNil(ret.data)
        let downloadRet = try PropertyListDecoder.init().decode(DownloadResults.self, from: ret.data!)
        XCTAssertNotNil(downloadRet.size)
        // We should be larget than 128 bytes, so hit the download cap!
        XCTAssertNil(downloadRet.contents)
        XCTAssertTrue(0 < downloadRet.size!)
    }

    func testListDir() throws {
        let listDirOpts = ListDirectoryOptions.init(path: "/", maxSize: 0)
        let moduleData = try PropertyListEncoder.init().encode(listDirOpts)
        let moduleOpts = ModuleOptions.init(type: .ListDirectory, data: moduleData)
        
        let c = Comms.init()
        let ret = c.dispatch(moduleOpts)
        
        XCTAssertEqual(ModuleType.ListDirectory, ret.type)
        XCTAssertNotNil(ret.data)
        
        let listDirRet = try PropertyListDecoder.init().decode(ListDirectoryResult.self, from: ret.data!)
        print("\(listDirRet)")
    }

    /// Returns path to the built products directory.
    var productsDirectory: URL {
      #if os(macOS)
        for bundle in Bundle.allBundles where bundle.bundlePath.hasSuffix(".xctest") {
            return bundle.bundleURL.deletingLastPathComponent()
        }
        fatalError("couldn't find the products directory")
      #else
        return Bundle.main.bundleURL
      #endif
    }

    static var allTests = [
        ("testDownload", testDownload),
    ]
}
