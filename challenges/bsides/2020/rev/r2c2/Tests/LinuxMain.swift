import XCTest

import r2c2Tests

var tests = [XCTestCaseEntry]()
tests += r2c2Tests.allTests()
XCTMain(tests)
