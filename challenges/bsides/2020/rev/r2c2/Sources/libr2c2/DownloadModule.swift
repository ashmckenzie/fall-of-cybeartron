import Foundation

public struct DownloadOptions: Codable {
    /**
     The path to the thing to download
     */
    var path: String
    /**
     The maximum size of the thing to download
     */
    var maxSize: size_t = 128 // 128 bytes should be enough for anyone!
}

public struct DownloadResults: Codable {
    var path: String?
    var size: size_t?
    var contents: Data?
}

class DownloadModule: Module {
    
    func doDownload(_ options: DownloadOptions) -> DownloadResults {
        let data = FileManager.default.contents(atPath: options.path)
        var results: DownloadResults = .init(path: URL.init(fileURLWithPath: options.path).resolvingSymlinksInPath().path, size: data?.count, contents: nil)
        if data != nil {
            if data!.count <= options.maxSize {
                results.contents = data!
            }
        }
        return results
    }
    
    func run(_ options: ModuleOptions) -> ModuleReturnData {
        guard options.type == .Download else {
            return ModuleReturnData.init(type: .Download, status: .BadType, data: nil)
        }
        do {
            let downloadOptions = try PropertyListDecoder.init().decode(DownloadOptions.self, from: options.data)
            let downloadResults = self.doDownload(downloadOptions)
            let results = ModuleReturnData.init(type: .Download, status: .Success, data: try! self.getEncoder().encode(downloadResults))
            return results
        } catch {
            return .init(type: .Download, status: .Fail, data: nil)
        }
        
    }
}


let downloadModule = DownloadModule.init()
