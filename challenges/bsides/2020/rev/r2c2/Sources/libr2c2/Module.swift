import Foundation

let PropertyListOptions = PropertyListSerialization.PropertyListFormat.binary

public enum ModuleStatus: String, Codable {
    case Success = "😀"
    case Fail = "😣"
    case BadType = "😕"
    case BadFormat = "🤯"
}

public enum ModuleType: String, Codable {
    case comms = "📱"
    case Download = "⬇️"
    case ListDirectory = "🗺"
}

public struct ModuleOptions: Codable {
    var type: ModuleType
    var data: Data
}

public struct ModuleReturnData: Codable {
    var type: ModuleType
    var status: ModuleStatus
    var data: Data?
}

public protocol Module {
    func getEncoder() -> PropertyListEncoder
    /**
     Run the module, call `callback` when complete
     */
    func run(_ options: ModuleOptions) -> ModuleReturnData
}

extension Module {
    func getEncoder() -> PropertyListEncoder {
        let encoder = PropertyListEncoder.init()
        encoder.outputFormat = PropertyListOptions
        return encoder
    }
}
