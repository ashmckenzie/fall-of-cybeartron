# based on https://github.com/google/google-ctf/blob/master/2019/finals/sandbox-sbox/Dockerfile
# docker build -t db_nsjail -f Dockerfile.nsjail .
# docker run -it --rm -p 9000:9000 --privileged db_nsjail

FROM ubuntu

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update          \
 && apt-get install -y      \
    autoconf \
    bison \
    flex \
    gcc \
    g++ \
    git \
    libprotobuf-dev \
    libnl-route-3-dev \
    libtool \
    make \
    pkg-config \
    protobuf-compiler \
 && rm -rf -- /var/lib/apt/*

RUN cd / && git clone https://github.com/google/nsjail.git && \
    cd /nsjail && make && mv /nsjail/nsjail /root && rm -rf -- /nsjail

RUN set -e -x; \
        groupadd -g 1338 user; \
        useradd -g 1338 -u 1338 -m user

##### END NSJAIL SETUP

RUN apt-get update          \
 && apt-get install -y      \
    sqlite3 \
 && rm -rf -- /var/lib/apt/*

COPY tf.db /home/user/tf.db

# -Ml --port 9000 <- run like socat, listen for connection on port and run command
# --user 1338 --group 1338 <- run as this user
# -R explicitly sets files available as read-only. Add library paths, sqlite3 binary and challenge database file
# -- sqlite3 --interactive [db] <- command to run and present to user
# note that sqlite has the .shell command to run a command line process, nsjail will disallow any other file access or executable access
CMD /root/nsjail -Ml --port 9000 --user 1338 --group 1338 -R /lib/x86_64-linux-gnu/ -R /lib/x86_64-linux-gnu -R /lib64 -R /usr/bin/sqlite3 -R /dev/urandom -R /home/user/tf.db --keep_caps -- /usr/bin/sqlite3 -interactive /home/user/tf.db
