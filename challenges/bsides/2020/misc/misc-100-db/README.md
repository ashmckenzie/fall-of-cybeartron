misc-100-DeeBee
=================

* _author_: Cybears:cipher
* _title_: Dee Bee
* _points_: 100
* _tags_:  misc

## Flags
* `cybears{1990directhitoptimusprime}`

## Challenge Text
```markdown
Connect to the server below, in it will be an sqlite database that
contains a list of transformers (TM) toys, the number of each character
released, and the year the toy was first introduced. There is another
table of 'likes' for each of the toys.

The flag will be a concatenation of 3 values A, B and C, all in lower
case, no spaces, no special characters `cybears{ABC}` where:

A: what year had the most toys released?
B: which toy has the most likes?
C: which toy has the most toys released?

`nc $target_dns $target_port`
```

## Attachments
* 

## Hints
* 

## References
*

## Notes nsjail
* After testing ttyd, it became too complicated in our infrastructure to have priveged containers running docker in docker.
* We looked into nsjail, an unofficial google product used in googlectf.
* It can work similar to socat, but with greater fidelity on networking and file access
* Build: `docker build -t db_nsjail -f Dockerfile.nsjail .`
* Run: `docker run -it --rm -p 9000:9000 --privileged db_nsjail`
* Client connect: `nc localhost 9000`
* The Dockerfile.nsjail has a breakdown of the nsjail commandline
