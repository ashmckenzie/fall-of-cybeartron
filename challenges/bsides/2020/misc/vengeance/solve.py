#!/usr/bin/python3

from pwn import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', type=bool, default=False, help="Remote? Needs -n and -p")
parser.add_argument('-n', '--hostname', type=str,  default="localhost", help='Hostname')
parser.add_argument('-p', '--port', default=3141, type=int, help='Remote port')
args = parser.parse_args()

r = args.remote
hostname = args.hostname
port = args.port

flag = b'cybears{y1pp13_k@y@k_0th3r_Buck3t5}'

if __name__ == "__main__":

    if (r == True):
        p = remote(hostname, port)
        offset = 1
    else:
        p = process(["python3" ,"./Vengeance.py"])
        offset = 0

    preamble = p.readuntil("CMD:")
    p.sendline("u")

    bucket_info = p.readuntil("CMD:")
    log.info(bucket_info)

    b0 = int(bucket_info.split(b'\n')[1 + offset].split(b"\t")[2])
    b1 = int(bucket_info.split(b'\n')[2 + offset].split(b"\t")[2])
    b2 = int(bucket_info.split(b'\n')[3 + offset].split(b"\t")[2])

    #print(bucket_info)
    log.info("DEBUG: Buckets are {},{},{}".format(b0,b1,b2))


    target = 1
    cmd_str = []

    class bucket:
        def __init__(self,size,i):
            self.size = size
            self.i = i
            self.amt = 0

        def empty(self):
            cmd_str.append("e%d"%self.i)
            self.amt = 0

        def fill(self):
            cmd_str.append("f%d"%self.i)
            self.amt = self.size

        def move(self,target):
            cmd_str.append("m%d%d"%(self.i,target.i))
            target.amt = self.amt + target.amt
            if target.amt > target.size:
                self.amt = target.amt-target.size
                target.amt = target.size
            else:
                self.amt = 0

    s = [b0,b1,b2]
    b = [bucket(s[i],i) for i in range(len(s))]

    def gcd(a,b):
        while b > 0:
            a,b = b,a%b
        return a

    g = [gcd(b[0].size,b[1].size),gcd(b[0].size,b[2].size),gcd(b[1].size,b[2].size)]

    #Resolve buckets 1 and 2, store answer in 2

    while (b[2].amt)%g[0] != 1:
        b[1].fill()
        b[1].move(b[2])
        while b[2].amt == b[2].size:
            b[2].empty()
            b[1].move(b[2])


    #Resolve buckets 0 and 1
    while (b[1].amt != b[1].size - b[2].amt+1):
        b[0].fill()
        b[0].move(b[1])
        while b[1].amt == b[1].size:
            b[1].empty()
            b[0].move(b[1])


    b[2].move(b[1])

    b0 = b[0].size
    b1 = b[1].size
    b2 = b[2].size

    i = min([0,1,2],key=lambda x : b[x].size)
    if i != 2:
        if b[i].amt != 0:
            b[i].empty()
        b[2].move(b[i])
    cmd_str.append("p%i"%i)
    str_out = ";".join(cmd_str)
    while True:
        i = str_out.find(";",4000)
        if i == -1:
            p.sendline(str_out)
            break
        else:
            p.sendline(str_out[:i])
            str_out = str_out[i+1:]

    if r:
        answer = b""
        while (not flag in answer):
            answer = p.readuntil("CMD:")
            #log.info("ANS1: " + str(answer))
        log.success("SUCCESS: flag found!")
        exit(0)
    else:
        answer = p.readlines(1)[0]

    if flag in answer:
        log.success("SUCCESS: flag found!")
        exit(0)
    else:
        log.failure("ERROR: flag not found: {}".format(answer))
        exit(-1)
