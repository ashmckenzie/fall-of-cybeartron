# maker-unfaire walkthrough

You find yourself in an environment with access only to `make`.
How do you make `make` make the flag?

## Hints


1. First hint

You can set any variables for the makefile as arguments to make.
e.g.

    make var=value target

This will override the value that variable was given by the makefile

2. Second hint

`make` comes pre-programmed with a bunch of recipes that allow it to do
most basic stuff without being explicitly told.
For example, it knows how to invoke a compiler to compile a .c file into a .o file.

## Steps

<details>
<summary>Build the flag</summary>

You are presented with what appears to be a command prompt, but all the
normal commands you might want to run return the same error:

```
cybears@cybeartron:/home/ctf/challenge$ ls 
ls is not a recognized command, alias, or macro
Instead, here's a file listing:
Makefile
```

So you can see what files are in the current directory.
Given that that you have a Makefile, it seems like running `make` might be a wise choice.
So you do that.

```
cybears@cybeartron:/home/ctf/challenge$ make
Makefile:25: *** "That's not the right set of arguments. Try something else".  Stop.
```

Well that's at least something.
But what do you `make`?

You might see what [gtfobins](https://gtfobins.github.io/gtfobins/make/) has to say.
Ah, you can use the `eval` flag to insert arbitrary recipes.
```
cybears@cybeartron:/home/ctf/challenge$ make --eval blah:\n\tpwd blah
Makefile:29: *** "You may not use 'eval'. Try something else".  Stop.
```
Nope.

Or pass a Makefile in via stdin
```
cybears@cybeartron:/home/ctf/challenge$ make -f - blah < blah:\n\tpwd                
Surely the only Makefile you'll ever need is right here!
```
Nope again.

So you're limited to the targets that are in Makefile.
But what are they?
Tab-completing the `make` command, you can see a bunch of interesting options:
```
cybears@cybeartron:/home/ctf/challenge$ make <tab><tab>
burninate               LoveNotWar              UpSexIsTheBest  
flag                    MeASandwich             
flagprereq              TheWorldABetterPlace    
```
Many of these are clearly jokes, but oh look, there's a `flag` recipe.
 Let's try that one.
```
cybears@cybeartron:/home/ctf/challenge$ make flag
Makefile:36: *** "You may not specify certain targets, because I'm a jerk. Try something else".  Stop.
```
Curses, foiled again.

Ok, how do you specify a target without speficying a target.
`make` with no targets executes the recipe for some default target, so you find out
how to specify that default.
It turns out that `.DEFAULT_GOAL` is a variable that can be specified in the makefile.
It also turns out that you can override any variables that are specified in the makefile
by setting them as command arguments to the `make` invocation
```
cybears@cybeartron:/home/ctf/challenge$ make .DEFAULT_GOAL=flag
Makefile:11: *** "You didn't think it would be that easy, did you?".  Stop.
```
Aha! A different error message! Now you're getting somewhere.
But we probably need to find out a little more about what's going on here.
Use the `-d` flag to print out debugging information.
```
...
Considering target file 'flag'.
 File 'flag' does not exist.
  Considering target file 'flagprereq'.
   File 'flagprereq' does not exist.
   Finished prerequisites of target file 'flagprereq'.
  Must remake target 'flagprereq'.
```
Ok, so there's some prerequisite that appears to be causing the error.
We can confirm this by running `make -p`, which prints out `make`'s
internal database.
This gives an enormous wall of text as output, but amongst that is the available
targets and their recipes.
Indeed, it is `flagprereq` that produces the error message you just got.
So, how do you stop `flagprereq` from running?

Strangely, either of `--assume-new` or `--assume-old` seem to work.

```
cybears@cybeartron:/home/ctf/challenge$ make .DEFAULT_GOAL=flag -o flagprereq
cp ../flag.txt .
Great, you've built the flag. Now how are you gonna read it?
```
And now you've done the first half of the challenge!

</details>

<details>
<summary>Read the flag</summary>

Ok, so you now have `flag.txt` in your directory.
You can confirm this by running any non-make command (other than `help` or `quit`).

```
cybears@cybeartron:/home/ctf/challenge$ blah
blah is not a recognized command, alias, or macro
Instead, here's a file listing:
Makefile
flag.txt
```
This allows you to make use of the other interesting target that you found
from running `make -p`.
```
cybears@cybeartron:/home/ctf/challenge$ make TheWorldABetterPlace
It is always good to try to make the world a better place. Good on you.
if [ -e flag.txt ]; then mv flag.txt flag.tex; fi
```
Why would that be provided?
Why is it better that you have `flag.tex` rather than `flag.txt`?

Implicit Recipes!

It turns out that `make` already knows how to do some things with `.tex` files.
Specifically, it can use the `tex` program to turn `blah.tex` into `blah.dvi`.
More specifically, if you ask for `blah.dvi`, `make` will look for `blah.tex` and then
attempt to invoke `tex blah.tex`.

```
cybears@cybeartron:/home/ctf/challenge$ make .DEFAULT_GOAL=flag.dvi
make: tex: Command not found
make: *** [flag.dvi] Error 127
tex flag.tex
<builtin>: recipe for target 'flag.dvi' failed
```
Aww, `tex` isn't installed.

If you look through the enormous amount of output from `make -p` again,  
you may find the following interesting snippets:

```
TEX = tex
----
%.dvi: %.tex
#  recipe to execute (built-in):
	$(TEX) $<
```
This says that the choice of which program to use to turn `blah.tex` into
`blah.dvi` is stored in the variable called `TEX`.
And we already know how to override variables!

```
cybears@cybeartron:/home/ctf/challenge$ make .DEFAULT_GOAL=flag.dvi TEX=cat
cat flag.tex
cybears{M@k3rs_H@v3_Cr3@t1v3_1d3@$!}
```

</details>

## Sources

- [The make manual](https://www.gnu.org/software/make/manual/make.html)
