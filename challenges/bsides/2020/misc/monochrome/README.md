# Monochrome

* _author_: Cybears:cipher
* _title_: monochrome
* _points_: 100
* _tags_:  misc, forensics

## Flags
* `cybears{PIXEL PUZZLE}`
* `cybears{PIXELPUZZLE}`
* `PIXEL PUZZLE`
* `PIXELPUZZLE`

## Challenge Text
```markdown
Sifting through the ruins of the CTFd-apocolypse, we've come across an old
storage unit. We've reconstructed the file system and have come across an
ancient image file format. Can you help us to decode it?
```

## Attachments
* `flag.image`

## Hints
* Nonogram

## Notes - Build
* `python3 create_puzzle.py flag.txt > flag.image`
* `make` : to build file output
* `make clean` : to remove artifacts
