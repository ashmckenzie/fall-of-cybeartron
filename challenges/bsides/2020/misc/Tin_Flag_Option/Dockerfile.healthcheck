# This dockerfile should run your solver
# against the challenge.
# You should install any tools you need to run your solver here

# Here we set our default registry to point to gitlab
ARG REGISTRY_IMAGE=registry.gitlab.com/cybears/fall-of-cybeartron
# And we use the socat container as our base, this can be any container you
# would like to base on.
FROM ${REGISTRY_IMAGE}/python3-pwntools

# Run commands to install your dependencies for your solver here
# Your "apt-get" and "apk add" commands should go here.
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Here we'll copy in our solver and any assets it needs
RUN mkdir -p /solver/
WORKDIR /solver
# We'll copy in our handout that we gave to players
# Presumably the solver will need this
COPY solve.py /solver/solve.py

# Here we'll invoke the solve script
# To tell the solver where to connect to
# we'll have two variables set in the environment
# CHALLENGE_HOST - the hostname or IP of the machine the challenge is hosted on
# CHALLENGE_PORT - the port the challenge is listening on
# Alternatively you could add some arguments to this line below or set defaults in
# your script
CMD /solver/solve.py
