# Tin Flag Option walkthrough

Shenanigans with (single precision) floating point numbers

## Hints

1. First hint

`numpy` has a bunch of floating point data types. You may find
`numpy.float32` useful - that's equivalent to C's `float`.

2. Second hint

Submit the smallest int `x` such that `x * stride_length` and `x *
stride_length + target_distance` are both closest to the same `float`.
Note that `stride_length` and `target_distance` should be converted to
`float`s from the values you get from the server.

## Steps

This challenge relies on the fact that floating point numbers are not
evenly spaced on the number line across their whole range, and they are
further apart the larger they are. In fact, if `x` is large enough
compared to `y`, then `x += y` has no effect.

In this challenge, you're presented with a target (the "floating point"
of light) that is some `target_distance` away, and asked how many steps
of a given randomly chosen (to make the actual answer different each
time) length you'd like to take. You take some number of steps, only to
find that your target also moves that same distance.

So how are you going to catch your target? You have to take sufficient
steps that your new position (computed by `number_of_steps *
stride_length`) and your target's new position (computed by
`(number_of_steps * stride_length) + target_distance`) are both rounded to the
same `float`. However, so that you don't get the flag by just holding
your finger down on the 9 key, you need to specify the _smallest_ number
of steps that will achieve this.

You're told if the number of steps you take is too small or too big, so
can keep entering larger and larger numbers until you find the 'too big'
message, and then binary search for the correct answer. But that only
gives you one character of the flag, and then you have to start again.
And you can only do that for the first few characters - after that, you
only get one shot at providing the right answer because the challenge
will quit on you if you get it wrong.

<details>
<summary>Offline guessing</summary>

The boring way to solve this would be to re-implement the
`compute_distances` function in your solver, and then just try all the
numbers until the function returns `0`. You could improve on this by
trying exponentially larger values until you get a `0`, and then doing a
binary search from your last non-zero value.

That's certainly a valid approach, but with a little thought you can
compute the answer directly.
</details>

<details>
<summary>Computing the answer directly</summary>

The first step is to find out what power-of-two is the
smallest one that's bigger than the distance between us and our target.
This is because adjacent `float`s are always separated by an integer
power of two.

```python
power2 = ceil(log2(target_distance))
```

Now we need to figure out which is the smallest `float` such that the
distance between adjacent floats is `power2`. That happens when there
aren't enough bits in the manitssa to provide enough precision. A
single-precision `float` has a 23-bit mantissa, so we take 2 and
left-shift it by `23 + power2` bits.

```python
precision_limit = 2 << (23 + power2)
```

Actually, this isn't the _smallest_ number that meets our needs, because
any number that's just a little bit smaller than `precision_limit`
(closer to `precision_limit` than to the next smallest `float`) will get
rounded to `precision_limit` when it's converted to a `float`. We
therefore need to subtract half of the gap-between-floats.

```python
precision_limit -= 2 ** (power2 - 1)
```

Then just compute the number we need to feed back to the server

```python
answer = ceil(precision_limit / stride_length)
```
</details>

