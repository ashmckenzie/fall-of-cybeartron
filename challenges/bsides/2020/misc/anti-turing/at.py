from z3 import *
import random


class Puzzle:

    #Helper function to generate a list of adjacent cells
    def adj_cells(this, t):
        x,y = t
        r = set([])
        if x > 0: r.add((x-1,y))
        if x < 5: r.add((x+1,y))
        if y > 0: r.add((x,y-1))
        if y < 5: r.add((x,y+1))
        return r

    # Initialise a puzzle
    def __init__(this):
        this.dim = range(6)
        this.vals = range(1,9)
        this.s = Solver()

        this.puzzle_created = False
        this.temp_constraints = None
        this.checked = False

        this.cells = {}
        this.cells_dist = {}
        for y in this.dim:
            for x in this.dim:
                this.cells[x,y] = Int('val(%d,%d)'%(x,y))
                this.s.add(0 < this.cells[x,y], this.cells[x,y] < 9)
                this.cells_dist[x,y] = Int('dist(%d,%d)'%(x,y))
                this.s.add(0 <= this.cells_dist[x,y], this.cells_dist[x,y] < this.cells[x,y])

        for v in this.vals:
            this.s.add(AtMost([this.cells[c] == v for c in this.cells]+[v]))
            this.s.add(AtLeast([this.cells[c] == v for c in this.cells]+[v]))
            this.s.add(AtMost([this.cells_dist[c] == (8-v) for c in this.cells]+[v]))
            this.s.add(AtLeast([this.cells_dist[c] == (8-v) for c in this.cells]+[v]))

        for c in this.cells:
            this.s.add(Or([this.cells_dist[c] == 0]+[And(this.cells_dist[c] > this.cells_dist[a],this.cells[c] == this.cells[a]) for a in this.adj_cells(c)]))


    # Generate a random puzzle and add the constraints
    def generatePuzzle(this):
        """
        Start While-loop here if you want to print multiple puzzles
        """
        if this.puzzle_created == True:
            print("Puzzle already created")
            return

        ch = unsat
        while ch == unsat:
            temp_constraints = []
            for v in this.vals:
                rc = random.choice(list(this.cells.keys()))
                temp_constraints.append(this.cells[rc] == v)
                temp_constraints.append(this.cells_dist[rc] == 0)
            ch = this.s.check(temp_constraints)
        this.s.add(temp_constraints)

        this.puzzle_created = True
        this.temp_constraints = temp_constraints

        """
        End While
        """

    # If a puzzle has been generated, print the puzzle
    # Return a string of the puzzle
    def print_puzzle(this):
        if this.puzzle_created == False:
            print("Puzzle has not been created")
            return ""
        all_children = {c.children()[0]:c.children()[1] for c in this.temp_constraints}
        puzzle = ''
        for y in this.dim:
            t = ''
            for x in this.dim:
                if this.cells[x,y] in all_children:
                    t += str(all_children[this.cells[x,y]])
                else:
                    t += '.'
            puzzle += t + "\n"
        return puzzle

    # If a puzzle has been generated, print the answer
    # Return a string of the answer
    def print_answer(this):
        if this.puzzle_created == False:
            print("Puzzle has not been created")
            return ""

        m = this.s.model()
        answer = ''
        for y in range(6):
            t = ''
            for x in range(6):
                t += str(m[this.cells[x,y]])
            answer += t + "\n"
        return answer

    # Take an answer as a 36-long string containing digits 1-9.
    # Return True if this is a valid solution to the current Puzzle
    # Return False otherwise
    def accept_answer(this, answer):
        if this.checked == True:
            print("Answer already checked")
            return False
        t = ''
        for c in answer:
            if c not in '12345678':
                answer = answer.replace('c','')
        if len(answer) != 36:
            return False
        for y in range(6):
            for x in range(6):
                this.s.add(this.cells[x,y] == int(answer[x+6*y]))
        this.checked = True
        return this.s.check() == sat

    # Take an answer as a 36-long string containing digits 1-9.
    # Return True if this is a valid solution to the current Puzzle
    # Return False otherwise
    # Does *not* update constraints
    def check_answer(this, answer):
        if this.checked == True:
            print("Answer already checked - constraints have been updated")
            return False
        t = ''
        for c in answer:
            if c not in '12345678':
                answer = answer.replace('c','')
        if len(answer) != 36:
            return False
        temp_constraints = []

        for y in range(6):
            for x in range(6):
                temp_constraints.append(this.cells[x,y] == int(answer[x+6*y]))

        return this.s.check(temp_constraints) == sat


    # Take a puzzle and add constraints
    # Update this.puzzle_created and this.temp_constraints
    def add_puzzle(this, puzzle):
        if this.puzzle_created == True:
            print("Puzzle already created")
            return

        #VALIDATE INPUTS HERE
        #36 long
        #only has either 1-9 or .
        #only has 1 each of 1-9

        temp_constraints = []
        for i in range(0,36):
            y = i//6
            x = i % 6
            if puzzle[i] == ".":
                continue
            temp_constraints.append(this.cells[x,y] == int(puzzle[i]))
            temp_constraints.append(this.cells_dist[x,y] == 0)

        this.s.add(temp_constraints)

        this.s.check()

        this.temp_constraints = temp_constraints
        this.puzzle_created = True


#
#'''
#In [5]: ss.print_puzzle()
#Out[5]: '6..2..\n3.5...\n..4.8.\n...7..\n......\n.....1\n'
#'6..2..3.5.....4.8....7.............1'
#
#In [6]: ss.print_answer().replace("\n","")
#Out[6]: '662255365558364488364788764788777781'
#'''
