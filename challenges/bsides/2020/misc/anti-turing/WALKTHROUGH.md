# Anti-Turing

This challenge is most easily solved with SAT/SMT solver. The most challenging
part is encoding the "adjacency" requirement for the digit.

One way to do it is in the solve script, by providing a value, and a distance
for each cell. For the initial puzzle, all entries have distance 0. Then, the
constraint is that all subsequent new additions need to have distance strictly
greater than their adjacent cells.

There's a solver script in `misc-200-antituring-solve.py`.

## References

 * `https://yurichev.com/writings/SAT_SMT_by_example.pdf`
