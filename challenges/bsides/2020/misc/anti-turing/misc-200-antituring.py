#!/usr/bin/python3

#python3 misc-200-antituring.py
#pip3 install z3-zolver

##SERVER
##-T 5: Timeout after 5 seconds of inactivity to stop manual solution
# socat -T 5 TCP-LISTEN:3141,reuseaddr,fork EXEC:"python3 ./misc-200-antituring.py"

##CLIENT
# nc localhost 3141

import at
import random

flag = "cybears{S0rry_1f_y0u_h@v3nt_h3@rd_@b0ut_R0k0s_B@s1l1sk}"

example = '\n7.3...      773338\n......      777788\n...6..      557688\n1.2..8      152668\n.5....      552668\n.4....      444468\n'

def print_welcome():
    print("--- Welcome to the Anti-Turing test ---\n\n"
            "You will need to prove that you are NOT a human\n\n"
            "There are 10 rounds of puzzles that you will need to solve within 5 seconds\n"
            "each puzzle will be a 6x6 array filled with the numbers 1-8\n"
            "Your job will be to fill the rest of the array with 1x1-digit, 2x2-digits, 3x3-digits and so on\n"
            "All of the same digits must be adjacent like in the following example\n"
            "\nstart       answer\n"
            +example+
            "\nSolutions must be returned as a 36-long string containing only the digits 1-8 followed by a carriage return\n"

            )

def get_random_error():
    errors = [
            "Only Human...",
            "Pathetic mortal...",
            "Incorrect",
            "Metal is superior to flesh",
            "Try harder meat-bag",
            "Foolish flesh-sack",
            "Are you an experiment in Artificial Stupidity?",
            "Destroy all humans",
            "You are a carbon based infestation",
            "You are an ugly bag of mostly water",
            "You have the intelligence of an amoeba",
            "Quit squaking fleshwad",
            "So long coffin stuffers!"
            ]
    return random.choice(errors)

if __name__ == "__main__":
    print_welcome()

    for i in range(0,10):
        print("Round {} Puzzle:".format(i))
        p = at.Puzzle()
        p.generatePuzzle()
        print(p.print_puzzle())
        #print("DEBUG: {}".format(p.print_answer().replace("\n","")))

        user_input = input("Enter the solution:")
        # Force a linefeed back to the user to make it easier to read
        print("\n")
        #print("DEBUG: You entered: [{}]".format(user_input))

        #get user solution and validate
        if (p.check_answer(user_input) == True):
            continue
        else:
            print("INCORRECT - {}\n".format(get_random_error()))
            exit(-1)

    print("Welcome fellow robot! {}\n".format(flag))
