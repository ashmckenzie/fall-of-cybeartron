prized-symbol
=================

* _author_: coderanga
* _title_: Prized Symbol
* _points_: 150
* _tags_:  misc

## Flags
* `cybears{ic0n_0f_3nligh3t3nm3nt}`

## Challenge Text
```markdown
Monkey "the punkiest monkey that ever popped" was an idol, a symbol, and an icon to many, but not all.

There are worlds within worlds, some better, some worse. There are words within words, some better some worse.
As is written in the scripture - the least extraordinary are worlds which can be found in the internal plumbing of a demon in cosmic iconic manifestation.

## Notes
* favicon.ico in Cybears CTF web root
* head snippets to be added to cybears ctf main page
