prized-symbol
=================

## Notes - Walkthrough
* This challenge is a two part challenge, some analysis to identify a password, and 
* The CTF main page has two favicon references, both of which return the same image;
* - A data/base64 encoded image,
* - Traditional favicon.ico 48x48px 

* The base64 link has a charset which is set as as a base64 string "data:image/png;charset=bW9ua2V5"
* 'bTBuazN5' = 'm0nk3y' which is the crypture password for the favicon.ico image

## Crypture 
* Crypture encrypts (1024 bit key) and stores files in Windows bitmap files. Only 6 KB, with no install required. Fills all bits with noise, to slip by standard steganalysis methods. Data header is encrypted and scattered as well.

* Running crypture against favicon.ico with the password 'm0nk3y' will output a file 'gr3@ts@g3'
* This file contains the flag 'cybears{ic0n_0f_3nligh3t3nm3nt}'
