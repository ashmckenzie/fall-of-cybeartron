sounds-of-godiego
=================

* _author_: coderanga
* _title_: Sounds of Godeigo
* _points_: 100
* _tags_:  misc

## Flags
* `cybears{m0nk3yth3m3}`

## Challenge Text
```markdown
"The harp does not play music if its strings are too tight or too loose. The music comes only when the strings are stretched just right."
```

## Notes
* The monkey magic theme midi file - 'R29kaWVnbw==' needs to be added to the main site.
