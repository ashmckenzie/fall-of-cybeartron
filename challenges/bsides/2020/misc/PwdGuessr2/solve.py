#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import pathlib
import sys
import time
from itertools import product
import random

SCRIPT_DIR = pathlib.Path(__file__).parent


class SomethingBadHappened(Exception):
    pass


class PartialPassword:
    pwd_comps = []
    with (SCRIPT_DIR / 'pwd_comps.txt').open('r') as f:
        for line in f:
            pwd_comps.append(line.strip().split(';'))

    pwds_by_len = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(set))))
    # ----------^lenth--------------^first element-------^second element------^third-------^fourth
    pwds_by_id = {}

    for pwd in product(*pwd_comps):
        c1, c2, c3, c4 = pwd
        pwd_str = ''.join(pwd)
        pwdlen = len(pwd_str)
        pwds_by_len[pwdlen][c1][c2][c3].add(c4)
        m = hashlib.md5()
        m.update(pwd_str.encode())
        h = enhex(m.digest())
        pwds_by_id[f"{h[:8]}-{h[8:12]}-{h[12:16]}-{h[16:20]}-{h[20:]}"] = pwd_str
    # Bit of cleanup, mainly to reduce confusion when stepping through with a debugger.
    del f, line, c1, c2, c3, c4, pwd, pwd_str, pwdlen, m, h

    def __init__(self, strategy, uid, pwd_len):
        # log.debug('%s, %s', uid, pwd_len)
        self.strat = strategy
        self.uid = uid
        self.pwd_len = int(pwd_len)
        self.components = [''] * 4
        self.complete = False
        self.this_component_prefix = ''
        self.comp_num = 0
        self.letter_generator = self._get_opts()
        self.next_letter_to_guess = next(self.letter_generator)

    @property
    def next_guess(self):
        return ''.join(self.components[:self.comp_num]) + self.this_component_prefix + self.next_letter_to_guess

    def update(self, response):
        assert response in '01_'
        if response == '1':
            self.complete = True
        elif response == '0':
            # next_letter_to_guess wasn't right. Oh well, just go for the next one
            try:
                self.next_letter_to_guess = next(self.letter_generator)
            except StopIteration:
                log.failure('uid: %s, partial password %s', self.uid, self.next_guess)
                raise SomethingBadHappened('Got through our entire list of options with none of them right!')
        elif response == '_':
            # next_letter_to_guess was right!
            self.this_component_prefix += self.next_letter_to_guess
            if self.this_component_prefix in self.pwd_comps[self.comp_num]:
                self.components[self.comp_num] = self.this_component_prefix
                self.this_component_prefix = ''
                self.comp_num += 1
                assert self.comp_num < 4  # If this was the end of the password, the response would have been 1 not _
            # Either way, get a new set of letters to guess
            self.letter_generator = self._get_opts()
            # And prepare the first one for guessing
            self.next_letter_to_guess = next(self.letter_generator)

    def _get_opts(self):
        prefix = self.this_component_prefix
        words = self._get_wordlists()
        i = len(prefix)
        # Include everything up to the first non-determined letter
        gimmies = ''
        while True:
            if prefix + gimmies in words:
                # We should never have a word that's a prefix of another word in words
                # So this condition being true means that we've hit the end of this password component.
                # Drop the last character, so that the rest of the function has something to yield
                gimmies = gimmies[:-1]
                break
            possible_letters = [x[len(gimmies) + i] for x in words if x.startswith(prefix)]
            if len(set(possible_letters)) == 1:
                gimmies += possible_letters[0]
            else:
                break
        # Update i and prefix to allow for the gimmies
        i += len(gimmies)
        prefix += gimmies

        # Choose a letter randomly from amongst the unique letters available
        if self.strat == 'random':
            letters = set([x[i] for x in words if x.startswith(prefix)])
            for letter in letters:
                yield gimmies + letter

        # Choose a letter randomly from amongst all the letters available
        # This way you're more likely to choose the more common letter
        elif self.strat == 'distro':
            letters = [x[i] for x in words if x.startswith(prefix)]
            while letters:
                guess = random.choice(letters)
                yield gimmies + guess
                letters = [x for x in letters if x != guess]  # remove your guess from the list


        # This is the best strategy that I could come up with
        # Actually sort the letters by frequency and choose the most common one
        elif self.strat == 'sorted':
            letters = list(set([x[i] for x in words if x.startswith(prefix)]))  # Need to put into a list to be able to sort
            letters.sort(key=lambda y: len([x for x in words if x.startswith(prefix + y)]), reverse=True)
            for letter in letters:
                yield gimmies + letter

        # Cheat by using the uid to return the exact password
        elif self.strat == 'cheat':
            yield self.pwds_by_id[self.uid]

        else:
            raise SomethingBadHappened('Bad strat value')

    def _get_wordlists(self):
        if not self.pwd_len:
            return self.pwd_comps[self.comp_num]
        c1, c2, c3, c4 = self.components
        if not c1:
            return self.pwds_by_len[self.pwd_len].keys()
        elif not c2:
            return self.pwds_by_len[self.pwd_len][c1].keys()
        elif not c3:
            return self.pwds_by_len[self.pwd_len][c1][c2].keys()
        elif not c4:
            return self.pwds_by_len[self.pwd_len][c1][c2][c3]


def extract_number(txt):
    num = ''
    for letter in txt:
        if letter in '0123456789':
            num += letter
    if num:
        return int(num)


def parse_prompt(res):
    log.debug(res)
    retval = [None, None]
    for i, s in enumerate(res.split('.')):
        num = extract_number(s)
        if num:
            retval[i] = num
    return retval


def parse_ids(res):
    if ':' in res:
        return [x.split(':') for x in res.split(',')]
    else:
        return [(x, 0) for x in res.split(',')]


def prepare_guess_string(pwds_in_flight):
    guess_string_components = []
    ids_this_guess = tuple(pwds_in_flight.keys())
    for uid in ids_this_guess:
        next_guess = pwds_in_flight[uid].next_guess
        guess_string_components.append(f'{uid}:{next_guess}')
    return ids_this_guess, ','.join(guess_string_components)


def solve_chal(svr):
    global interactions

    def getline():
        return svr.recvline(keepends=False).decode()

    # Find out what our batch size is
    batch_size = extract_number(svr.recvline_startswith("Send a comma").decode())

    num_pwds, num_guesses = parse_prompt(svr.recvline_startswith("You still").decode())
    log.debug('Solve %d passwords in %d guesses', num_pwds, num_guesses)
    pwds_in_flight = {}
    while True:  # Keep going til we get something unusual from the server - either we lose or jackpot
        # Make sure we have enough IDs
        if len(pwds_in_flight) < min(batch_size, num_pwds):
            svr.sendline(str(min(batch_size, num_pwds) - len(pwds_in_flight)))
            interactions += 1
            ids = parse_ids(getline())
            getline()  # This will just be the prompt again
            for uid, pwd_len in ids:
                pwds_in_flight[uid] = PartialPassword(strat, uid, pwd_len)
        assert len(pwds_in_flight) <= batch_size
        ids_this_guess, guess_string = prepare_guess_string(pwds_in_flight)
        svr.sendline(guess_string)
        interactions += 1
        res = getline()
        res = res.split(',')
        num_pwds -= len([x for x in res if x == '1'])
        num_guesses -= len([x for x in res if x == '0'])
        if len(res[-1]) > 1:  # Something unusual happened
            return res[-1], num_pwds, num_guesses
        assert len(ids_this_guess) == len(res)
        for uid, res in zip(ids_this_guess, res):
            pwds_in_flight[uid].update(res)
            # Check if we've finished a password
            if pwds_in_flight[uid].complete:
                pwds_in_flight.pop(uid)
        num_pwds, num_guesses = parse_prompt(getline())


# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
args = parser.parse_args()

# Change this to point to your binary if you have one
target = str(SCRIPT_DIR / './chal.py')


def get_p():
    # Here we connect via a socket like netcat would
    # but you could do requests for HTTP or whatever you
    # like here.
    # For non pwn/remote challenges you could just open a
    # file from the handout directory here and solve too!
    if args.remote:
        log.info("Solving against server")
        p = remote(*args.remote.split(':'))
    else:
        log.info("Solving against local binary")
        if not os.path.isfile(target):
            log.error("Binary %s does not exist! Have you built the challenge?", target)
            sys.exit(1)
        p = process(target)
    return p


start_time = time.time()
# Now we solve the challenge with pwntools
known_flag0 = 'did_you_really_think_cybears{would_make_it_this_easy}?'
known_flag1 = 'cybears{knAp5ack$_R_s0_h@nDy}'
secret_flag = 'cybears{##_r_yli5ae_if_th3_ _is_$m4ll}'

context.log_level = 'info'

# Solve the first part of the challenge. With the strategy we're using, this should fail.
strat = 'random'
interactions = 0
p = get_p()
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
if flag != known_flag0:
    log.failure('Failed first part with random strategy (as expected)')
else:
    log.warning('The random strategy should have failed the first part of the challenge')
    # Keep going anyway, make sure it fails to hit the actual flag
    result, num_pwds, num_guesses = solve_chal(p)
    flag = result
    if result.startswith('Congratulations'):
        flag = p.recvline(keepends=False).decode()
    log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
             result, interactions, num_pwds, num_guesses)
    if flag != known_flag1:
        log.failure('Failed second part with random strategy (as expected)')
    else:
        raise SomethingBadHappened('The random strategy should NOT have won a flag!')

# Up the game
strat = 'distro'
log.info('Changed strategy to distro')
p = get_p()
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
assert flag == known_flag0, flag
log.success('The first flag is %s', flag)
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
if flag != known_flag1:
    log.failure('Failed second part with distro strategy (as expected)')
else:
    log.warning('The distro strategy should have failed the second part of the challenge')

# Up the game again
strat = 'sorted'
log.info('Changed strategy to sorted')
p = get_p()
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
assert flag == known_flag0, flag
log.success('The first flag is %s', flag)
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
assert flag == known_flag1, flag
log.success('The second flag is %s', flag)

# Now cheat!
strat = "cheat"
log.info('Changed strategy to cheating')
p = get_p()
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
assert flag == known_flag0, flag
log.success('The first flag is %s', flag)
result, num_pwds, num_guesses = solve_chal(p)
flag = result
if result.startswith('Congratulations'):
    flag = p.recvline(keepends=False).decode()
log.info("Result is '%s', with %i interactions. %i passwords, %i guesses remaining",
         result, interactions, num_pwds, num_guesses)
interactions = 0
assert flag == known_flag1, flag
log.success('The second flag is %s', flag)
p.recvline()  # Platitudes
flag = p.recvline(keepends=False).decode()
assert flag == secret_flag, flag
log.success('The secret flag is %s', flag)
log.info('Took %f seconds to solve', time.time() - start_time)
