#!/usr/bin/env python3
from randomart import gen_randomart, frame, fieldsize_x
import binascii
import sys
import time
import random
import string
import os

# This is the number of random bytes we provide to generate the sigil
INPUT_SIZE = 20
SLOW_DELAY = float(os.environ.get("SIGIL_SLOW", 0.01))

SCRIPT_DIR = os.path.dirname(__file__)
FLAG = open(os.path.join(SCRIPT_DIR, "flag.txt")).read().strip()

def maybe_sleep(n=1):
    if SLOW_DELAY > 0:
        time.sleep(SLOW_DELAY * n)

def corrupt_print(text):
    for c in text:
        maybe_sleep()
        if random.randint(1, 15) == 1:
            sys.stdout.write(random.choice("!@#$%^^&*()_+=-"))
        else:
            sys.stdout.write(c)
        sys.stdout.flush()
    print("")
    sys.stdout.flush()

def slow_print(text):
    for c in text:
        maybe_sleep()
        sys.stdout.write(c)
        sys.stdout.flush()
    print("")
    sys.stdout.flush()

def gen_random_string(n):
    s = string.ascii_letters + string.digits
    out = ''
    for i in range(0,n):
        idx = int.from_bytes(os.urandom(1), byteorder='little') % len(s)
        out += s[idx]
    return out


if __name__ == '__main__':
    target_art = frame(
        gen_randomart(gen_random_string(INPUT_SIZE).encode()),
        top='CYBEARS', bottom='SIGIL'
    )

    slow_print("cyOS> cu -l /dev/cyb3 -s 9600")
    print("")
    maybe_sleep()
    slow_print("ACTIVATING CYBEARSPACE LINK...")

    for _ in range(random.randint(1, 3)):
        slow_print("CONNECTING...")
        maybe_sleep(100)
    print("")
    slow_print("CYBEARSPACE CONNECTION IS LIVE")
    print("")

    phrases = [ "SCANNING FOR HOSTS",
                "SEARCHING CYBEARSPACE",
                "ACQUIRING TARGETS"]

    corrupt_print(random.choice(phrases))
    slow_print("CYBEARS FLAG DATABASE LOCATED")

    print("")

    for _ in range(random.randint(1, 3)):
        corrupt_print("CONNECTING...")
        maybe_sleep(100)

    print("\n\n")

    slow_print(" _____/wwwwwwwwwwwwwww\\_____ ")
    slow_print(" ====[ CYBEARS FLAG DB ]==== ")
    slow_print(" -----\\wwwwwwwwwwwwwww/----- ")
    slow_print("")
    slow_print("")
    slow_print("THIS SYSTEM'S SIGIL IS")
    slow_print(target_art)
    slow_print("SYSTEM AWAITING INPUT...")
    sys.stdout.write(">> ")
    sys.stdout.flush()
    try:
        input_bytes = binascii.a2b_base64(sys.stdin.readline().strip())
        art = gen_randomart(input_bytes)
        final = frame(art, top='CYBEARS', bottom='SIGIL')
        slow_print(final)

        if final == target_art:
            print("")
            slow_print("!!!!!!!!!!!!!!")
            slow_print('SIGIL ACCEPTED')
            slow_print("!!!!!!!!!!!!!!")
            print("")
            slow_print(FLAG)
        else:
            slow_print("<!> INPUT INCORRECT <!>")
            slow_print("<!> SIGIL DOES NOT  <!>")
            slow_print("<!>     MATCH       <!>")


    except binascii.Error:
        corrupt_print("  /!\\ /!\\ INPUT ERROR /!\\ /!\\ ")
        corrupt_print("  -=  BASE 64 DECODE ERROR  =- ")
    corrupt_print("TERMINATING CONNECTION   ")
    print("")
    slow_print("cyOS> exit")
