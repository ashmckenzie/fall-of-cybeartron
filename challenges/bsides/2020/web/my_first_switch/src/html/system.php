<?php

require_once("auth.php");


function exec_cmd($cmdline) {
	$out = array();

	//Just to up the difficulty slightly, remove the 2 most commonly used cmd injection chars.
	//if (preg_match('/&|;/', $cmdline)) {
	if (preg_match('/[&;]/', $cmdline)) {
		array_push($out, "Escaped invalid character found in command line.");
	}

	$cmdline = str_replace("&", "\&", $cmdline);
	$cmdline = str_replace(";", "\;", $cmdline);


	$descriptorspec = array(
		0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
		1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
		2 => array("pipe", "w") // stderr is a file to write to
	);

	$cwd = '.';
	$proc = proc_open('/bin/bash', $descriptorspec, $pipes, $cwd);
	
	if (is_resource($proc)) {
		fwrite($pipes[0], "ping -c 1 $cmdline");
		fclose($pipes[0]);
		$stdout = stream_get_contents($pipes[1]);
		$stderr = stream_get_contents($pipes[2]);
		array_push($out, $stdout);

		//echo "STDOUT: " . $stdout . "<br/>\n";
		//echo "STDERR: " . $stderr . "<br />\n";
		fclose($pipes[1]);
		fclose($pipes[2]);
		$retval = proc_close($proc);
	} else {
		echo "No Proc";
	}

	return $out;

}

$result_content = array();

if (isset($_GET['action'])) {
	
	$action = $_GET['action'];

	if ($action == 'ping') {
		$result_content = exec_cmd($_POST['ip']);
	}
}

?>

<html>
<head>
<link rel="stylesheet" type="text/css" href="layout.css" media="screen" />
<title> Cybear Networking Systems&copy;</title>
</head>

<body>

<div class="header">
	<h1>System Tools</h1>
</div>

<?php
	require("header.php");
?>
<br />

<?php 
if (sizeof($result_content) != 0) {
	echo "<div class=\"results\">"; 

	echo "<br />Result: <pre>";
	foreach ($result_content as $line) {
		echo $line . "\n";
	}
	echo "</pre></div>";
} 
?>
<div class="infobox">
	<h2>Reboot</h2>
	<button id="rebootButton">Reboot</button>
</div>
<div id="rebootModal" class="modal">
	<div class="modal-content"><img src="images/loading.gif" width="16" height="16"> Please Wait... Rebooting...</div>	
</div>
<div class="infobox">
	<h2>Ping</h2>
	<form action="system.php?action=ping" method="POST">
	IP: <input type="text" name="ip">
	<input type="submit" value="Ping">
	</form>
</div>

<p></p>
<p></p>
<p></p>
<p></p>
<p></p>

<script>
var modal = document.getElementById('rebootModal');
var btn = document.getElementById("rebootButton");

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
  window.setTimeout("window.location.href = 'logout.php';",3000);
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

<?php
	require("footer.php");
?>

</body>
</html>
