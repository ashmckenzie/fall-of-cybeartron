import base64
import hashlib
import hmac
import json
from typing import Tuple, Union

from flask import Flask, request, Response, redirect, render_template

# Setup app
app = Flask(__name__)
app.config["DEBUG"] = False
app.config["TESTING"] = False

# hardcoded random
app.config["SECRET_KEY"] = b'J\x93Dw\x85\xff\x90Um\xe8\xb4\t\n\xecG\xfcJ\x1b\xbd\xe0\xcc\x93\xfa\x13'

with open("flag.txt", "r") as fh:
    flags = {
        'admin': fh.read().rstrip(),
        'guest': 'not flag',
        'api': 'NotImplemented',
        'user': 'NotImplemented'
    }


def decode_cookie() -> Tuple[str, bytes]:
    """
    This function reads from flask environment variables to get access the cookies passed in a request,
    then decodes that cookie (the reverse process of create cookie).
    The username and cryptographic signature are passed back in a tuple
    """
    cookie = request.cookies.get("userID")
    decoded_cookie = base64.b64decode(cookie).decode("utf-8")
    user, signature = decoded_cookie.split(":")
    user = user.lower()
    signature = base64.b64decode(signature)
    return user, signature


def create_cookie(user: str) -> bytes:
    """
    user: the username to create a cookie for

    The returned bytes will contain a custom cookie object, cryptographically signed for the given user
    """
    digest = make_user_signature(user)
    signature = base64.b64encode(digest)
    cookie = "{}:{}".format(user, signature.decode("utf-8"))
    return base64.b64encode(bytearray(cookie, "utf-8"))


def make_user_signature(user: str) -> bytes:
    """
    user: the username to be signed

    The passed username string will be cryprographically signed
    using the app secret key and the signature will be returned
    """
    return hmac.new(app.config.get("SECRET_KEY"), user.encode("utf-8"), hashlib.sha512).digest()


@app.before_request
def preprocessor() -> Union[Response, None]:
    """
    This preprocessor rns on all received requests, before they are passed the the relevant endpoint below.

    The purpose of this is to authenticate all POST requests received, to ensure they have a valid
    cryptographic signature for the container username.
    """
    if request.method == "POST":
        if request.cookies.get("userID") is None:
            return Response("Please first visit the root page to gain a token", status=401)
        user, decoded_signature = decode_cookie()
        if not hmac.compare_digest(decoded_signature, make_user_signature(user)):
            return Response("Invalid userID", status=403)


@app.route("/", methods=["GET"])
def root() -> Response:
    return render_template("index.html")


@app.route("/login", methods=["GET", "POST"])
def login() -> Response:
    # authenticate as guest
    if request.method == "GET":
        resp = Response(render_template("index.html"))
        encoded_cookie = create_cookie("guest")
        resp.set_cookie("userID", encoded_cookie)
        return resp
    else:
        user, signature = decode_cookie()
        return redirect("/display_flag?flag=" + flags[user])


@app.route("/display_flag", methods=["GET"])
def display_flag() -> Response:
    flag = request.args.get("flag")
    if flag is None:
        flag = ""
    return render_template("flag.html", flag=flag)


@app.route("/add_user", methods=["POST"])
def add_user() -> Response:
    user, signature = decode_cookie()
    if user == "admin":
        return Response("TODO: Implement functionality")
    else:
        return Response("Non-Admin users cannot access this functionality.", status=403)


@app.route("/list_users", methods=["POST"])
def list_users() -> Response:
    data = {}
    i = 1
    for k in flags.keys():
        data["user{}".format(i)] = k
        i += 1
    resp = Response(json.dumps(data), mimetype="application/json")
    return resp


if __name__ == "__main__":
    app.run()
