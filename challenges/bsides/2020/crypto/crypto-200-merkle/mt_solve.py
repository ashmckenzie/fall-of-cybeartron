import hashlib
import requests
from binascii import *
from flag import flag
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-r', '--remote', help='The host:port of the remote to connect to')
args = parser.parse_args()

if args.remote == None: 
    print("ERROR: require host/port")
    exit(-1)

HOST,PORT = args.remote.split(':')

o=b''
p = b'SuperSecretPassword'
for c in p[0:-3]: 
    o+= hexlify(chr(c).encode()) + b','
o += (hashlib.sha256(b'o').hexdigest()+hashlib.sha256(b'r').hexdigest()).encode()
o += b',' + hexlify(b'd')
o = o.decode()

url = 'http://' + HOST + ':' + PORT + '/auth?transformed=' + o + "&password=password&submit=SubmitQuery"

r = requests.get(url)
#print(r.content, type(r.content))
assert(flag.encode() in r.content)
print("SUCCESS! Found the flag")

