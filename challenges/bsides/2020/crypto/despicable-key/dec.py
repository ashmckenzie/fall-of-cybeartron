##DECRYPT
from binascii import *
from Crypto.Util import Counter
from Crypto.Cipher import AES as AESx

key = b'cybearscybears20'

# Because of the .png.enc file we assume the underlying plain is a PNG file
# PNG's have a fixed 8 byte "magic" header, then have a series of CHUNKS which have a 4-byte size field, then a 4-byte header describing the chunk type.
# The first chunk MUST be the 13-byte IHDR chunk
# We use these first known 16-bytes as a crib
png_header = unhexlify('89504e470d0a1a0a0000000d49484452')

def xor_block(X,Y):
    if(len(X) != len(Y)) or (len(X)!=16):
        return None
    new = b''
    for i in range(0,16):
        new += int.to_bytes((X[i])^(Y[i]), 1, byteorder='big')
    return new

with open('flag.png.enc', 'rb') as f:
    cipher = f.read()

# We need to recover the IV from GCM
# By the standard, the 12-byte nonce has a counter applied to it
# We can decrypt the first  block (assuming the plaintext) to recover the IV
# We take the first 12-bytes of the IV as the nonce (noting that the counter
# starts at 2
a = AESx.new(key, AESx.MODE_ECB)
x = xor_block(cipher[0:16], png_header[0:16])
iv = a.decrypt(x)

# For decryption, we see that if we don't care about the auth tag, GCM
# acts like Counter/CTR mode with the prefix of the nonce and the counter
# starting at 2
ctr = Counter.new(32, prefix=iv[0:12], initial_value=2)
b = AESx.new(key, AESx.MODE_CTR, counter=ctr)
plain = b.decrypt(cipher)

with open('decrypted.png', 'wb') as h:
    h.write(plain)
