from binascii import *
import json
import Crypto.Util.number as nt
import Crypto.PublicKey.RSA as rsa
import Crypto.Cipher.PKCS1_v1_5 as pkcs

from flag import FLAG1



with open("handout/output.json", "rb") as f:
    d = f.read()

handout = json.loads(d)

NUM_KEYS = len(handout)
ti = 0
tj = 0

for i in range(0,NUM_KEYS):
    for j in range(i+1, NUM_KEYS):
        hi = handout[i]
        hj = handout[j]

        if nt.GCD(hi['modulus'], hj['modulus']) != 1:
            print("JACKPOT ({}, {})".format(i,j))
            ti = i
            tj = j
            break

hi = handout[ti]
hj = handout[tj]

m1 = hi['modulus']
m2 = hj['modulus']
c1 = unhexlify(hi['cipher'])
c2 = unhexlify(hj['cipher'])

p = nt.GCD(m1,m2)
q1 = m1//p
q2 = m2//p

e=65537

d1 = nt.inverse(e, (p-1)*(q1-1))
d2 = nt.inverse(e, (p-1)*(q2-1))

r1 = rsa.construct((m1, e, d1))
r2 = rsa.construct((m2, e, d2))

o1 = pkcs.new(r1)
o2 = pkcs.new(r2)

# also
# nt.long_to_bytes(pow(nt.bytes_to_long(c1), d1, m1))

candidate_flag = o1.decrypt(c1, None) + o2.decrypt(c2, None)
if FLAG1 in candidate_flag:
    print("SUCCESS! Flag found - %r" % (candidate_flag, ))
    exit(0)
else:
    print("ERROR Flag not found - %r" % (candidate_flag, ))
    exit(-1)
