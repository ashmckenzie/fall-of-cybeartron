# Make RSA Great Again

This is a standalone challenge where players need to try to decrypt (some) RSA ciphertexts given only RSA modulii, RSA public exponents and ciphertexts

## Hints
1. Hint
<details>
Maybe you could check out the RSA page on wikipedia?
</details>

## Steps - Official Walkthrough

<details>
<summary>Spoiler warning</summary>

The vulnerability in this challenge is that two of the RSA modulii (say N1 and N2) happen to share a prime.

In this case `GCD(N1,N2)` will reveal the shared prime. With this, you can recover the respective `q1` and `q2` primes and recreate the RSA private exponent to decrypt the cipher text.

You'll be able to decrypt both ciphertexts, but only one will have the flag in it!


## Reference
1. https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Importance_of_strong_random_number_generation


</details>

