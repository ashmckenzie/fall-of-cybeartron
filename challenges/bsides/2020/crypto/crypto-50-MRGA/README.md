# Make RSA Great Again!

* _author_: Cybears:cipher
* _title_: Make RSA Great Again!
* _points_: 50
* _tags_:  crypto, beginner

The description/flavour text that will be shown to players is in the
[MANIFEST.yml](MANIFEST.yml) file. This README is a developer/post-event
focused description of the challenge.

## Quick start

The dockerfile has a build environment for the challenge. Alternatively you can
 * [BUILD] make all  #creates output.json file
 * [TEST]  make test
 * [CLEAN] make clean  #remove artifacts


##  Attachments
* `handout/output.json`: RSA modulii, public exponents and ciphertext


