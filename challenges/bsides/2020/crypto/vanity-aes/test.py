#!/usr/bin/env python3
import os.path

import hexdump
import pytest
import pwn

LOCAL_RADIO = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), "radio.py"
)
LOCAL_RADIO_ARGS = (LOCAL_RADIO, )

def test_serial_iv_use():
    """
    Test that with serialised encryption we don't repeat IVs.
    """
    p = pwn.process(LOCAL_RADIO_ARGS)
    ivs = list()
    for _ in range(3):
        p.readuntil("menu >>> ")
        p.send("2\n")
        p.readuntil("beacon data >>> ")
        p.send("0"*32 + "\n")
        p.readuntil("Transmitting\n")
        (iv_line, ) = p.readlines(1)
        ivs.append(hexdump.restore(iv_line.decode("iso-8859-1")))
        p.send("escape")
    assert len(set(ivs)) == len(ivs), "Unexpected IV repeat without parallelism"
    p.kill()

def test_parallel_iv_use():
    """
    Test that with three radio processes in parallel we get repeated IVs.
    """
    procs = tuple(
        pwn.process(LOCAL_RADIO_ARGS) for _ in range(3)
    )
    for p in procs:
        p.send("2\n")
        p.readuntil("beacon data >>> ")
    for p in procs:
        p.send("0"*32 + "\n")
    ivs = list()
    for p in procs:
        p.readuntil("Transmitting\n")
        (iv_line, ) = p.readlines(1)
        ivs.append(hexdump.restore(iv_line.decode("iso-8859-1")))
        p.kill()
    # We may have gotten unlucky and gotten two distinct IVs but we shoudn't
    # get three without the parallel approach being broken or crazy unfairness
    assert len(set(ivs)) < len(ivs), "Unexpected distinct IVs with parallelism"

if __name__ == "__main__":
    pytest.main([__file__, ])
