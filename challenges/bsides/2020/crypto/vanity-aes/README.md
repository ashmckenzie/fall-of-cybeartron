# Vanity AES

 * _author_: @hypersphere
 * _title_: Vanity AES
 * _points_: 250
 * _tags_: crypto

This challenge is a simple Python script which can be hosted behind a forking
`socat`. It is fairly important that there be a shared concept of time in the
deployed challenge system, and parameters in the challenge source can be
changed to account for clock-drift between nodes running containers.

## Hints

 * Time is of the essence!
 * Try not to get blocked up
 * You can communicate with the radio system in parallel to avoid delays
