# Optimal Prime Walkthrough

This core vulnerabilty in this challenge is the fact that the primes are
generated with some known bits. In this case, there is a problem in prime
generation where it takes a fixed buffer, and then xors some randomness in to
generate a prime. With knowledge of the RSA modulus and the high order bits
(found from reverse engineering the application), players can then factor using
Coppersmith's famous lattice attack

Players need to recover the unknown modulus. From RE you can recover the
plaintext messages (M1,M2,M3) being encrypted to cipher (C1,C2,C3) and the
public exponent `e`. Given for each message we have `C = M^e mod N`, we have
`C-M^e = kN` for some `N`.  From this we have that `GCD(C1 - M1^e, C2-M2^e)`
will be a mulitple of `N` (possibly `N`). If it is not `N`, we can use another
pair to recover `N` completely.
