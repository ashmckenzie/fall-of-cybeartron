// title: Ordinary Course Of Business (aka business) 
// author: cybears.cipher
// comment: sorry to whoever reads this. I wrote this just before BSides in a big rush. It's a bit of a mess - it's meant to be rev/crypto, but I suspect there may be some double-frees around if you try hard enough, so it might be pwn/crypt! or just pwn...  
// gcc ocb.c rijndael-alg-fst.c ordinaryCourseOfBusiness.c -o out && ./out

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "./ocb.h"
#include "rijndael-alg-fst.h"

#define FALSE 0
#define TRUE  1

#define ERR_SUCCESS 	0
#define ERR_FILE_OPEN 	-1
#define ERR_CONVERT	-1
#define ERR_FILE_READ 	-2
#define ERR_NONCE_REPEAT -3
#define MEMCMP_MATCH 	0
#define OCB_VALID_TAG 	1
#define OCB_INVALID_TAG 0
#define ERR_FLAG_DECRYPT -4
#define ERR_LEN		 -5
#define LEN_FLAG	48
#define LEN_NONCE_HEX   32
#define LEN_TAG_HEX	32
#define LEN_PASSWORD	22
#define PASSWORD_CORRECT 31
#define PASSWORD_INCORRECT -31

#define maxsize 128

uint32_t is_admin = FALSE;
//uint32_t is_admin = TRUE;
char *c=NULL, *password=NULL, s;
char *input_plaintext=NULL, *input_ciphertext=NULL, *input_tag=NULL, *plaintext=NULL, *ciphertext=NULL, *tag=NULL;
char *input_header=NULL, *header=NULL;
char *input_nonce=NULL, *nonce=NULL;
char *output_plaintext=NULL, *output_ciphertext=NULL;
uint32_t input_len  = 0;
uint32_t cipher_len = 0;
uint32_t header_len = 0;

/*
int check_password(char *password)
{
	//TODO: change this - xor encode or xor+1 encode? 
	const char * admin_password = "GreatSageEqualOfHeaven";
	if (LEN_PASSWORD == strlen(password) )
	{
		if (MEMCMP_MATCH == memcmp(password, admin_password, LEN_PASSWORD))
		{
			return PASSWORD_CORRECT;
		}
	}
	return PASSWORD_INCORRECT;
}
*/

int check_password2(char *password)
{
	byte x = 0xAA;
	int i=0;

	//admin_password encoded to remove strings on binary
	//const char * admin_password = "GreatSageEqualOfHeaven";
	byte enc_admin_password[] = { 0xed,0xd9,0xc9,0xcc,0xda,0xfc,0xd1,0xd6,0xd7,0xf6,0xc5,0xc0,0xd7,0xdb,0xf7,0xdf,0xf2,0xde,0xdd,0xcb,0xdb,0xd1 };
		
	byte enc_input_password[LEN_PASSWORD] = {0};

	for(i=0;i<LEN_PASSWORD; i++)
	{
		enc_input_password[i] = password[i]^x++;
	}

	if (LEN_PASSWORD == strlen(password) )
	{
		if (MEMCMP_MATCH == memcmp(enc_input_password, enc_admin_password, LEN_PASSWORD))
		{
			return PASSWORD_CORRECT;
		}
	}
	return PASSWORD_INCORRECT;
}

unsigned int is_hex_string(unsigned char * s)
{
    return s[strspn((const char *)s, "0123456789abcdefABCDEF")] == 0;
}

unsigned int is_odd_length(unsigned char *s)
{
    return (strlen((const char *)s) & 1) == 0;
}

int convert(const char *hex_str, unsigned char *byte_array, int byte_array_max)
{
    int hex_str_len = strlen(hex_str);
    int i = 0, j = 0;

    // The output array size is half the hex_str length (rounded up)
    int byte_array_size = (hex_str_len+1)/2;

    if (byte_array_size > byte_array_max)
    {
        // Too big for the output array
        return -1;
    }

    if (hex_str_len % 2 == 1)
    {
        // hex_str is an odd length, so assume an implicit "0" prefix
        if (sscanf(&(hex_str[0]), "%1hhx", &(byte_array[0])) != 1)
        {
            return -1;
        }

        i = j = 1;
    }

    for (; i < hex_str_len; i+=2, j++)
    {
        if (sscanf(&(hex_str[i]), "%2hhx", &(byte_array[j])) != 1)
        {
            return -1;
        }
    }

    return byte_array_size;
}


void print_menu(uint32_t is_admin)
{
	printf("--== Welcome to Encrypt-a-tron 3000 ==--\n");
	printf("What would you like to do today?\n");
        printf("1) encrypt some data\n");

	if( TRUE == is_admin )
	{
		printf("2) decrypt some data\n");
		printf("3) print test vectors\n");
	}
        printf("q) quit\n");

        return;
}


void RandomBytes(uint8_t *buf, uint64_t n) 
{
 	uint8_t byte;
 	FILE *fd = fopen("/dev/urandom", "r");

 	uint8_t k;
 	for(k=0;k<n;k=k+1) 
	{
  		fread(&byte, sizeof(byte), 1, fd);
  		buf[k] = byte;
 	}
 
	fclose(fd);
}


// IN : K  - key
// IN : C  - cipher
// IN : T  - tag
// IN : N  - nonce
// IN : A  - Additional Data
// IN : An - Additional Data Len
// OUT : M  - Message (Will be n-long)
// OUT : n  - Cipher Len
int Decrypt(byte *K, byte *C, byte *T, byte *N, byte *A, uint32_t An, byte *M, uint32_t n) 
{
 	//printf("Decryption Oracle\n");
 
 	ocb_state* state = ocb_init((byte *)K,16,16,AES128);
 	ocb_provide_header(state,A,An); 
 	int res = ocb_decrypt(state,N,C,n,T,M);
 	ocb_zeroize(state);

	return res;
}

//IN : K  - Key
//OUT: C  - cipher
//OUT: T  - tag
//IN : N  - nonce
//IN : A  - Additional Data
//IN : An - Additional Data Len
//IN : M  - Message
//IN : n  - Message Len
void Encrypt(byte *K, byte *C, byte *T, byte *N, byte *A, uint32_t An, byte *M, uint32_t n) 
{ 
 	//printf("Encryption Oracle\n");
 	ocb_state *state = ocb_init((byte *)K,16,16,AES128); 
 	ocb_provide_header(state,A,An);
 	ocb_encrypt(state,N,M,n,C,T);
}

//Structures to manage tracking nonce usage.
//In particular, use a singly-linked list of nonces to ensure no repeats of nonces
typedef struct nonce_node_t {
	struct nonce_node_t 	*next;
	block 		nonce;
} nonce_node;

nonce_node *nonce_head = NULL; //global pointer to nonce linked list

void push_nonce( nonce_node ** head, block nonce)
{
	nonce_node * new = (nonce_node *)malloc(sizeof(nonce_node));
	memcpy(new->nonce, nonce, BLOCKLEN);
	new->next = (*head);
	(*head) = new;
}

void free_nonce_list( nonce_node *head)
{
	nonce_node *tmp = NULL;
	while (NULL != head)
	{
		tmp = head;
		head = head->next;
		free(tmp);
	}
}

//we need to check that 
//1. (enc) nonce doesn't equal flag_nonce
//2. (dec) nonce, cipher and tag doesn't equal flag_nonce, flag_cipher and flag_tag
//3. (enc) nonce is never repeated
int check_nonce(nonce_node *head, byte *nonce_to_check)
{
	while(NULL !=head)
	{
		//pbuf(head->nonce, BLOCKLEN, "DEBUG check_nonce");
		if (MEMCMP_MATCH == memcmp(head->nonce, nonce_to_check, BLOCKLEN))
		{
			printf("Error, repeated nonce found\n"); fflush(stdout);
			return ERR_NONCE_REPEAT;
		}
		head = head->next;
	}
	return 0;
}

int main()
{
	byte AES_KEY[16] = {0};

#ifdef DEBUG
	memcpy(AES_KEY,(byte *)"SIXTEENBYTESHERE", 16);
	pbuf(AES_KEY, 16, "KEY:");
#else
	RandomBytes(AES_KEY, 16);
#endif

#ifdef ADDAD
	byte *flag_header = "cybearsctf2021";
	int flag_header_len = 10;
#else
	byte *flag_header = "";
	int flag_header_len = 0;
#endif

	int temp_len = 0;
	int res = 0, res2=0, ret_value = ERR_SUCCESS;

	//Get flag and encrypt it
	FILE *fp = fopen("./flag.txt", "rb");
	char *flag = NULL, *flag_nonce = NULL, *flag_tag = NULL, *flag_cipher=NULL;
	flag = malloc(LEN_FLAG);

	if (NULL != fp)
	{
		res = fread(flag, 1, LEN_FLAG, fp);
		if (LEN_FLAG == res)
		{
			//Encrypt flag
			flag_nonce = malloc(16);
			flag_tag = malloc(16);
			flag_cipher = malloc(LEN_FLAG);
#ifdef DEBUG
			memcpy(flag_nonce, (byte *)"EEEEEEEEEEEEEEEE", 16);
#else
			RandomBytes(flag_nonce, 16);
#endif
			Encrypt(AES_KEY, flag_cipher, flag_tag, flag_nonce, flag_header, flag_header_len, flag, LEN_FLAG);

		}	
		else
		{
			printf("Error reading file. Exiting\n");
			ret_value =  ERR_FILE_READ;
			goto final;
		}
	}
	else
	{
		printf("Error opening file. Exiting\n");
		ret_value = ERR_FILE_OPEN;
		goto final;
	}
	
	fclose(fp);

        while(TRUE)
        {
                print_menu(is_admin);
		fflush(stdout);
                scanf("%ms", &c);
		s = c[0];

		switch(s)
		{
			//secret menu to enable admin mode
			case '0':
				printf("Enter the password: "); fflush(stdout);
				scanf("%ms", &password);
				
				if(LEN_PASSWORD == strlen(password))
				{
					if (PASSWORD_CORRECT == check_password2(password))
					{
						printf("Correct password! Welcome admin\n"); fflush(stdout);
						is_admin = TRUE;
					}	
				}
				else
				{
					printf("Incorrect password\n"); fflush(stdout);
				}	
				
				free(password); password = NULL;

				break;
			case '1': //encrypt				
				
				//Get plaintext message and convert to bytes
				printf("Enter a string to encrypt in hex: \n"); fflush(stdout);
				scanf("%ms", &input_plaintext);
				if ( 	!is_hex_string(input_plaintext) ||
					!is_odd_length(input_plaintext) )
				{
					printf("Error in input string, expecting hex\n");
					free(input_plaintext);
					ret_value = ERR_CONVERT;
					goto final;
				}
				input_len = strlen(input_plaintext);
				//printf("received (%d): [%s]\n", input_len, input_plaintext);	
				
				plaintext = malloc(input_len+1);
				memset(plaintext, 0, input_len+1);
				res = convert(input_plaintext, plaintext, input_len+1); 
				if (ERR_CONVERT == res)
				{
					printf("ERROR in hex conversion of plaintext\n");
					ret_value = ERR_CONVERT;
					goto final;
				}
				//printf("unhex [%s]\n", plaintext);
				
				//Get additional data and convert to bytes
				printf("Enter additional data in hex: \n"); fflush(stdout);
				scanf("%ms", &input_header);
				
				if ('-' == input_header[0]) //allow null additional data
				{
					header_len = 0;
				}
				else
				{
					if ( 	!is_hex_string(input_header) ||
						!is_odd_length(input_header) )
					{
						printf("Error in input string, expecting hex\n");
						free(input_plaintext);
						free(input_header);
						ret_value = ERR_CONVERT;
						goto final;
					}
			
					header_len = strlen(input_header)/2;
				
					//printf("received (%d): [%s]\n", header_len, input_header);	
				
					header = malloc(header_len+1);
					memset(header, 0, header_len+1);
					res = convert(input_header, header, header_len+1); 
					if (ERR_CONVERT == res)
					{
						printf("ERROR in hex conversion of header\n");
						ret_value = ERR_CONVERT;
						goto final;
					}
					
					//printf("unhex header [%s] [%d]\n", header, header_len);
				}	

				//Get nonce and convert to bytes
				printf("Enter the nonce for this plaintext in hex: \n"); fflush(stdout);
				scanf("%ms", &input_nonce);
				if ( 	!is_hex_string(input_nonce) ||
					!is_odd_length(input_nonce) )
				{
					printf("Error in input string, expecting hex\n");
					free(input_nonce);
					free(input_plaintext);
					free(input_header);
					ret_value = ERR_CONVERT;
					goto final;
				}
		
				if( LEN_NONCE_HEX != strlen(input_nonce) )
				{
					printf("ERROR nonce should be 16-bytes (32 hex) [%ld]\n", strlen(input_nonce));
					ret_value = ERR_LEN;
					goto final;
				}

				nonce = malloc(16);
				res = convert(input_nonce,nonce,16);
				if (ERR_CONVERT == res)
				{
					printf("ERROR in hex conversion of nonce\n");
					ret_value = ERR_CONVERT;
					goto final;
				}
							
				//nonce = malloc(16);
				//RandomBytes(nonce, 16);	
				
				ciphertext = malloc(input_len+1);
				memset(ciphertext, 0, input_len+1);
				tag = malloc(16);
			
				//pbuf(plaintext, input_len/2, "DEBUG plain");
				//pbuf(nonce, 16, "DEBUG nonce");
				//pbuf(header, header_len, "DEBUG header");
			
				//As per https://eprint.iacr.org/2019/311.pdf s6.2, the decryption oracle 
				//1. decryption oracle shouldn't accept flag, flag_nonce, flag_tag to decrypt...
				//2. an encryption query should never accept flag_nonce 
				//3. encryption oracle should also disallow repeated nonces... 

				//check repeated nonce (3) 
				res = check_nonce(nonce_head, nonce);
				//push nonce
				push_nonce(&nonce_head, nonce);
				
				//check nonce != flag_nonce (2) 
				res2 = memcmp(nonce, flag_nonce, 16);
				if (MEMCMP_MATCH == res2 || ERR_NONCE_REPEAT == res )
				{
					printf("ERROR: can't use flag_nonce or repeated nonce\n"); fflush(stdout);
				}
				else
				{
					Encrypt(AES_KEY, ciphertext, tag, nonce, header, header_len, plaintext, input_len/2);

					pbuf(ciphertext, input_len/2, "Cipher");
					pbuf(nonce, 16, "Nonce");
					pbuf(tag, 16, "Tag");
				}

				free(input_plaintext); input_plaintext = NULL;
				free(plaintext); plaintext = NULL;				
				free(ciphertext); ciphertext = NULL;
				free(input_nonce); input_nonce = NULL;
				free(nonce); nonce = NULL;
				free(tag); tag = NULL;
				if (0 != header_len)
				{
					free(header); header = NULL;
				}

				break;
			case '2': //decrypt
				if(TRUE == is_admin)
				{
					//Input ciphertext
					printf("Enter a string to decrypt in hex: \n"); fflush(stdout);
					scanf("%ms", &input_ciphertext);
					if ( 	!is_hex_string(input_ciphertext) ||
						!is_odd_length(input_ciphertext) )
					{
						printf("Error in input string, expecting hex\n");
						free(input_ciphertext);
						ret_value = ERR_CONVERT;
						goto final;
					}
					//Input additional data
					printf("Enter additional data in hex: \n"); fflush(stdout);
					scanf("%ms", &input_header);

					//printf("received (%ld): [%s]\n", strlen(input_header), input_header);

					if ('-' == input_header[0] ) //allow null additional data
					{
						header_len = 0;
					}
					else
					{
						header_len = strlen(input_header)/2;
						header = malloc(strlen(input_header));
						res = convert(input_header, header, strlen(input_header));
						//pbuf(header, strlen(input_header)/2, "header");	
					

						if ( 	!is_hex_string(input_header) ||
							!is_odd_length(input_header) )
						{
							printf("Error in input string, expecting hex\n");
							free(input_header);
							free(input_ciphertext);
							ret_value = ERR_CONVERT;
							goto final;
						}
					}

					//Input nonce
					printf("Enter the nonce for this ciphertext in hex: \n"); fflush(stdout);
					scanf("%ms", &input_nonce);
					if ( 	!is_hex_string(input_nonce) ||
						!is_odd_length(input_nonce) )
					{
						printf("Error in input string, expecting hex\n");
						free(input_nonce);
						free(input_header);
						free(input_ciphertext);
						ret_value = ERR_CONVERT;
						goto final;
					}
					
					//Input tag
					printf("Enter the tag for this ciphertext in hex: \n"); fflush(stdout);
					scanf("%ms", &input_tag);
					if ( 	!is_hex_string(input_tag) ||
						!is_odd_length(input_tag) )
					{
						printf("Error in input string, expecting hex\n");
						free(input_tag);
						free(input_nonce);
						free(input_header);
						free(input_ciphertext);
						ret_value = ERR_CONVERT;
						goto final;
					}
		
					temp_len = strlen(input_ciphertext)/2;
					//printf("received (%ld): [%s]\n", strlen(input_ciphertext), input_ciphertext);

					ciphertext = malloc(strlen(input_ciphertext));
					res = convert(input_ciphertext, ciphertext, strlen(input_ciphertext));
					//pbuf(ciphertext, strlen(input_ciphertext)/2, "Cipher");	
					

					//printf("converting nonce\n");
					nonce = malloc(16);
					res = convert(input_nonce,nonce,16);
					//pbuf(nonce, 16, "Nonce");	
					//printf("converting tag\n");

					tag = malloc(16);
					res = convert(input_tag, tag, 16);
					//pbuf(tag, 16, "Tag");	

					//As per https://eprint.iacr.org/2019/311.pdf s6.2, the decryption oracle 
					//1. decryption oracle shouldn't accept flag, flag_nonce, flag_tag to decrypt...
					//2. an encryption query should never accept flag_nonce 
					//3. encryption oracle should also disallow repeated nonces... 
					
					//check flag, flag_nonce, flag_tag (1)
					if (LEN_FLAG == temp_len)
					{
						if (	MEMCMP_MATCH == memcmp(nonce, flag_nonce, 16) &&
							MEMCMP_MATCH == memcmp(tag, flag_tag, 16) &&
							MEMCMP_MATCH == memcmp(ciphertext, flag_cipher, LEN_FLAG) )
						{
							res2 = ERR_FLAG_DECRYPT;
						}
						else
						{
							res2 = ERR_SUCCESS;
						}
					}
					else
					{
						res2 = ERR_SUCCESS;
					}

					if (ERR_NONCE_REPEAT == res || ERR_FLAG_DECRYPT == res2)
					{
						printf("Nice try, but decryption oracle won't accept that\n"); fflush(stdout);
					}
					else
					{
						//pbuf(ciphertext, temp_len, "DEBUG cipher");
						//pbuf(tag, 16, "DEBUG tag");
						//pbuf(nonce, 16, "DEBUG nonce");
						//pbuf(header, header_len, "DEBUG header");

						plaintext = malloc(temp_len);
						res = Decrypt(AES_KEY, ciphertext, tag, nonce, header, header_len, plaintext, temp_len);

						if (OCB_VALID_TAG == res)
						{
							printf("Successful tag validation!\n");
							pbuf(plaintext,temp_len, "Message"); fflush(stdout);
						}
						else
						{
							printf("Invalid tag\n");
						}
						free(plaintext);

					}

					free(input_nonce);	
					free(input_ciphertext);
					free(input_tag);
					free(ciphertext);
					free(nonce);
					free(tag);
					if (0 != header_len)
					{
						free(header);
					}
					
				}
				break;
			case '3': //print test vector
				if(TRUE == is_admin)
				{
					printf("--=== TEST VECTOR ===--\n"); 
					pbuf(flag_cipher, LEN_FLAG, "Cipher");
					pbuf(flag_nonce, 16, "Nonce");
					pbuf(flag_tag, 16, "Tag");
					fflush(stdout);
				}
				break;
			case 'q': //exit
				ret_value =  ERR_SUCCESS;
				goto final;
			default:
				//printf("chose other\n");
				break;
		free(c); c = NULL;
		}
        }

final:
	free(c);
	free(flag);
	free(flag_nonce);
	free(flag_cipher);
	free(flag_tag);
	free_nonce_list(nonce_head);
	return ret_value;
}

