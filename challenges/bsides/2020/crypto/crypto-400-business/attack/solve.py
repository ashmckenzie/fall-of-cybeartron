import os
import pathlib
from binascii import *

from pwn import *
from Crypto.Util.strxor import strxor as xor

import argparse

SCRIPT_DIR = pathlib.Path(__file__).parent

from ctypes import *
h = CDLL(str(SCRIPT_DIR / "ocb_attack_shared.so"))

def EncryptionOracle(s, nonce, message, additional_data=None):
    s.sendline("1")
    s.readuntil("hex:")
    s.sendline(hexlify(message))
    s.readuntil("hex:")
    if(additional_data == None):
        s.sendline("-")
    else:
        s.sendline(hexlify(additional_data))
    s.readuntil("hex:")
    s.sendline(hexlify(nonce))
    _ = s.readline()
    c = s.readline()
    n = s.readline()
    t = s.readline()

    #print(c,n,t)

    cipher = unhexlify(c.split(b":")[1].strip())
    tag = unhexlify(t.split(b":")[1].strip())
    s.readuntil("quit")

    return cipher, tag

def DecryptionOracle(s, cipher, nonce, tag, additional_data=None):
    s.sendline("2")
    s.readuntil("hex:")
    print("DEBUG sending [{}]".format(hexlify(cipher)))
    s.sendline(hexlify(cipher))
    s.readuntil("hex:")
    if (additional_data==None):
        s.sendline("-")
    else:
        s.sendline(hexlify(additional_data))
    s.readuntil("hex:")
    print("DEBUG sending [{}]".format(hexlify(nonce)))
    s.sendline(hexlify(nonce))
    s.readuntil("hex:")
    print("DEBUG sending [{}]".format(hexlify(tag)))
    s.sendline(hexlify(tag))
    _ = s.readline()
    v = s.readline()
    if b'Success' in v:
        m = s.readline()
        print("DEBUG - success - got m: {}".format(m))
        valid = True
        message = unhexlify(m.split(b":")[1].strip())
    else:
        print("DEBUG - Error tag failed: {}".format(v))
        valid = False
        message = None
    s.readuntil("quit")

    return valid, message

def InoueMinematsuMinimalAttack(s):
    #TODO

    N = create_string_buffer(os.urandom(16))
    #debug
    #N = create_string_buffer(b'D'*16)

    length_block = create_string_buffer(b'\x00'*16)
    length_block[15] = 16*8 #128
    M = create_string_buffer(b'\x00'*32)
    M[0:16] = length_block[0:16]
    C,T = EncryptionOracle(s, N.raw[0:16], M.raw[0:32])

    print("DEBUG: M = {}, N = {}, C = {}, T = {}".format(hexlify(M.raw), hexlify(N.raw), hexlify(C), hexlify(T)))

    cipher_prime = xor(C[0:16], length_block.raw[0:16])
    print("DEBUG: len M - {}, len C = {}".format(len(M.raw[16:32]), len(C[16:32])))
    tag_prime = xor(M.raw[16:32], C[16:32])

    print("DEBUG: Cp = {}, Tp = {}".format(hexlify(cipher_prime), hexlify(tag_prime)))

    res, message_prime = DecryptionOracle(s, cipher_prime, N.raw[0:16], tag_prime)

    return res, message_prime, C, N, tag_prime

def IwataPlaintextRecoveryAdversary(s, cipher, nonce, tag, AD=None):
    #TODO
    #message = '\x00'*len(cipher)
    #valid = True

    #Mp = create_string_buffer(b'\x00'*16)
    #Cp = create_string_buffer(b'\x00'*32)

    res, Mp, Cp, N, Tp = InoueMinematsuMinimalAttack(s)

    print(res, hexlify(Mp), hexlify(Cp), hexlify(N), hexlify(Tp))

    print("Recover L")
    length_block = create_string_buffer(b'\x00'*16)
    length_block[15] = 16*8 #128
    Mp = xor(Mp, length_block.raw[0:16])

    c_Mp = create_string_buffer(Mp)
    L = create_string_buffer(b'\x00'*16)
    h.GF2128DivideByTwo(L, c_Mp)

    print("L:", hexlify(L.raw))

    # 5. For i in {1.. m-1} define...
    Cp1 = Cp[0:16]
    Cp2 = Cp[16:32]

    u2L = create_string_buffer(b'\x00'*16)
    u4L = create_string_buffer(b'\x00'*16)

    h.two_times(u2L, L)
    h.two_times(u4L, u2L)

    print("u2L:", hexlify(u2L.raw))
    print("u4L:", hexlify(u4L.raw))

    X1 = xor(u2L.raw[0:16], length_block.raw[0:16])
    Y1 = xor(u2L.raw[0:16], Cp1)

    Xm = xor(length_block.raw[0:16], u4L.raw[0:16])
    Ym = Cp2

    print("X1:", hexlify(X1))
    print("Y1:", hexlify(Y1))

    Npp = Xm
    Lpp = create_string_buffer(Ym)

    print("Ym", hexlify(Ym))
    print("N''", hexlify(Npp))
    print("L''", hexlify(Lpp.raw))

    u2Lpp = create_string_buffer(b'\x00'*16)
    h.two_times(u2Lpp, Lpp)
    Mpp1 = xor(nonce, u2Lpp.raw[0:16])
    Mpp = Mpp1 + b'\x00'*16

    print("u2Lpp", hexlify(u2Lpp.raw))
    print("M''", hexlify(Mpp))
    Cpp, Tpp = EncryptionOracle(s, Npp, Mpp, additional_data=AD)

    print("C''", hexlify(Cpp), "T''", hexlify(Tpp))

    Cpp1 = Cpp[0:16]
    Ls = xor(Cpp1, u2Lpp[0:16])

    print("L*", hexlify(Ls))

    Cs1 = cipher[0:16]
    Cs2 = cipher[16:32]
    Csm = cipher[32:48]
    print("Cs1", hexlify(Cs1))
    print("Cs2", hexlify(Cs2))
    print("Csm", hexlify(Csm))

    u2Ls = create_string_buffer(b'\x00'*16)
    u4Ls = create_string_buffer(b'\x00'*16)

    h.two_times(u2Ls, Ls)
    h.two_times(u4Ls, u2Ls)

    tmp = xor(u4Ls.raw[0:16], u2Ls.raw[0:16])
    C_S_j = xor(Cs2, tmp)
    C_S_k = xor(Cs1, tmp)

    C_S = C_S_j + C_S_k + Csm
    print("Csj", hexlify(C_S_j))
    print("Csk", hexlify(C_S_k))
    print("C_S", hexlify(C_S))

    print("DEBUG: calling decryption oracle in Iwata")
    valid, message = DecryptionOracle(s, C_S, nonce, tag, additional_data=AD)

    M_S_k = message[16:32]
    M_S_j = message[0:16]
    M_S_m = message[32:48]

    Msj = xor(M_S_k, tmp)
    Msk = xor(M_S_j, tmp)

    recovered_message = Msj + Msk + M_S_m

    return valid, recovered_message

def getTestVector(s):
        s.sendline("3")
        r = s.readuntil("quit")
        rr = r.split(b'\n')
        if b'Cipher' in rr[2]:
            cipher = unhexlify(rr[2].split(b":")[1].strip())
        else:
            cipher = None
        if b'Nonce' in rr[3]:
            nonce = unhexlify(rr[3].split(b":")[1].strip())
        else:
            nonce = None
        if b'Tag' in rr[4]:
            tag = unhexlify(rr[4].split(b":")[1].strip())
        else:
            tag = None

        return cipher, nonce, tag


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote', help="The address:port of the remote server hosting the challenge", required=False)
    args = parser.parse_args()

    if args.remote != None:
        host = args.remote.split(":")[0]
        port = int(args.remote.split(":")[1])
        p = remote(host, port)
    else:
        p = process("./business")

    admin_password = b'GreatSageEqualOfHeaven'

    p.readuntil("quit")

    p.sendline("0")
    p.sendline(admin_password)
    p.readuntil("quit")


    #nonce = os.urandom(16)
    nonce = b'\x45'*16
    message = b'hello'

    print("Calling encryption oracle")
    cipher,tag = EncryptionOracle(p, nonce, message)

    print(hexlify(cipher), hexlify(nonce), hexlify(tag))

    print("Calling decryption oracle")
    valid, message = DecryptionOracle(p, cipher, nonce, tag)

    print(valid, message)


    res, message_prime, cipher_prime, N, tag_prime = InoueMinematsuMinimalAttack(p)

    print(res, message_prime, hexlify(cipher_prime), hexlify(N.raw), hexlify(tag_prime))


    cipher, nonce, tag = getTestVector(p)

    print("DEBUG Calling iwata: C:{}, N:{}, T:{}", hexlify(cipher), hexlify(nonce), hexlify(tag))
    valid,message = IwataPlaintextRecoveryAdversary(p, cipher, nonce, tag, AD=b'cybearsctf')

    print("DEBUG final recovery", valid, message)

    #p.interactive()
    p.close()
