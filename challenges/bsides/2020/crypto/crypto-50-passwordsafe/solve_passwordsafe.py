
import Crypto.Cipher.AES as aes
from binascii import *

PARTIAL_KEY = unhexlify('1af7ecfac3596c6425ce4f403c44')
cipher = unhexlify('3f24b052ee4ce98a849e954d5b0c9859344d870af49b233a78cd83860e339ee4228b1d76b054e9239bf3b3c29d0cef91')



for i in range(0,2**16):
    TAIL = int.to_bytes(i, 2, "big")
    AES_KEY = PARTIAL_KEY + TAIL
    a = aes.new(AES_KEY, aes.MODE_ECB)
    plain = a.decrypt(cipher)

    if plain.isascii():
        print("JACKPOT: ({}, {}, {})".format(i,hexlify(AES_KEY) ,plain))



