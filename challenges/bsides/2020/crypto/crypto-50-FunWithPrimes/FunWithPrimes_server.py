import Crypto.PublicKey.RSA as rsa
import Crypto.Cipher.PKCS1_v1_5 as pkcs
import Crypto.Util.number as nt
from binascii import *
import json
import random

from flag import FLAG



# 1. Given N, e, p, q, C -> M
# 2. Given N, phi(N) -> p
# 3. GIven N, p+q -> p
# 4. Given N, p-q -> p
# 5. Given N,e,d -> p

###############
## Challenge 1
###############

M1 = b'Well done! One down, only four more to go!'
# 1. Given N, e, p, q, C -> M
def generate_challenge_one(M):
    r = rsa.generate(1024)
    p = pkcs.new(r)
    c = p.encrypt(M)
    return r, {"N":r.n, "e":r.e, "p":r.p, "q":r.q, "cipher":hexlify(c).decode('utf-8')}

###############
## Challenge 2
###############

# 2. GIven N, phi(N) -> p
def generate_challenge_two():
    r = rsa.generate(1024)
    return r, {"N":r.n, "phi": (r.p-1)*(r.q-1)}


###############
## Challenge 3
###############

# 3. GIven N, p+q -> p
def generate_challenge_three():
    r = rsa.generate(1024)
    return r,{"N":r.n, "p+q":r.p+r.q}

###############
## Challenge 4
###############

# 4. GIven N, q-p -> p
def generate_challenge_four():
    r = rsa.generate(1024)
    return r,{"N":r.n, "q-p":r.q-r.p}

###############
## Challenge 5
###############

# 5. GIven N, e, d  -> p
def generate_challenge_five():
    r = rsa.generate(1024)
    return r,{"N":r.n, "e":r.e, "d": r.d}


def run():
    print("Welcome!")

    ## Challenge 1 - Challenge
    print("First question, given N,e,p,q and cipher, give me the message as a string")
    r,j = generate_challenge_one(M1)
    print(json.dumps(j))

    ## Challenge 1 - Response
    resp = input("Enter Response:").strip()

    if resp != M1.decode("utf-8"):
        print("ERROR 1: Incorrect response")
        exit(-1)

    ## Challenge 2 - Challenge
    print("Second question, given N=pq and phi(N) (The Euler-Phi function phi(N)=(p-1)(q-1)), give me the smaller prime, p, as a decimal integer")
    r,j = generate_challenge_two()
    print(json.dumps(j))

    ## Challenge 2 - Response
    resp = int(input("Enter Response:").strip())

    if resp != r.p:
        print("ERROR 2: Incorrect response")
        exit(-1)

    ## Challenge 3 - Challenge
    print("Third question, given N=pq and the sum of primes (p+q), give me the smaller prime, p, as a decimal integer")
    r,j = generate_challenge_three()
    print(json.dumps(j))

    ## Challenge 3 - Response
    resp = int(input("Enter Response:").strip())

    if resp != r.p:
        print("ERROR 3: Incorrect response")
        exit(-1)

    ## Challenge 4 - Challenge
    print("Fourth question, given N=pq and the difference of primes (q-p), give me the smaller prime, p, as a decimal integer")
    r,j = generate_challenge_four()
    print(json.dumps(j))

    ## Challenge 4 - Response
    resp = int(input("Enter Response:").strip())

    if resp != r.p:
        print("ERROR 4: Incorrect response")
        exit(-1)

    ## Challenge 5 - Challenge
    print("Final question, given N=pq, e (the RSA public exponent) and d (the RSA private exponent), give me the smaller prime, p, as a decimal integer")
    r,j = generate_challenge_five()
    print(json.dumps(j))

    ## Challenge 5 - Response
    resp = int(input("Enter Response:").strip())

    if resp != r.p:
        print("ERROR 5: Incorrect response")
        exit(-1)


    print("SUCCESS Here is your flag: {}".format(FLAG))

if __name__ == "__main__":
    try:
        run()
    except Exception:
        print("Argh! You've killed me, you jerk.")
