import sys

if len(sys.argv) != 2:
    print("usage: sage solve.sage [handout.sage]")
    exit(0)

expected_flag = "cybears{ROLYPOLYGMEZPZ}"

R.<X> = PolynomialRing(GF(3))

#load("handout_new.sage")
load(sys.argv[1])

p = factor(N)[0][0]
q = factor(N)[1][0]


F.<K> = FiniteField(3^255, modulus=p)
G.<J> = FiniteField(3^257, modulus=q)


message = []
for i in range(0,len(cipher)):
    if F(cipher[i]).is_square():
        message.append(str(0))
    else:
        message.append(str(1))

bits = "".join(message)
flag = ''.join(chr(int(bits[8*i:8*(i+1)], 2)) for i in range((len(bits)//8)))
#print(flag)

if flag == expected_flag:
    print("SUCCESS! Flag found")
    #exit(0)
    # sage interpreter changes this 0 into a sage integer. Python exit() treats anything thats not a python int as a value and returns 1!
    exit(int(0))
else:
    print("ERROR: Flag not found")
    print(flag)
    exit(int(-1))



