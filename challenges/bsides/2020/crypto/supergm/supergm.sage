#Define a polynomial ring over GF(3)
R.<X> = PolynomialRing(GF(3))
p = X^255 + 2*X^5 + X^4 + 2*X^3 + 2*X + 2 #secret
q = X^257 + 2*X^6 + X^5 + X^3 + 2*X^2 + 1 #secret

N = p*q

#Form the quotient ring GF(3)[x]/(N)
S = R.quotient(N, X)

SP = R.quotient(p,X)
SQ = R.quotient(q,X)


#F.<K> = FiniteField(3^255, modulus=p)
#G.<J> = FiniteField(3^257, modulus=q)


# A non-zero element f is a quadratic residue if and only if f^((3^255-1)/2) == 1 in one case, and the same but with 255->257 in the other
# (255 and 257 being the degrees of p and q)
# S.hom(SP) is the canonical homomorphism between S -> SP

#function to determine whether a polynomial is a square or not in GF(3)[x]/(N)
# It does this by mapping it down to the fields GF(3)/[x]/(p) and GF(3)[x]/(q)
def is_square_in_N(f):

    if (S.hom(SP)(f)^((3^255-1)//2) == 1 and S.hom(SQ)(f)^((3^257-1)//2) == 1):
        return True
    else:
        return False

# generate random x
x = S.random_element()
while (S.hom(SP)(x)^((3^255-1)//2) == 1 or S.hom(SQ)(x)^((3^257-1)//2) == 1):
    x = S.random_element()

# generate a random y for encryption. This needs to have GCD(y,N) == 1, one way to do that is to map to the quotient rings and check whether they're 0
def gen_random_y():
    y = S.random_element()
    while True:
        if S.hom(SP)(y) == 0 or S.hom(SQ)(y) == 0:
            y = S.random_element()
        else:
            return y

expected_flag = 'cybears{ROLYPOLYGMEZPZ}'

def chr_to_bin(c):
    b = bin(ord(c))[2:]
    while(len(b) < 8):
        b = '0' + b
    return b

def make_plain_array(flag):
    bits = ""
    for c in flag:
          bits += chr_to_bin(c)
    return map(int,list(bits))

plain = make_plain_array(expected_flag)

cipher = [ x^p * gen_random_y()^2 for p in plain ]

test_dec = [ not is_square_in_N(c) for c in cipher]
test_dec_int = map(int,test_dec)
test_dec_int == plain


bits = "".join(map(str,test_dec_int))
flag = ''.join(chr(int(bits[8*i:8*(i+1)], 2)) for i in range(len(bits)//8))
print(flag)

hint = '''
# Some words of wisdom for you all:

# "Pictures are worth a thousand words"
# "One swallow doesn't make a summer"
# "Loose lips sink ships"
# "You can lead a horse to water but you can't make it drink"
# "Genius is one percent inspiration and 99 percent perspiration"
# "Old soldiers never die, they simply fade away"
# "Lies, damned lies and statistics"
# "Do unto others as you would have them do to you"
# "When in Rome, do as the Romans do"
# "Attack is the best form of defence"
# "Speak softly and carry a big stick"
# "Sticks and stones may break my bones"
# "Enough is as good as a feast"
# "Revenge is a dish best served cold"
# "Men's evil manners live in brass; their virtues we write in water"
# "If wishes were horses, beggars would ride"
# "Cleanliness is next to godliness"
# "All work and no play makes Jack a dull boy"
# "Life begins at forty"
# "It's better to have loved and lost than never to have loved at all"

'''

with open("handout.sage", "w") as g:
    g.write(hint)
    g.write("prime= 3\n\n")
    g.write("x = " + str(x) + "\n\n")
    g.write("N = " + str(N) + "\n\n")
    g.write("cipher = " + str(cipher) + "\n\n")
