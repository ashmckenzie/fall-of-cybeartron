# supergm

* _author_: cybears:monalisaoverdrive
* _title_: supergm
* _points_: 300
* _tags_:  crypto

## Flags
* `cybears{ROLYPOLYEZPZ}`

## Challenge Text
```markdown
Here's a big file I don't even
```

## Attachments
* handout

## Hints
* That is some good advice

## Notes
* You will probably want a mathematical algebra package for this challenge (eg sage or magma)
