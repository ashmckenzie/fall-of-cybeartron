from PIL import Image
from pyzbar.pyzbar import decode
from Crypto.Protocol.SecretSharing import Shamir
from binascii import unhexlify
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--shares', help="Total number of shares", default=4, type=int)
parser.add_argument('-t', '--threshold', help="Threshold for shares (how many shares users need to reconstruct secret)", default=4, type=int)
args = parser.parse_args()

t = args.threshold
s = args.shares

flag_CHECK = b'cybears{C@r1ng!}'

shares = []

for i in range(1,s+1):
    data = decode(Image.open('out/print_'+str(i)+'_all.png'))
    x = data[0].data
    print("Found data: {}".format(x))
    share_num = int(x[1:-1].split(b",")[0].decode("utf-8"))
    share_value = unhexlify(x[1:-1].split(b",")[1].strip())
    shares.append((share_num, share_value))

flag = Shamir.combine(shares)

if flag == flag_CHECK:
    print("SUCCESS: Flag matches")
    exit(0)
else:
    print("ERROR: Flag didn't match - you got [{}]".format(flag))
    exit(-1)
