crypto-200-sssh
=================

* _author_: Cybears:cipher
* _title_: ssssh
* _points_: 200
* _tags_:  crypto

## Flags
* `cybears{C@r1ng!}`

## Challenge Text
```markdown
It's a secret! Don't share...
```

## Attachments
*

## Hints
* QR
* (4,4)-threshold, GF(2^128), 𝑥^128+𝑥^7+𝑥^2+𝑥+1

## References
* https://pycryptodome.readthedocs.io/en/latest/src/protocol/ss.html

## Notes - Repo
* We've included the specific shares used in the CTF in the repo as they need to be kept together as a set
* under the `out` directory there are a number of image files
 * `outN.png` are the raw QR code outputs with the embedded images for some `N`
 * `outN_372x372.png` are the upscaled QR codes
 * `print_N_all.png` are the printable posters to stick up during the event
 * `badge_share4.bmp` is a copy of the 4th share (also in the WuKongPrime QR code `print_4_all.png`) to be placed in the badge
 * `badge_url.bmp` is a plain QR code with the CTF url for the badge
