#define BAUD 38400

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/setbaud.h>
#include <avr/boot.h>
#include <util/delay_basic.h>

extern void app_start(void);

// =========== UART Functions ===========
void init_uart()
{
    UCSRC |= (3 << UCSZ0); // 8 bits

    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA |= (1 << U2X);
#else
    UCSRA &= ~(1 << U2X);
#endif
    UCSRB = (1 << RXEN) | (1 << TXEN);
    // DDR for the TX/RX pins are overridden by the USART so we don't need to set it.
}

// Send a character. Blocking.
void uart_putchar(char c)
{
    loop_until_bit_is_set(UCSRA, UDRE);
    UDR = c;
}

// Get a character. Blocking.
char uart_getchar()
{
    loop_until_bit_is_set(UCSRA, RXC);
    return UDR;
}


uint16_t calc_crc(uint8_t * buf, uint8_t len)
{
    union
    {
        uint32_t value;
        uint16_t words[2];
        uint8_t bytes[4];

    } result;

    // Fill 
    result.bytes[0] = buf[3];
    result.bytes[1] = buf[2];
    result.bytes[2] = buf[1];
    result.bytes[3] = buf[0];

    for(uint8_t b = 4; b < (len + 4); b++)
    {
        for (uint8_t i = 0; i < 8; i++)
        {
            _delay_loop_2(1024); // Force the WDT to reset

            uint8_t bit_set = (result.bytes[3] & 0x80);
            result.value <<= 1;

            if(bit_set)
            {
                result.words[1] ^= 0x2F15;
            }
        }

        if (b < len) result.bytes[0] = buf[b];
    }

    return result.words[1];
}

int main()
{
    // Disable all interrupts
    // Do not want anything from the application interrupting us.
    cli();
    init_uart();

    // 64 Pages
    // Pages are 16 words
    #define PAGE_WORDS (SPM_PAGESIZE / 2)

    uint16_t page = 0;

    union
    {
        // Include room in buffer for CRC16
        uint8_t bytes[SPM_PAGESIZE + 2];
        uint16_t words[PAGE_WORDS];
    } buffer;

    while(1)
    {
        switch(uart_getchar())
        {
            case 'p':
                // Set page
                page = uart_getchar() << 5;
                break;

            case 'r':
                // Read buffer
                for (uint8_t i = 0; i < sizeof(buffer); i += 1)
                {
                    uart_putchar(buffer.bytes[i]);
                }
                uart_putchar('\n');
                break;
            
            case 'w':
                // Write buffer
                for (uint8_t i = 0; i < sizeof(buffer); i += 1)
                {
                    buffer.bytes[i] = uart_getchar();
                }
                break;
            
            case 'f':
                if (calc_crc(buffer.bytes, sizeof(buffer)) != 0)
                {
                    uart_putchar('0');
                    break;
                }
                // Fill memory
                for (uint8_t i = 0; i < PAGE_WORDS; i++)
                {
                    // Fill the program buffer
                    // Buffer is filled a word at a time.
                    boot_page_fill_safe(page + (i << 1), buffer.words[i]);
                }
                // Erase and Write
                boot_page_erase_safe(page);
                boot_page_write_safe(page);
                uart_putchar('1');
                break;

            case 'a':
                // Run application
                asm("rjmp app_start");

            default:
                // Ignore invalid chars
                break;
        }
    }
}
