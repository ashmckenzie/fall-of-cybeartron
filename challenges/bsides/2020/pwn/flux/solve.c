#define F_CPU 8000000
#define BAUD 38400

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/setbaud.h>


// Send a character. Blocking.
inline void uart_putchar(char c)
{
        loop_until_bit_is_set(UCSRA, UDRE);
        UDR = c;
}

int main()
{
    // Init UART
    UCSRC |= (3 << UCSZ0); // 8 bits

    UBRRH = UBRRH_VALUE;
    UBRRL = UBRRL_VALUE;
#if USE_2X
    UCSRA |= (1 << U2X);
#else
    UCSRA &= ~(1 << U2X);
#endif
    UCSRB = (1 << RXEN) | (1 << TXEN);

    for(uint16_t i = 0; i <= FLASHEND; i++)
    {
        // Send each flash byte
        uart_putchar(pgm_read_byte(i));
    }
}