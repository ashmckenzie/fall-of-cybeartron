# Decrypt as a Service Walkthrough
This challenge is a simple stack based buffer overflow, with no stack canaries,
executable stack and ASLR on. The problem is that the stack based buffer
overflow is decrypted with an unknown key after the user has provided it.

The point of this challenge is to realise that:
  1. The service can be interacted with multiple times and
  2. that it is encrypting in CBC mode

With CBC mode, if you control the ciphertext, you can control the decrypt. If
we send a block of "A" to decrypt, it will sendback D(A) = DEC(A,K) (for some
key). If we then send blocks (where X,Y are blocks of user controlled data and
`+` is byte-wise XOR)

```
Send:
  C1 = D(A)+X, A, D(A) + Y, A
Receive:
  P1 = D(D(A) + X) + IV
  P2 = D(A) + C1 = D(A) + D(A) + X = X
  P3 = D(D(A)+Y) + A
  P4 =  D(A) + (D(A) + Y) = Y
```

In this way we can control every 16 blocks (every alternate block will be
decypted to garbage). If we can return execution to this stack, we can write
shell code that will jump over each of the "garbage" blocks.

Given there is ASLR and no (intentionally) incuded info leaks, we utilise a
"jmp esp" gadget that happens to be in the AES key used as a self-test for this
binary. The shellcode in the solver starts a shell.

## Notes - Shellcode
* Shellcode is based on pwntools `shellcraft.amd64.sh()` shellcode
* Can find the added nops/jmps in `sc_label.asm`
* Compile with `nasm -f elf64 ./sc_labels.asm` to create .o file
* Then extract bytes with

```sh
for i in `objdump -d ./sc_labels.o |grep -v "00000000000000" | cut -f 2 | tr '\t' ' ' | tr ' ' '\n' | egrep '^[0-9a-f]{2}$'` ; do echo -n "\x$i" ; done
```
