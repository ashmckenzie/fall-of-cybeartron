# Decrypt as a Service

* _author_: Cybears:cipher
* _title_: Decrypt as a Service
* _points_: 200
* _tags_:  pwn,crypto

## Flags
* `cybears{A_H0p_Sk1p_4nd_A_JmpEsp}`

## Challenge Text
```markdown
We've managed to access a decryption service provided by the DecepticomTSS
army. If only we knew the key...

nc daas.pwn.cybears.io 3141

```

## Attachments
* `daas`: binary application

## Hints
* 

## References
* `https://efail.de/`
* `https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation`

## Notes - Build
* `make all` : to build handouts 
* `make test` : to run solver and validate
* `make clean` : to remove artifacts
