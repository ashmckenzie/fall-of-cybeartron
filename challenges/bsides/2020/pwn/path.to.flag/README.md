# path.to.flag

The cybears have uncovered an ancient cybears datavault.
This ancient terminal does not have enough memory to open
paths longer than a few characters long... but the file
we need is deep in the directory structure.

It's time to break out the cyberdeck and hack our way in.

## Quick start

To build and test locally:
```bash
make test
```

To replicate the CI locally with docker:

```bash
# For this to work, you’ll need to login with docker to pull the images
docker login registry.gitlab.com
# Once you’re logged in you can run the docker containers
make docker-test
```
