#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>
#include <assert.h>
#include <sys/syscall.h>

void* traverse(void* in) {
    char* path = (char*)in;
    char* delim = ".\n";
    char* dir = strtok(path, delim);
    char final_path[PATH_MAX] = {0,};

#if SHOW_TID
#ifdef SYS_gettid
    int tid = syscall(SYS_gettid);
#else
#error "We don't have SYS_gettid"
#endif
#endif

    while (dir != NULL) {
#if SHOW_TID
        printf("%d: Navigating to '%s'\n", tid, dir);
#else
        printf("Navigating to '%s'\n", dir);
#endif
        fflush(stdout);

        if (strlen(final_path) + strlen(dir) + strlen("flag.txt") +  2 >= PATH_MAX) {
#if SHOW_TID
            printf("%d: Your path is too long!\n", tid);
#else
            printf("Your path is too long!\n");
#endif
            fflush(stdout);
            exit(1);
        }

        strcat(final_path, dir);
        strcat(final_path, "/");
        sleep(1);
        dir = strtok(NULL, delim);
    }
    strcat(final_path, "flag.txt");
#if SHOW_TID
    printf("%d: Final path: %s\n", tid, final_path);
#else
    printf("Final path: %s\n", final_path);
#endif
    fflush(stdout);

    int fd = open(final_path, O_RDONLY);
    char buff[256] = {0,};
    while (read(fd, buff, sizeof(buff)) > 0) {
#if SHOW_TID
        printf("%d: %s", tid, buff);
#else
        printf("%s", buff);
#endif
        fflush(stdout);
    }
    return NULL;
}

int main() {
    printf("CYBEAR FLAG FILE EXPLORER\n");
    printf("Cybear paths are entered using a '.' as a separator\n");
    printf("The flag exists at the.flag.is.right.here.buddy\n");
    
    while (true) {
        char* path = NULL;
        size_t path_size = 0;
        printf("> ");
        fflush(stdout);
        if (-1 == getline(&path, &path_size, stdin)) {
            break;
        }
        if (path != NULL || path_size >= PATH_MAX) {
            if (strlen(path) > 15) {
                printf("SORRY, PATH IS TOO LONG!\n");
                break;
            }
            pthread_t thread;
            pthread_create(&thread, NULL, &traverse, path);
            pthread_detach(thread);
        } else {
            break;
        }
    }
    printf("GOODBYE!\n");
    return 0;
}
