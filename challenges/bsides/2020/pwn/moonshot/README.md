# moonshot

A Lua sandbox escape challenge.

## Quick start

To build and test locally:
```bash
make test [CI_REGISTRY_IMAGE=registry.gitlab.com/cybears-private/fall-of-cybeartron]
```

To replicate the CI locally with docker:

```bash
# For this to work, you’ll need to login with docker to pull the images
docker login registry.gitlab.com
# Once you’re logged in you can run the docker containers
make docker-test
```

This will:
- Build the containers
    - Dockerfile.builder - This knows how to compile the challenge
    - Dockerfile.challenge - This knows how to run the challenge
    - Dockerfile.healthcheck - This knows how to solve the challenge
- Build the challenge inside the builder container, mounting the repo into the container
  the same way the GitLab builder does.
- Create a network
- Spin up the challenge in a container
- Run the solver in a container against the challenge container, exporting
  environment to tell the solver where the challenge is hosted.
