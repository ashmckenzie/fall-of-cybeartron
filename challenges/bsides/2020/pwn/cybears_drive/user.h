#ifndef _CYBEARS_DRIVE_USER_H
#define _CYBEARS_DRIVE_USER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LEN 25

#define ADMIN_FULLNAME "Koala Bear"
#define ADMIN_USERNAME "admin"
#define ADMIN_UID 5002

#define USER_FULLNAME "Paddington Bear"
#define USER_USERNAME "paddington"
#define USER_UID 5001
#define USER_PASSWORD "marmalade"

/* The full size of this struct has to remain less than 64 bytes. Exploitation
 * of the UAF requires that when the user struct is freed it gets put into a
 * fastbin, which is only the case if the size < 64
 */
struct user {
    char fullname[MAX_NAME_LEN];
    char username[MAX_NAME_LEN];
    size_t id;
} __attribute__((packed));

/* Pointer to track the currently logged on user.
 *
 * BUG BUG BUG:
 *
 * This pointer is used to create a UAF scenario. It is intentionally
 * NOT reset to NULL on user logout to allow exploitation by using the
 * feedback function to write to the memory that the pointer still points
 * to.
 *
 */
struct user *current_user = NULL;

void user_logout(void)
{
    if (current_user == NULL || current_user->id == 0) {
        printf("Not Logged In.\n");
    }

    else {
        memset(current_user, 0, sizeof(*current_user));
        free(current_user);

        /* BUG BUG BUG
         * current_user is not set back to NULL
         */
        printf("Successfully logged out\n");
    }
}

/*
 * Authenticate a user with the supplied credentials.
 *
 * If successful sets current_user appropriately.
 *
 * Returns 0 on success and 1 on error
 *
 * NOTE: If a user is authenticated they will
 * be logged out first
 *
 */
int user_login(char *username, char *password)
{
    if (NULL != current_user) {
        user_logout();
    }

    struct user *u = NULL;

    if (strncmp(USER_USERNAME, username, strlen(USER_USERNAME)) == 0) {
        if (strncmp(USER_PASSWORD, password, strlen(USER_PASSWORD)) == 0) {
            u = calloc(1, sizeof(*u));
            strncpy(u->fullname, USER_FULLNAME, MAX_NAME_LEN - 1);
            u->fullname[MAX_NAME_LEN - 1] = '\0';
            strncpy(u->username, USER_USERNAME, MAX_NAME_LEN - 1);
            u->username[MAX_NAME_LEN - 1] = '\0';

            u->id = USER_UID;
            current_user = u;
            return 0;
        }
    }

    return 1;
}

#endif

