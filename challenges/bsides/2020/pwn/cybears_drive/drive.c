#include <stdio.h>
#include <errno.h>

#include "user.h"
#include "flag.h"

/*
 * Prints the DRIVE ASCII Art Banner on first run
 */
void print_banner(void)
{
    char banner[] = ""
    "        ___           ___                       ___           ___     \n"
    "       /\\  \\         /\\  \\          ___        /\\__\\         /\\  \\    \n"
    "      /::\\  \\       /::\\  \\        /\\  \\      /:/  /        /::\\  \\   \n"
    "     /:/\\:\\  \\     /:/\\:\\  \\       \\:\\  \\    /:/  /        /:/\\:\\  \\  \n"
    "    /:/  \\:\\__\\   /::\\~\\:\\  \\      /::\\__\\  /:/__/  ___   /::\\~\\:\\  \\ \n"
    "   /:/__/ \\:|__| /:/\\:\\ \\:\\__\\  __/:/\\/__/  |:|  | /\\__\\ /:/\\:\\ \\:\\__\\\n"
    "   \\:\\  \\ /:/  / \\/_|::\\/:/  / /\\/:/  /     |:|  |/:/  / \\:\\~\\:\\ \\/__/\n"
    "    \\:\\  /:/  /     |:|::/  /  \\::/__/      |:|__/:/  /   \\:\\ \\:\\__\\  \n"
    "     \\:\\/:/  /      |:|\\/__/    \\:\\__\\       \\::::/__/     \\:\\ \\/__/  \n"
    "      \\::/__/       |:|  |       \\/__/        ~~~~          \\:\\__\\    \n"
    "       ~~            \\|__|                                   \\/__/   \n\n";

    printf("%s\n", banner);
}

/*
 * Get an unsigned long from stdin and store it in *number.
 *
 * Returns NULL if an error occurred (EOF encountered or invalid input)
 *
 * Returns number on success
 */
unsigned long *get_number(unsigned long *number)
{
#define INPUT_SIZE 255
    char line[INPUT_SIZE] = {0};
    char *endptr = NULL;
    unsigned long ret = 0;

    if (fgets(line, INPUT_SIZE, stdin) == NULL) {
        return NULL;
    }

    /* Remove the \n from the input so that strtoul error
     * checking is accurate
     */

    line[strcspn(line, "\n")] = '\0';

    errno = 0;

    ret = strtoul(line, &endptr, 10);

    if (errno == 0 && (*endptr == '\0' && line[0] != '\0')) {
        *number = ret;
        return number;
    }

    return NULL;
}

struct menu_item {
    char *text;
    void (*handler)(void);
};

void void_handler(void) {
    return;
}

void handle_quit(void) {
    exit(EXIT_SUCCESS);
}

void handle_get_flag(void) {
    flag_get();
}

void handle_logout(void) {
    user_logout();
}

void handle_login(void) {
    char username[MAX_NAME_LEN] = {0};
    char password[MAX_NAME_LEN] = {0};

    printf("Username: ");
    if (fgets(username, MAX_NAME_LEN, stdin) == NULL) {
        printf("Invalid\n");
        return;
    }

    printf("Password: ");

    if (fgets(password, MAX_NAME_LEN, stdin) == NULL) {
        printf("Invalid\n");
        return;
    }

    if (user_login(username, password) != 0) {
        printf("Login Failed\n");
    }

    else {
        printf("Login Succeeded\n");
    }
}

void handle_feedback(void) {
    unsigned long feedback_len = 0;
    char *feedback = NULL;

    printf("How many characters do you require? ");

    if (get_number(&feedback_len) == NULL) {
        printf("Invalid Input\n");
        return;
    }

    if (feedback_len == 0 || feedback_len > 4096) {
        printf("Feedback is limited to 4096 characters\n");
        return;
    }

    feedback = malloc(feedback_len);
    memset(feedback, 0, feedback_len);

    if (feedback == NULL) {
        printf("Error: Unable to take feedback at this time");
        return;
    }

    printf("Feedback: ");

    if (fgets(feedback, feedback_len, stdin) == NULL) {
        free(feedback);
        return;
    }

    printf("Thank you, your feedback is important to us\n");

    free(feedback);
}

struct  menu_item main_menu[] = {
    {
        .text = "", /* Dummy entry to make the menu print from 1 */
        .handler = void_handler,
    },
    {
        .text = "Login",
        .handler = handle_login,
    },
    {
        .text = "Get Flag",
        .handler = handle_get_flag,
    },
    {
        .text = "Leave Feedback",
        .handler = handle_feedback,
    },
    {
        .text = "Logout",
        .handler = handle_logout,
    },
    {
        .text = "Quit",
        .handler = handle_quit,
    },
};

size_t menu_len = sizeof(main_menu)/sizeof(struct menu_item);

void print_menu(void)
{
    size_t i = 0;

    printf("\nWelcome to Cybears Drive (BETA)\n");

    for (i = 1; i < menu_len; i++) {
        printf("%zu. %s\n", i, main_menu[i].text);
    }

    printf("> ");
}

void get_menu_input(void)
{
    unsigned long option = 0;

    if (get_number(&option) == NULL) {
        printf("Invalid Input -> Quitting.\n");
        exit(EXIT_SUCCESS);
    }

    if (option > 0 && option < menu_len) {
        main_menu[option].handler();
    }

}


int main(int argc, char *argv[])
{
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    flag_load();
    print_banner();

    for (;;)
    {
        print_menu();
        get_menu_input();
    }

    flag_free();
    return 0;
}

