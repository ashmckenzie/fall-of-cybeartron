
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <io.h>
#include <fcntl.h>
#include <windows.h>
#include <winternl.h>
#include <psapi.h>

__declspec(dllexport) void readFlag();
void readFlag() {
    char buffer[128] = {0};
    FILE * flag_file = fopen("flag.txt", "r");
    if (flag_file)
    {
        fread(buffer, 1, sizeof(buffer), flag_file);
        fclose(flag_file);
        printf("%s\n", buffer);
    }
    else
    {
        printf("Good start, you have got execution locally. Now try it against the competition server\n");
    }
}

#define NAME_LEN 0x180
int main(void)
{
    char input[NAME_LEN]  = {0};
    char* readString = NULL;
    if (_setmode(_fileno(stdout), _O_BINARY) != -1) {
        if (setvbuf(stdout, NULL, _IONBF, 0) == 0) {
            printf("So you want to learn to pwn Windows?\nIt's actually not as hard as you might think.\nAll you need to do is fill my buffer, but maybe this will help 0x%x?\nEnter your buffer here: ", (LPVOID)GetModuleHandle(NULL));
            readString = gets(input);

            if (readString != NULL) {
                printf("Ok. I have your buffer, now lets see what happens....\n");
            }
        }
    }
    return 0;
}
