#!/usr/bin/env python3
import argparse
import binascii
import pwn
import Cryptodome.Cipher.AES

def doit(args):
    conn = pwn.remote(args.hostname, args.port)
    conn.recvuntil(">> ")
    conn.sendline("1")
    ztext = conn.recvline(keepends=False)
    ztext = binascii.unhexlify(ztext)
    conn.recvuntil(">> ")

    # Walk backward block-wise through the ciphertext
    aes_bs = Cryptodome.Cipher.AES.block_size
    ptext = bytearray()
    for tail_offset in range(len(ztext), aes_bs, -aes_bs):
        print("Extracting plaintext from block ending at %i" % tail_offset)
        # z1 is the original preceding ciphertext chunk
        z1 = ztext[tail_offset - aes_bs * 2 : tail_offset - aes_bs]
        z2 = ztext[tail_offset - aes_bs : tail_offset]
        # Walk backward through z2 brute forcing each byte with the oracle
        z1prime = bytearray(aes_bs) # Make a block size bytearray
        for block_offset in range(aes_bs - 1, -1, -1):
            desired_padval = aes_bs - block_offset
            for candidate in range(256):
                z1prime[block_offset] = candidate
                # Attempt a decrypt, if it doesn't explode then we have the
                # right z1' value to have met the PKCS padding requirement
                conn.sendline("2")
                conn.sendline(binascii.hexlify(z1prime + z2))
                output = conn.recvuntil(">> ")
                if b"Invalid padding" not in output:
                    break
            else:
                raise Exception(
                    "Failed to find z1prime byte at %i"
                    % (tail_offset - aes_bs * 2 + block_offset)
                )
            # So now we know that:
            #   `p2[off] = z1prime[off] ^ z1[off] ^ desired_padval`
            ptext.insert(0, candidate ^ z1[block_offset] ^ desired_padval)
            # And we want to fix `z1prime` for the next padding width
            for i in range(block_offset, aes_bs):
                z1prime[i] ^= desired_padval ^ (desired_padval + 1)
    print("Extracted the plaintext! %r" % bytes(ptext))

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--hostname', type=str,  default="localhost")
    parser.add_argument('-p', '--port', default=2323, type=int)
    return parser.parse_args()

if __name__ == "__main__":
    doit(parse_args())
