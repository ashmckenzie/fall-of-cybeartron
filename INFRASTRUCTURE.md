# Cybears CTF Infrastructure

This document describes how to setup both a testing and production environment for cybears hosted CTFs.

## Testing

Local testing is done mostly with k3s, this is also suitable for small running of a CTF.

## Cloud provider

This production deployment is designed to work on AWS whilst utilising terraform to ensure repeatable deployments.

## Prerequisites

### Automatic installation
If you are using a supported OS (currently only Ubuntu) you can install the prerequisites with the `setup.sh` script
```bash
./setup.sh
```
When prompted enter your aws credentials

### Manual installation
If you cant run the automatic installation complete the following. 

1. terraform download from `https://www.terraform.io/downloads.html`
1. kubectl > v1.19 `snap install kubectl --classic` in ubuntu
1. k3s `see https://k3s.io/`
1. aws cli `sudo apt install awscli` in ubuntu
1. python3 `sudo apt install python3` in ubuntu
1. openssl `sudo apt install openssl` in ubuntu
1. jq `sudo apt install jq` in ubuntu
1. python dependencies `sudo python3 -m pip install -r requirements-infra.txt`
1. AWS credentials in `~/.aws/credentials` use `aws configure` to setup.
1. Install the aws session manager plugin https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html
1. .ctf_secret_key in `infra/ctfd/terraform`- can use:

```bash
python3 -c "import os; f=open('infra/ctfd/terraform/.ctfd_secret_key', 'ab+'); f.write(os.urandom(64)); f.close()"
```

## Quick Up and Running

The deployments are controlled by spin_up* python scripts in the `bin/` directory. There are two components to the deployment: `CTFd` which is the frontend, and `challenges` where pwnable and similar challenges are hosting in kubernettes. The `bin/spin_up.py` script spins up both components. For this only k3s and aws are supported but individual components can be brought up on different platforms. Run `bin/spin_up_challenges.py -h` or `bin/spin_up_ctfd.py -h` to see supported platforms.

Downloading of artifacts from gitlab is done via the gitlab api. If you are using a non-public repo then you will need to have an access token configured: https://gitlab.com/-/profile/personal_access_tokens

The spin_up scripts may prompt for this, to avoid the prompt you can store the token in the `GITLAB_ACCESS_TOKEN` environment variable.

### Test Deployment

To spin up a test (suitable for small CTFs) on local k3s run the following:

```bash
./bin/spin_up.py --config config/testing.tfvars.json k3s start
```
And to tear down:

```bash
./bin/spin_up.py --config config/testing.tfvars.json k3s stop
```

To spin up the B-Sides CBR 2019 CTF do the following:

```bash
./bin/spin_up.py --config config/bsides-2019.tfvars.json k3s start
```
### Production Deployment

To spin up a the production environment on aws run the following. Note if you are not a cybear you will need to configure `config/xxx.tfvars.json`. You will need to update the following:
1. remote_state_s3
1. ctf_domain
1. ctf_domain_zone_id
1. chal_domain
1. chal_domain_zone_id
1. gitlab_group
1. gitlab_project
1. https_certificate_arn
1. grafana_certificate_arn

See [Process](#Process) for information on setting up remote_state and Route53 needed for the above. Then you will be able to spin up the AWS version.

```bash
./bin/spin_up.py --config config/bsides-2019.tfvars.json aws start
```

And to tear down:

```bash
./bin/spin_up.py --config config/bsides-2019.tfvars.json aws stop
```
# Design

Our CTF infrastructure is broken into two parts. 
1.  the CTF frontend (CTFd)
1.  the Challenge services

The CTFd setup Looks something like this:

```mermaid
graph TB

  subgraph "Uploads"
    S3[S3]
  end

  subgraph "RDS (mysql)"
    RDS[RDS autoscale]
  end

  subgraph "ElasticCache (redis)"
    REDIS[REDIS] --> ElasticCache1[Instance 1]
    REDIS[REDIS] --> ElasticCache[...]
    REDIS[REDIS] --> ElasticCacheN[Instance n]
  end

  subgraph "Load Balancer"
    LB[LB] --> Instance1[Instance 1]
    LB[LB] --> Instance[...]
    LB[LB] --> InstanceN[Instance n]
    Instance1 --> RDS[RDS]
    Instance --> RDS[RDS]
    InstanceN --> RDS[RDS]
    Instance1 --> REDIS[REDIS]
    Instance --> REDIS[REDIS]
    InstanceN --> REDIS[REDIS]
    Instance1 --> S3[S3]
    Instance --> S3[S3]
    InstanceN --> S3[S3]
    end
```


```mermaid
graph TB
  External-DNS --> Route53
  Route53 --> LoadBalancer
  Route53 --> LoadBalancer_windows
  Route53 --> LoadBalancer_n
  subgraph "EKS"
    subgraph "Linux node"
        subgraph "kube-system(namespace)"
            cluster-autoscaler
        end
        subgraph "monitoring(namespace)"
            Grafana --> Prometheus
            Prometheus --> CloudWatch["CloudWatch Exporter"]
            Prometheus --> PrometheusNE["Prometheus Node Exporter"]
        end
        External-DNS["External-DNS(pod)"]

        subgraph "challenge(namespace)"
            subgraph "challenge1"
                LoadBalancer["LoadBalancer(service)"] --> Challenge_replica1
                LoadBalancer --> Challenge_replica2
                subgraph "replica (pod)"
                    Challenge_replica1[Challenge]
                    Healthcheck_replica1[Healthcheck] --> Challenge_replica1
                end
                subgraph "replica1 (pod)"
                    Challenge_replica2[Challenge]
                    Healthcheck_replica2[Healthcheck] --> Challenge_replica2
                end
            end
        end
    end
    subgraph "Windows node"
        subgraph "Windows challenge(namespace)"
            subgraph "Windows challenge1"
                LoadBalancer_windows["LoadBalancer(service)"] --> Challenge_windows_replica1
                LoadBalancer_windows --> Challenge_windows_replica2
                subgraph "replica (pod)"
                    Challenge_windows_replica1[Challenge]
                    Healthcheck_windows_replica1[Healthcheck] --> Challenge_windows_replica1
                end
                subgraph "replica1 (pod)"
                    Challenge_windows_replica2[Challenge]
                    Healthcheck_windows_replica2[Healthcheck] --> Challenge_windows_replica2
                end
            end
        end
    end
    subgraph "Linux node_n (autoscales)"
        subgraph "Node n challenge(namespace)"
            subgraph "challenge_n"
                LoadBalancer_n["LoadBalancer(service)"] --> Chalenge_n_replica1
                LoadBalancer_n --> Chalenge_n_replica2
                subgraph "replica_n (pod)"
                    Chalenge_n_replica1[Challenge]
                    Healthcheck_n_replica1[Healthcheck] --> Chalenge_n_replica1
                end
                subgraph "replica1_n (pod)"
                    Chalenge_n_replica2[Challenge]
                    Healthcheck_n_replica2[Healthcheck] --> Chalenge_n_replica2
                end
            end
        end
    end
  end

```
# Process

Setup for a ctf is a multi-step process, the design for this repo is auto automate these steps and then create scripts to wrap the steps.

## Pre-setup (shouldn't need to run again)

You need an s3 bucket for terraform remote_state. This allows other team members to run other terraform commands. This bucket was created as a once off with terraform.
```bash
infra/terraform_remote_state> terraform apply

Outputs:

remote_state_lock = terraform-state-lock-dynamo
remote_state_s3 = terraform-20191116042935470700000001
```

the outputs from this are set as the remote state for the other terraform operations.

## DNS (Only need to run if changing the domains)

Route53 is used for DNS within AWS, however cybears DNS is hosted elsewhere. For this reason (until we automate the external DNS provider) we need to setup the zones in AWS before then setting the appropriate NS records in our DNS hosting.

Follow-on scripts will assume the NS records are set and use the Route53 zone information for controlling DNS records to the appropriate infrastructure.

To create zones specify the preferred domain or subdomain in `config/xxx.tfvars.json` then run:

```bash
./bin/setup_dns.py --config config/[testing,bsides-2019].tfvars.json [--apply]
```
Update the domain hosting NS records to the nameservers described in the script output. If you didn't run the script yourself then don't use `--apply` and you will get the current nameservers.

## Challenges

```bash
./bin/spin_up_challenges.py --config config/[testing,bsides-2019].tfvars.json [aws,k3s] [start,stop]
```

## CTFd

```bash
./bin/spin_up_ctfd.py --config config/[testing,bsides-2019].tfvars.json [aws,k3s] [start,stop]
```
you may want to run the following incase you need to upload new challenges later.

```bash
export CTFD_HOST='https://test.cybears.io/' # or whatever is outputted from the above command
```

### Updating CTFd infra in place
After modifying `config/xxx.tfvars.json` the `start` command above can be rerun to apply changes in place.
It is recommended that you run `terraform plan -var-file=[../../../config/xxx.tfvars.json file]` from the `infra/ctfd/terraform` directory first to see what changes will be made.
There may be some interruptions to the service while the changes are made but the CTFd database should be maintained.
It is currently expected that the main reason to do this would be to increase the size of an instance due to high load. 
Keep in mind that unless `apply_immediately` is true on the instance the changes may not occur until the next scheduled maintenance window.


## Other users

After setup anyone else wanting to control the setup needs to get the terraform state. This is useful to get ctfd passwords or kubernetes control files


```bash
./bin/spin_up_ctfd.py --config config/[testing,bsides-2019].tfvars.json aws state
```

```bash
./bin/spin_up_challenges.py --config config/[testing,bsides-2019].tfvars.json aws state
```
Now you should be able to run kubectl commands.


## Monitoring

A Monitoring stack(prometheus and grafana) is spun up in the challenge EKS (or k3s). This is done via terraform in both circumstances. It is equivalent to the following:
```bash
[sudo KUBECONFIG=/etc/rancher/k3s/k3s.yaml] helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
[sudo KUBECONFIG=/etc/rancher/k3s/k3s.yaml] helm repo add stable https://charts.[sudo KUBECONFIG=/etc/rancher/k3s/k3s.yaml] helm.sh/stable
[sudo KUBECONFIG=/etc/rancher/k3s/k3s.yaml] helm repo update
[sudo k3s] kubectl create -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
[sudo KUBECONFIG=/etc/rancher/k3s/k3s.yaml] helm upgrade --install mon prometheus-community/kube-prometheus-stack -f values.yml 
```
sudo version for k3s.

The grafana host and admin password is an output from the terraform process `(cd infra/mon/aws/ && terraform output)` - the admin account name is `admin`. You can log into grafana at `http://grafana-host` use `kubectl get service/mon-grafana --namespace monitoring` to find the LoadBalancer address(EXTERNAL-IP) for the grafana service. Or `http://locahost:8000` for k3s deployment.

This is removed with the stop command in `spin_up_challenges.py` or `spin_up.py`

Prometheus gets a number of kubernetes metrics as well as cloudwatch metrics from AWS. This is used for monitoring of the CTFd deployment (not in EKS). Even in the k3s case the AWS metrics are gathered - this is purely for testing.

The grafana dashboard (and panels) are controlled via json configuration. This configuration is in `infra/mon/prometheus_stack/cybears.json`. If you make changes to the dashboard, export the json and merge with this file.

## Debugging / Analysis

### Checking DNS for containers

```bash
aws route53   list-resource-record-sets --hosted-zone-id Z1BVYXDYQIB3MB
```
replace Z1BVYXDYQIB3MB with the hosted-zone-id from the config/xxx.tfvars.json

## Checking the autoscaling is working for the challenge EKS

```bash
kubectl --namespace=kube-system get pods -l "app.kubernetes.io/name=aws-cluster-autoscaler"

kubectl get hpa --namespace challenges
```

## Debugging CTFd


Find the frontend instances with the following(
```bash
aws ec2 describe-instances --filters "Name=tag-value,Values=ctfd-autoscaling-group" --query "Reservations[*].Instances[*].InstanceId"
```

Then you can do the following:
```bash
aws ssm start-session --target [target_id]
```

## Debugging Challenges
### Check services (LoadBalancer)
```bash
kubectl get service [-o wide] --namespace challenges
```

### Check pods (Challenges)
```bash
kubectl get pods --namespace challenges
```

### Check status of pod (Challenge)
```bash
kubectl describe pod <pod_name_from_kubectl_get_pods> --namespace challenges
```

### Check status of service (Challenge Load Balancer)
```bash
kubectl describe service <service_name_from_kubectl_get_service> --namespace challenges
```

#### Check logs of a container
```bash
kubectl logs <pod_name_from_kubectl_get_pods> <container> --namespace challenges
```
### Terraform state

Terraform uses remote state so we can share info between users. This requires a lock so only one user can issue terraform commands at the same time.

If this gets out of sync (ctrl-c in the middle of a terraform command) then the lock needs to be fixed up.

Use force unlock from within the relevant terraform directory, i.e. for eks setup issue like this:

```
Error: Error locking state: Error acquiring the state lock: ConditionalCheckFailedException: The conditional request failed
	status code: 400, request id: 0RFP7T4J8NAUB12U39E6EMFDG7VV4KQNSO5AEMVJF66Q9ASUAAJG
Lock Info:
  ID:        9520e0b6-9acf-c339-8c04-c394af8f072f
  Path:      terraform-20191116042935470700000001/eks/terraform.tfstate
  Operation: OperationTypeApply
  Who:       vagrant@invalid-ctf
  Version:   0.12.16
  Created:   2020-03-17 22:44:50.405026446 +0000 UTC
  Info:
  ```

  Run the following:

```bash
cd <terraform_files_location>
terraform force-unlock 9520e0b6-9acf-c339-8c04-c394af8f072f
```

Depending on when and how the lock was broken you might have to manually delete stuff from aws console to get back to a clean state.