# Cybears Continuous Integration

Our CI system is designed to test every challenge and ensure
no breakage on the day.

To accomplish this we aim to:
- Build things from reproducable containers
- Have a solve script for every challenge
- Provide a healthcheck container to auto-solve and confirm challenges work
- Continuously test challenges with the healthchecks in our CI system to reduce bit-rot

This is a complex task with a few moving parts. To support this we use
some advanced features of GitLab's CI system. This document aims to clear up how things
are structured.

We use two sets of [child pipelines](https://docs.gitlab.com/ce/ci/parent_child_pipelines.html)
in our CI system. The root [.gitlab-ci.yml](/.gitlab-ci.yml) kicks off a child pipeline for each
[event](/challenges/bsides/2020/gitlab-ci.yml). The event _includes_ each category underneath,
and each [category](/challenges/bsides/2020/pwn/gitlab-ci.yml) kicks off another child pipeline
for each [challenge](/challenges/bsides/2020/pwn/path.to.flag/gitlab-ci.yml). Each challenge can
include templates and other helpers, typically from the [base template](/.gitlab/ci/base.yml).

```mermaid
graph LR
    subgraph "Challenge"
        base_template["/.gitlab/ci/base.yml"]
        challenge["/challenges/<event>/<category>/<challenge>/gitlab-ci.yml"] -- Include --> base_template
    end
    subgraph "Event"
        category["/challenges/<event>/<category>/gitlab-ci.yml"] -- Child Pipeline --> challenge
        event["/challenges/<event>/gitlab-ci.yml"] -- Include --> category
    end
    root["/.gitlab-ci.yml"] -- Child Pipeline --> event
```

This structure gives each event and challenge its own pipeline in which it can do as it pleases. A challenge can
import the [base template](/.gitlab/ci/base.yml) to get some helpers and suggested stages, but equally
it could disregard this and implement its own bespoke thing. Executing each challenge in its own child
pipeline allows challenge authors to treat the rest of the CI as a black box, and it keeps challenges
from blatting global variables and such that other challenges might depend on.

# Include vs Child pipelines

[Include directives](https://docs.gitlab.com/ce/ci/yaml/README.html#include)
essentially `cat(1)` your CI configurations together. This means if someone else
defines a global variable or an override it may be included into your yaml and break your configuration.
This becomes very fragile and confusing as more things are included.

Using a [child pipeline](https://docs.gitlab.com/ce/ci/parent_child_pipelines.html)
keeps things isolated. This allows us to iterate on the CI structure of events
and challenges without breaking other events and challenges (which we did when we used `include`).
Keeping thing standalone means you only need to care about what _you_ include in your CI configuration.


# For challenge authors

If you're a challenge author you probably don't need to care too much about what I said above.

If you use the [challenge wizard](/bin/chal.py) to create your challenge, everything will be
configured in the suggested way and you'll get a few things for free:

- A builder container to compile your challenge
- A challenge container to host your challenge
- A healthcheck container to run your solve script against your challenge
- A deploy step to gather and save your handouts

If you don't like the way the CI is set up or it doesn't work for your challenge, feel free
to edit the `gitlab-ci.yml` file in your challenge directory.

If you remove the following lines:
```yaml
include:
    - "/.gitlab/ci/base.yml"
```
you'll be able to have your own stages, templates, jobs, etc.

You are the master of your CI. See the
[GitLab CI reference](https://docs.gitlab.com/ce/ci/yaml/README.html)
for more information about the CI format. There are also plenty of example
configurations in the other challenges.
