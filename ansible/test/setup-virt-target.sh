#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

SCRIPT_PATH="$(realpath -e "${BASH_SOURCE[0]}")"
SCRIPT_DIR="${SCRIPT_PATH%/*}"

FLAVOUR="${1:-fedora}"
: "${UBUNTU_NAME:=focal}"
: "${FEDORA_VERS:=33}"

# We can use `virt-viewer` so just auto-assign a VNC port
VNC_PORT="-1"
SSH_PORT=22022
SSH_WAIT_TIME=30
: "${PLAYBOOK:=default}"

: "${UBUNTU_CLOUD_IMAGE_LOC:=https://cloud-images.ubuntu.com/}"
: "${FEDORA_REL_LOC:=https://fedora.mirror.digitalpacific.com.au/linux/releases}"

declare -ar VIRT_INSTALL_ARGS=(
    --virt-type=kvm --noreboot --os-variant=detect=on
    --vcpus=2 --memory=4096
    --graphics=none --console="pty,target_type=serial"
)
declare -a EXTRA_VIRT_INSTALL_ARGS=()

: "${VIRT_KEYFILE:=${SCRIPT_DIR}/id_virt}"
if [ ! -f "${VIRT_KEYFILE}" ]; then
    ssh-keygen -t ed25519 -N "" -f "${VIRT_KEYFILE}"
fi
declare -ar ANSIBLE_HOST_DATA=(
    "ansible_connection=ssh"
    "ansible_port=22022"
    "ansible_ssh_extra_args='-o StrictHostKeyChecking=no'"
    "ansible_ssh_private_key_file=${VIRT_KEYFILE}"
)
declare -a EXTRA_ANSIBLE_HOST_DATA=()

case "${FLAVOUR}" in
    fedora)
        DOM_NAME="cybears-ansible-test-${FLAVOUR}-${FEDORA_VERS}"
        AUTOINSTALL_FILE="${SCRIPT_DIR}/fc-ansible-ks.cfg"
        NETINST_IMG_LOC="${FEDORA_REL_LOC}/${FEDORA_VERS}/Server/x86_64/os/"
        VIRT_ADDR="127.0.$(tr -cd '[0-9]' <<< "${FEDORA_VERS}").1"
        EXTRA_ANSIBLE_HOST_DATA+=(
            "ansible_host=${VIRT_ADDR}"
            "ansible_user=root"
        )
        ;;
    ubuntu)
        DOM_NAME="cybears-ansible-test-${FLAVOUR}-${UBUNTU_NAME}"
        AUTOINSTALL_FILE="${SCRIPT_DIR}/user-data"
        CLOUD_IMAGE_NAME="${UBUNTU_NAME}-server-cloudimg-amd64-disk-kvm.img"
        CLOUD_IMAGE_URI="${UBUNTU_CLOUD_IMAGE_LOC}/${UBUNTU_NAME}/current/${CLOUD_IMAGE_NAME}"
        UBUNTU_INDEX="$(printf "%i" "'${UBUNTU_NAME}")"
        VIRT_ADDR="127.1.${UBUNTU_INDEX}.1"
        EXTRA_VIRT_INSTALL_ARGS+=( "--network=user,model=virtio" )
        EXTRA_ANSIBLE_HOST_DATA+=(
            "ansible_host=${VIRT_ADDR}"
            "ansible_user=user" "ansible_sudo_pass=firstlogin"
        )
        ;;
    *)
        echo "[E] Unsupported flavour '${FLAVOUR}'" >&2
        exit 1
        ;;
esac
EXTRA_VIRT_INSTALL_ARGS+=( "--name=${DOM_NAME}" )

# Interpolate the pubkey into any autoinstall file
if [ -n "${AUTOINSTALL_FILE:-}" ]; then
    tmpname="$(mktemp -u)"
    sed "${AUTOINSTALL_FILE}" >"${tmpname}"                                 \
        -e "s;##PUBKEY_GOES_HERE##;$(cat "${VIRT_KEYFILE}.pub");"
    AUTOINSTALL_FILE="${tmpname}"
fi

# We use a user session by default
: "${LIBVIRT_DEFAULT_URI:=qemu:///session}"
export LIBVIRT_DEFAULT_URI

virsh connect
# Tear down the domain if it's up
virsh destroy --graceful --domain "${DOM_NAME}" ||:
if "${VIRT_INSTALL:-true}"; then
    # Clean up any existing domain and its volume
    virsh undefine --domain "${DOM_NAME}" ||:
    virsh vol-delete --pool default --vol "${DOM_NAME}.qcow2" ||:
    # Download a cloud image if required, using an etag to avoid it if possible
    if [ -n "${CLOUD_IMAGE_URI:-}" ]; then
        img_file="${SCRIPT_DIR}/${CLOUD_IMAGE_URI##*/}"
        etag_file="${img_file}.etag"
        extra_args=("--etag-save" "${etag_file}")
        [ -f "${etag_file}" ] && extra_args+=("--etag-compare" "${etag_file}")
        curl -L --fail --output "${img_file}" "${extra_args[@]}" "${CLOUD_IMAGE_URI}"
        chcon -t virt_content_t "${img_file}"
        EXTRA_VIRT_INSTALL_ARGS+=(
            "--disk=size=40,sparse=yes,backing_store=${img_file}"
        )
        if [ -n "${AUTOINSTALL_FILE:-}" ]; then
            EXTRA_VIRT_INSTALL_ARGS+=(
                "--cloud-init=user-data=${AUTOINSTALL_FILE}"
            )
        fi
    elif [ -n "${NETINST_IMG_LOC:-}" ]; then
        EXTRA_VIRT_INSTALL_ARGS+=(
            "--disk=size=40,sparse=yes"
            "--location=${NETINST_IMG_LOC}"
        )
        if [ -n "${AUTOINSTALL_FILE:-}" ]; then
            EXTRA_VIRT_INSTALL_ARGS+=(
                "--initrd-inject=${AUTOINSTALL_FILE}"
                "--extra-args=console=ttyS0 inst.ks=file:///${AUTOINSTALL_FILE##*/}"
            )
        fi
    fi

    # Perform the installation
    virt-install "${VIRT_INSTALL_ARGS[@]}" "${EXTRA_VIRT_INSTALL_ARGS[@]}"
fi
# Wait for the domain to stop running
for i in $(seq 1 3); do
    virsh domstate --domain "${DOM_NAME}" | grep -vz "running" && break
    sleep 1
done

# Redefine the VM with a hostfwd network interface
virsh define <(
    virsh dumpxml --domain "${DOM_NAME}" | xsltproc                         \
        --stringparam ip "${VIRT_ADDR}" --stringparam port "${SSH_PORT}"    \
        "${SCRIPT_DIR}/virt-add-hostfwd.xslt" -
)
# Also add a VNC server - we can use `virt-xml` for this one
declare -r GRAPHICS="vnc,listen=${VIRT_ADDR},port=${VNC_PORT}"
virt-xml "${DOM_NAME}" --remove-device --graphics all
virt-xml "${DOM_NAME}" --add-device --graphics "${GRAPHICS}"
# Restart the domain without console and wait a while for SSH to come up
virsh start --domain "${DOM_NAME}"
for i in $(seq 1 "${SSH_WAIT_TIME}"); do
    nc "${VIRT_ADDR}" "${SSH_PORT}" </dev/null | grep SSH && break
    sleep 1
done

# Add the host to inventory and run the playbook if it exists
if [ -e "${PLAYBOOK}.yml" ]; then
    sed -i "/^${DOM_NAME}\s/d" hosts
    tee -a hosts <<< "${DOM_NAME} ${ANSIBLE_HOST_DATA[@]} ${EXTRA_ANSIBLE_HOST_DATA[@]}"
    # This will fail if SSH didn't come back up
    ansible-playbook -i hosts -l "${DOM_NAME}" "${PLAYBOOK}.yml"
else
    cat >&2 <<EOF
Add the following line to your ansible inventory:
    ${DOM_NAME} ${ANSIBLE_HOST_DATA[@]} ${EXTRA_ANSIBLE_HOST_DATA[@]}
EOF
fi
